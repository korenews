/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
exports.openEditorOld = function (fname, win, template) {
  let dwn = win.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindow);
  let chromeWin =
    dwn.QueryInterface(Ci.nsIInterfaceRequestor).
    getInterface(Ci.nsIWebNavigation).
    QueryInterface(Ci.nsIDocShellTreeItem).
    rootTreeItem.
    QueryInterface(Ci.nsIInterfaceRequestor).
    getInterface(Ci.nsIDOMWindow);
  if (!chromeWin) return;
  if (typeof(template) !== "string") template = "";
  let fl;
  if (fname instanceof Ci.nsIFile) {
    fl = fname;
  } else {
    fl = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);
    fl.followLinks = true;
    fl.initWithPath(fname);
  }
  debuglog("openEditor: ["+fl.path+"]");
  if (fl.exists() && fl.isDirectory()) return;
  let saved = false;
  let text = template;
  try {
    text = fileReadText(fl);
    saved = true;
  } catch (e) {
    logError("can't read file: "+fname);
  }
  let spw = chromeWin.Scratchpad.ScratchpadManager.openScratchpad({
    filename: fl.path,
    text: text,
    saved: saved,
  });
};


exports.openEditor = function (fname, win, template) {
  let fl;
  if (typeof(template) !== "string") template = "";
  if (fname instanceof Ci.nsIFile) {
    fl = fname;
  } else {
    fl = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);
    fl.followLinks = true;
    fl.initWithPath(fname);
  }
  if (fl.exists() && fl.isDirectory()) return;
  if (fl.exists() && !fl.isReadable()) return;

  if (!fl.exists()) {
    let foStream = Cc["@mozilla.org/network/file-output-stream;1"].createInstance(Ci.nsIFileOutputStream);
    // use 0x02 | 0x10 to open file for appending.
    foStream.init(fl, 0x02|0x08|0x20, 0666, 0);
    // write, create, truncate
    // In a c file operation, we have no need to set file mode with or operation,
    // directly using "r" or "w" usually.
    try {
      // if you are sure there will never ever be any non-ascii text in data you can
      // also call foStream.write(data, data.length) directly
      let converter = Cc["@mozilla.org/intl/converter-output-stream;1"].createInstance(Ci.nsIConverterOutputStream);
      converter.init(foStream, "UTF-8", 0, 0);
      try {
        converter.writeString(template);
      } finally {
        converter.close(); // this closes foStream
      }
    } catch (e) {
      foStream.close();
      throw e;
    }
  }

  debuglog("openEditor: ["+fl.path+"]");
  /*
  let exe = "/home/ketmar/zoft/bin/sterm";
  let params = [
    "-c", "korenews_term",
    "-t", "korenews",
    "-S",
    "-R", "ignoreclose 1 ; maxhistory 0",
    "-e", "egedit"
  ];
  */
  let exe = addonOptions.editor; //||"/usr/bin/leafpad";
  let params = [];

  params.push(fl.path);
  let path = Services.prefs.getCharPref("extensions.k8runitproto.runpath");
  //Cu.reportError("exe: ["+url+"]; args="+params.join("|"));
  let file = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);
  file.initWithPath(exe);
  let process = Cc["@mozilla.org/process/util;1"].createInstance(Ci.nsIProcess);
  process.init(file);
  //process.run(false, params, params.length);
  process.runAsync(params, params.length);
}
