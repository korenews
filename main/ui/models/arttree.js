/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
let flags = require("flags");


////////////////////////////////////////////////////////////////////////////////
let dateNow = new Date(); //FIXME: this must be periodically updated, due to "today" and such

// trick, 0 for `setDate()` is ok, heh
let dateYesterday = new Date(dateNow.getTime());
dateYesterday.setDate(dateNow.getDate()-1);


function unixTimeToString (date) {
  date = new Date(date*1000);
  let dstr;
  if (dateNow.getFullYear() == date.getFullYear() &&
      dateNow.getMonth() == date.getMonth() &&
      dateNow.getDate() == date.getDate())
  {
    dstr = "today,";
  } else if (dateYesterday.getFullYear() == date.getFullYear() &&
             dateYesterday.getMonth() == date.getMonth() &&
             dateYesterday.getDate() == date.getDate())
  {
    dstr = "yesterday,";
  } else {
    dstr =
      ""+date.getFullYear()+"/"+
      ("0"+(date.getMonth()+1)).substr(-2)+"/"+
      ("0"+date.getDate()).substr(-2);
  }
  let res =
    dstr+" "+
    ("0"+date.getHours()).substr(-2)+":"+
    ("0"+date.getMinutes()).substr(-2);
  return res;
}


////////////////////////////////////////////////////////////////////////////////
/* article tree data model
 * visible item object (article object with possible additional fields)
 *   int id         (article id)
 *   string url
 *   string title
 *   string author
 *   int pubdate    (unixtime)
 *   int state
 *   int mark
 */


////////////////////////////////////////////////////////////////////////////////
// creator should set such methods:
//   void onMarkToggled (obj)
function ArtTreeModel () {
  this.visItems = [];
  this.cacheArtsById = {};
    /* key: group id
     * data:
     *   object obj  (article object)
     *   int idx     (index in visItems)
     */
  this.treebox = null;

  // tree view model part
  // the total number of rows in the tree (including the offscreen rows)
  Object.defineProperty(this, "rowCount", {get: function () this.visItems.length});
}


ArtTreeModel.prototype.cleanup = function () {
  this.cacheArtsById = {};
  if (this.treebox && this.visItems.length > 0) this.treebox.rowCountChanged(0, -this.visItems.length);
  this.visItems = [];
  // update dates, otherwise they'll go out of sync. lol.
  dateNow = new Date();
  dateYesterday = new Date(dateNow.getTime());
  dateYesterday.setDate(dateNow.getDate()-1);
};


// add article
//   int id       (group id)
//   TODO
ArtTreeModel.prototype.addArticle = function (art) {
  if (art.id in this.cacheArtsById) return; //throw new Error("use `modifyArticle()` to change existing article");

  let vii = this.visItems;
  let insertAt = vii.length;

  this.cacheArtsById[art.id] = {obj:art, idx:insertAt};

  // add to visItems
  art.utitle = normSpaces(xmlUnescape(art.title||""));
  art.uauthor = normSpaces(xmlUnescape(art.author||""));
  vii.push(art);

  // update treebox
  if (this.treebox) this.treebox.rowCountChanged(insertAt, 1);
};


ArtTreeModel.prototype.setArticleMark = function (aid, marked) {
  let cc = this.cacheArtsById[aid];
  if (typeof(cc) !== "object") return;
  if (!cc.obj.mark == !marked) return; // nothing to change
  cc.obj.mark = !!marked;
  //if (this.treebox) this.treebox.invalidateRow(cc.idx);
  if (this.treebox) this.treebox.invalidateCell(cc.idx, this.treebox.columns[0]);
};


ArtTreeModel.prototype.setArticleState = function (aid, state) {
  let cc = this.cacheArtsById[aid];
  if (typeof(cc) !== "object") return;
  if (cc.obj.state === state) return; // nothing to change
  cc.obj.state = state;
  if (this.treebox) this.treebox.invalidateRow(cc.idx);
};


////////////////////////////////////////////////////////////////////////////////
ArtTreeModel.prototype.getDataAt = function (index) (index < 0 || index >= this.visItems.length ? null : this.visItems[index]);


// yields visitem object
ArtTreeModel.prototype.artIterator = function () {
  for (let vi of this.visItems) yield vi;
};


// yields index and visitem object
ArtTreeModel.prototype.artIteratorIndexed = function () {
  for (let [idx, vi] in Iterator(this.visItems)) yield [idx, vi];
};


////////////////////////////////////////////////////////////////////////////////
// treeview model
// the total number of rows in the tree (including the offscreen rows)
//Object.defineProperty(this, "rowCount", {get: function () this.visItems.length});

//boolean canDrop(in long index, in long orientation, in nsIDOMDataTransfer dataTransfer);
ArtTreeModel.prototype.canDrop = function (index, orientation, dataTransfer) false;

//void cycleCell(in long row, in nsITreeColumn col);
ArtTreeModel.prototype.cycleCell = function (row, col) {
  // change "marked" state if article is not deleted
  if (col.id === "artree-col-flag") {
    let art = this.getDataAt(row);
    //debuglog("cycleCell: row=", row, "; col.id=", col.id, "; art.id=", art.id, "; art.mark=", art.mark, "; art.state=", art.state);
    if (art.state < flags.ASTATE.DELMARKED) {
      this.onMarkToggled(art);
    }
  }
};

//void cycleHeader(in nsITreeColumn col);
ArtTreeModel.prototype.cycleHeader = function (col) {};

//void drop(in long row, in long orientation, in nsIDOMDataTransfer dataTransfer);
ArtTreeModel.prototype.drop = function (row, orientation, dataTransfer) {};

//AString getCellProperties(in long row, in nsITreeColumn col);
// return spalce-delimited string of properties
ArtTreeModel.prototype.getCellProperties = function (row, col) {
  let props = this.getRowProperties(row);
  if (col.id === "artree-col-flag") props += " favicon";
  return props;
};

//AString getCellText(in long row, in nsITreeColumn col);
ArtTreeModel.prototype.getCellText = function (row, col) {
  let art = this.getDataAt(row);
  switch (col.id) {
    case "artree-col-author": return (art.uauthor || art.author || "ghost of Kain");
    case "artree-col-title": return (art.utitle || art.title || "<untitled>");
    case "artree-col-date":
      // cache date string
      //FIXME: this must be periodically updated, due to "today" and such
      if (!art.pubdateStr) art.pubdateStr = unixTimeToString(art.pubdate);
      return art.pubdateStr;
  }
  return "";
};

//AString getCellValue(in long row, in nsITreeColumn col);
// this method is only called for columns of type other than text

//AString getColumnProperties(in nsITreeColumn col);
ArtTreeModel.prototype.getColumnProperties = function (col) "";

//AString getImageSrc(in long row, in nsITreeColumn col);
ArtTreeModel.prototype.getImageSrc = function (row, col) {
  if (col.id === "artree-col-flag") {
    let art = this.getDataAt(row);
    return (art.mark ? "chrome://korenews/skin/images/flag.png" : "chrome://korenews/skin/images/dot.png");
  }
  return null;
};

//long getLevel(in long index);
ArtTreeModel.prototype.getLevel = function (index) 0;

//long getParentIndex(in long rowIndex);
ArtTreeModel.prototype.getParentIndex = function (rowIndex) -1;

//long getProgressMode(in long row, in nsITreeColumn col);
// this method is only called for columns of type progressmeter

//AString getRowProperties(in long index);
ArtTreeModel.prototype.getRowProperties = function (index) {
  let props = "";
  let art = this.getDataAt(index);
  if (art.state >= flags.ASTATE.DELMARKED) {
    props += " delmark";
  } else if (art.state == flags.ASTATE.UNREAD) {
    props += " unread";
  }
  return props;
};


//boolean hasNextSibling(in long rowIndex, in long afterIndex);
// used to determine if the row at rowIndex has a nextSibling that occurs *after* the index specified by afterIndex
ArtTreeModel.prototype.hasNextSibling = function (rowIndex, afterIndex) false;

//boolean isContainer(in long index);
ArtTreeModel.prototype.isContainer = function (index) false;

//boolean isContainerEmpty(in long index);
ArtTreeModel.prototype.isContainerEmpty = function (index) true;

//boolean isContainerOpen(in long index);
ArtTreeModel.prototype.isContainerOpen = function (index) false;

//boolean isEditable(in long row, in nsITreeColumn col);
ArtTreeModel.prototype.isEditable = function (row, col) false; //TODO: true to allow inline editing of feed titles?

//boolean isSelectable(in long row, in nsITreeColumn col);
// this method is only called if the selection type is cell or text

//boolean isSeparator(in long index);
ArtTreeModel.prototype.isSeparator = function (index) false;

//boolean isSorted();
ArtTreeModel.prototype.isSorted = function () false;

//void performAction(in wstring action);
ArtTreeModel.prototype.performAction = function (action) {
  debuglog("arttree action: '", action, "'");
};

//void performActionOnCell(in wstring action, in long row, in nsITreeColumn col);

//void performActionOnRow(in wstring action, in long row);

//void selectionChanged();
ArtTreeModel.prototype.selectionChanged = function () {
  debuglog("arttree: selection changed");
};

//void setCellText(in long row, in nsITreeColumn col, in AString value);
// this method is called when the contents of the cell have been edited by the user

//void setCellValue(in long row, in nsITreeColumn col, in AString value);
// this method is only called for columns of type other than text

//void setTree(in nsITreeBoxObject tree);
ArtTreeModel.prototype.setTree = function (treebox) {
  this.treebox = treebox;
};

//void toggleOpenState(in long index);
ArtTreeModel.prototype.toggleOpenState = function (index) {};


////////////////////////////////////////////////////////////////////////////////
exports.ArtTreeModel = ArtTreeModel;
