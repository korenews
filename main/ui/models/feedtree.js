/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
let flags = require("flags");


////////////////////////////////////////////////////////////////////////////////
function buildObjText (uobj, deftitle) {
  if (typeof(deftitle) === "undefined") deftitle = "";
  let text = uobj.title||(""+deftitle);
  if (uobj.countTotal > 0 || uobj.countUnread > 0) {
    text += " (";
    if (uobj.countUnread > 0) text += uobj.countUnread+"/";
    text += (uobj.countTotal >= 0 ? uobj.countTotal : "?");
    text += ")";
  }
  return text;
}


////////////////////////////////////////////////////////////////////////////////
/* feed tree data model
 * visible item object:
 *   object obj        underlying object: feed or group; stripped (see below)
 *   bool container
 *   bool open
 *   int parentGid     group id (valid only for feeds)
 *   int parentIndex   in visItems or -1
 *   int nextSibIndex  index of next sibling or -1; valid only for groups
 *   string icon       can be "" or absent
 *   string text       cell text
 *
 * underlying feed object (can be shared):
 *   int id
 *   string title
 *   int countUnread   unread messages or 0
 *   int countTotal    total messages or 0
 *   string icon       can be "" or absent
 *   bool wasError     was there any error while updating this feed?
 *   int flags
 *
 * underlying group object (can be shared):
 *   int id
 *   string title
 *   int treeindex
 *   int countUnread   unread messages or 0
 *   int countTotal    total messages or 0
 *   string icon       can be "" or absent
 *   int[] feeds       feeds this group has
 *   bool hasFeeds     has some feeds added
 */


////////////////////////////////////////////////////////////////////////////////
function FeedTreeModel () {
  this.visItems = [];
  this.cacheGroupsById = {};
    /* key: group id
     * data: group object
     */
  this.cacheFeedsById = {};
    /* key: feed id
     * data: feed object
     */
  this.treebox = null;

  // tree view model part
  // the total number of rows in the tree (including the offscreen rows)
  Object.defineProperty(this, "rowCount", {get: function () this.visItems.length});
}


////////////////////////////////////////////////////////////////////////////////
// save open/closed state and cursor position
FeedTreeModel.prototype.saveState = function () {
  let state = {
    groups: {}, // key:int id; {bool open}
    curGid: -1, // selected group; -1: none
    curIdx: -1, // inside group; 0: group itself
    //topRow: -1, // treeview top row
  };
  let selIdx = -1;
  if (this.treebox) {
    //state.topRow = this.treebox.getFirstVisibleRow();
    selIdx = this.selection.currentIndex;
  }
  let lastGIdx = -1, lastGid = -1; // last seen group
  for (let [idx, vi] in Iterator(this.visItems)) {
    if (vi.container) {
      // group
      lastGIdx = idx;
      lastGid = vi.obj.id;
      state.groups[lastGid] = {open:vi.open};
      if (idx == selIdx) {
        state.curGid = lastGid;
        state.curIdx = 0;
      }
    } else {
      // feed
      if (lastGid >= 0 && idx === selIdx) {
        state.curGid = lastGid;
        state.curIdx = idx-lastGIdx;
      }
    }
  }
  return state;
};


// restore open/closed state and cursor position
FeedTreeModel.prototype.restoreState = function (state) {
  if (typeof(state) !== "object") return;
  if (this.treebox) this.treebox.beginUpdateBatch();
  let vii = this.visItems;
  let gs = state.groups;
  let idx = 0, visRow = -1;
  let lastGIdx = -1, lastGid = -1, ingIdx = 0; // last seen group
  while (idx < vii.length) {
    let vi = vii[idx];
    if (vi.container) {
      // group
      if (lastGid === state.curGid && visRow === -1) {
        // previous group was active one, yet we didn't found row
        visRow = idx-1;
      }
      let go = gs[vi.obj.id];
      if (typeof(go) === "object") {
        // restore "open" state
        if (go.open != vi.open) {
          if (go.open) this.openAt(idx); else this.closeAt(idx);
        }
      }
      lastGIdx = idx;
      lastGid = vi.obj.id;
      ingIdx = 0;
    } else {
      // feed
      if (lastGid === state.curGid && ingIdx === state.curIdx) visRow = idx;
      ++ingIdx;
    }
    ++idx;
  }
  if (this.treebox) {
    if (visRow >= 0) {
      this.selection.select(visRow);
      this.treebox.ensureRowIsVisible(visRow);
    }
    this.treebox.endUpdateBatch();
  }
};


////////////////////////////////////////////////////////////////////////////////
// fix nextSibIndex for groups, parentIndex for feeds
FeedTreeModel.prototype.fixVisDataFrom = function (idx) {
  if (idx < 0) idx = 0;
  let vii = this.visItems;
  if (idx >= vii.length) return;
  // if we are not at container, go up to container
  if (!vii[idx].container) {
    while (--idx >= 0 && !vii[idx].container) {}
    if (idx < 0) throw new Error("the thing that should not be");
    vii[idx].parentIndex = -1; // just in case, heh
  }
  // process container contents
  let ctIdx = idx; // index of last seen container
  for (++idx; idx < vii.length; ++idx) {
    let v = vii[idx];
    if (v.container) {
      // new container, fix `nextSibIndex` in previous
      vii[ctIdx].nextSibIndex = idx;
      ctIdx = idx; // remember this container's index
      v.parentIndex = -1; // just in case, heh
    } else {
      // content
      v.parentIndex = ctIdx;
    }
  }
  vii[ctIdx].nextSibIndex = -1; // last container
};


// add group with object from `getGroupsAsync()`
//   int id       (group id)
//   string title  (group title; "" means "default top-level group", always presented)
//   int treeindex
//   int[] feeds  (id of feeds in this group)
// optional:
//   open
FeedTreeModel.prototype.addGroup = function (grp) {
  if (grp.id in this.cacheGroupsById) throw new Error("use `modifyGroup()` to change existing group");
  let vii = this.visItems;

  // underlying group object
  let uobj = {
    id: grp.id,
    title: grp.title,
    treeindex: (grp.id !== 0 ? grp.treeindex : 0x7ffffff), // "uncategorized" is always last one
    countUnread: 0,
    countTotal: 0,
    feeds: grp.feeds,
    hasFeeds: false,
  };
  this.cacheGroupsById[uobj.id] = uobj;

  // find desired group position in visItems
  let insertAt = vii.length;

  let prevGroupIdx = -1; // previous container index, if any
  for (let [idx, v] in Iterator(vii)) {
    if (v.container) {
      if (v.obj.treeindex > uobj.treeindex) { insertAt = idx; break; }
      prevGroupIdx = idx;
    }
  }

  // store current selected row, so we can fix this later
  //let oidx = (this.treebox ? this.treebox.currentIndex : -1);

  // insert group visItem
  let insIdx = insertAt;
  let gvi = {
    obj: uobj,
    container: true,
    open: ("open" in grp ? !!grp.open : (grp.id != 0)), // "group zero" is always closed
    parentGid: -1,
    parentIndex: -1,
    nextSibIndex: -1, // will be fixed later
    icon: null,
  };
  vii.splice(insIdx++, 0, gvi);

  // insert feeds belonging to this group, if any, calc counts
  let prevFeedSib = -1;
  for (let [fid, feed] of Iterator(uobj.feeds)) {
    if (typeof(feed) !== "object") continue;
    // if group is open, insert feed item
    if (uobj.open) {
      vii.splice(insIdx++, 0, {
        obj: feed,
        container: false,
        open: false,
        parentGid: uobj.id,
        parentIndex: insertAt,
        icon: null,
        text: buildObjText(feed),
      });
    }
    uobj.hasFeeds = true;
    uobj.countUnread += feed.countUnread;
    uobj.countTotal += feed.countTotal;
  }

  // fix text, as we calculated counts
  gvi.text = buildObjText(uobj, "uncategorized");

  // fix nextSibIndex of previous group
  if (prevGroupIdx >= 0) vii[prevGroupIdx].nextSibIndex = insertAt;

  // fix other things in underlying items
  this.fixVisDataFrom(insIdx);

  // update treebox
  if (this.treebox) {
    this.treebox.rowCountChanged(insertAt, insIdx-insertAt);
    /*
    if (oidx >= 0) {
      if (oidx >= insertAt) {
        oidx += insCount;
        this.treebox.currentIndex = oidx;
      }
      this.treebox.ensureRowIsVisible(oidx);
    }
    */
  }
};


FeedTreeModel.prototype.dumpVisItems = function () {
  this.fixVisDataFrom(0);
  let res = "";
  for (let [idx, vi] in Iterator(this.visItems)) {
    res +=
      "\nidx="+idx+
      "\n  id="+vi.obj.id+
      "\n  container="+vi.container+
      "\n  open="+vi.open+
      "\n  parentGid="+vi.parentGid+
      "\n  parentIndex="+vi.parentIndex+
      "\n  nextSibIndex="+vi.nextSibIndex;
  }
  debuglog(res);
  return res;
};


// add feed with object from `getFeedsAsync()`
//   int id       (group id)
//   string url
//   string homeurl (can be "")
//   string icon    ("data:" or "")
//   string title
//   string descr
//   int flags
//   int upinterval
//   int uplast
//   int upfailed
//   int total
//   int unread
FeedTreeModel.prototype.addFeed = function (feed) {
  if (feed.id in this.cacheFeedsById) throw new Error("use `modifyFeed()` to change existing feed");
  let vii = this.visItems;

  if (this.treebox) this.treebox.beginUpdateBatch();

  // underlying feed object
  let uobj = {
    id: feed.id,
    title: feed.title,
    countUnread: feed.unread||0,
    countTotal: feed.total||0,
    icon: feed.icon,
    wasError: false,
    flags: feed.flags,
    upinterval: feed.upinterval,
  };
  this.cacheFeedsById[uobj.id] = uobj;
  let viicon = ((feed.upfailed||0) ? "chrome://korenews/skin/images/brokenFeed.png" : null);

  //debuglog("adding feed: ", JSON.stringify(uobj));

  // store current selected row, so we can fix this later
  //let oidx = (this.treebox ? this.treebox.currentIndex : -1);

  // go thru all visItems and add feed in groups, if it's necessary
  let firstChangedIdx = -1; // index of first change, so we can fix items data later
  let idx = 0;
  while (idx < vii.length) {
    let vig = vii[idx];
    // skip feeds
    if (vig.container) {
      let pos = vig.obj.feeds.indexOf(uobj.id);
      if (pos < 0) { ++idx; continue; } // skip this group, as it doesn't contains our feed
      let gid = vig.obj.id;
      let vidx = idx;
      if (firstChangedIdx < 0) firstChangedIdx = idx;
      // build cache of all fids that should follow this one in group
      let fc = {};
      while (pos++ < vig.obj.feeds.length) fc[vig.obj.feeds[pos]] = true;
      // if this group is open, update it
      if (vig.open) {
        // find place to insert
        while (++idx < vii.length && !vii[idx].container) {
          if (vii[idx].obj.id in fc) break;
        }
        // insert visItem at `idx`
        //if (oidx >= idx) ++oidx; // move current visible item down, if necessary
        vii.splice(idx, 0, {
          obj: uobj,
          container: false,
          open: false,
          parentGid: gid,
          parentIndex: vidx,
          icon: viicon,
          text: buildObjText(uobj),
        });
        if (this.treebox) this.treebox.rowCountChanged(idx, 1);
      }
      ++idx;
      // update group data
      vig.hasFeeds = true;
      if (uobj.countUnread || uobj.countTotal) {
        vig.obj.countUnread += uobj.countUnread;
        vig.obj.countTotal += uobj.countTotal;
        vig.text = buildObjText(vig.obj, "uncategorized");
        if (this.treebox) this.treebox.invalidateRow(vidx);
      }
    } else {
      ++idx;
    }
  }

  // was any changes?
  if (firstChangedIdx >= 0) {
    this.fixVisDataFrom(firstChangedIdx);
    /*
    if (this.treebox && oidx >= 0) {
      this.treebox.currentIndex = oidx;
      this.treebox.ensureRowIsVisible(oidx);
    }
    */
  }

  if (this.treebox) this.treebox.endUpdateBatch();
};


FeedTreeModel.prototype.removeFeedById = function (fid) {
  let feed = this.cacheFeedsById[fid];
  if (!feed) return; // no such feed

  let vii = this.visItems;

  // remove from cache
  delete this.cacheFeedsById[fid];

  if (this.treebox) this.treebox.beginUpdateBatch();

  // now iterate thru visItems and check each group
  // here i'm cheating, as i'm really lazy:
  // to remove feed, i'm simply closing group,
  // removing feed from `feeds` array, and reopening group
  // this distort selections, but user shouldn't remove feeds very often
  let idx = 0;
  while (idx < vii.length) {
    let vig = vii[idx];
    if (vig.container) {
      let wasOpen = !!vig.open;
      let fidx = vig.obj.feeds.indexOf(fid);
      if (fidx >= 0) {
        // here! close group
        if (wasOpen) this.closeAt(idx);
        // now remove feed
        vig.obj.feeds.splice(fidx, 1);
        if (vig.obj.feeds.length == 0) {
          // no more feeds in this group, close it and mark as empty
          vig.hasFeeds = false;
          wasOpen = false;
        }
        // fix totals
        vig.obj.countUnread -= feed.countUnread;
        vig.obj.countTotal -= feed.countTotal;
        vig.text = buildObjText(vig.obj, "uncategorized");
        // and reopen group, if necessary
        if (wasOpen) {
          this.openAt(idx);
        } else {
          if (this.treebox) this.treebox.invalidateRow(idx); // redraw totals
        }
      }
    }
    ++idx;
  }

  if (this.treebox) this.treebox.endUpdateBatch();
};


FeedTreeModel.prototype.removeGroupById = function (gid) {
  let grp = this.cacheGroupsById[gid];
  if (!grp) return; // no such feed

  let vii = this.visItems;

  if (this.treebox) this.treebox.beginUpdateBatch();

  let idx = 0;
  while (idx < vii.length) {
    let vi = vii[idx];
    if (vi.container && vi.obj === grp) {
      // remove from cache
      delete this.cacheGroupsById[gid];
      this.closeAt(idx); // ease work
      vii.splice(idx, 1);
      if (this.treebox) this.treebox.rowCountChanged(idx, -1);
      this.fixVisDataFrom(idx);
      break;
    }
    ++idx;
  }

  if (this.treebox) this.treebox.endUpdateBatch();
};


// same cheat again: close, then open
FeedTreeModel.prototype.moveGroupUp = function (gid) {
  let uobj = this.cacheGroupsById[gid];
  if (!uobj) return;

  // find group in vislist
  let vii = this.visItems;

  if (this.treebox) this.treebox.beginUpdateBatch();

  let pidx = -1;
  for (let [idx, vi] in Iterator(vii)) {
    if (vi.container) {
      if (vi.obj.id === gid) {
        // found it! now move it right before pidx
        if (pidx < 0) break; // nothing to do
        let wasOpen = vi.open;
        this.closeAt(idx);
        // remove...
        vi = vii.splice(idx, 1)[0];
        this.treebox.rowCountChanged(idx, -1);
        // ...and insert
        vii.splice(pidx, 0, vi);
        this.treebox.rowCountChanged(pidx, 1);
        // fix other things in underlying items
        this.fixVisDataFrom(pidx);
        if (wasOpen) this.openAt(pidx);
        break;
      } else {
        // remember last seen group index
        pidx = idx;
      }
    }
  }

  if (this.treebox) this.treebox.endUpdateBatch();
};


// same cheat again: close, then open
FeedTreeModel.prototype.moveGroupDown = function (gid) {
  let uobj = this.cacheGroupsById[gid];
  if (!uobj) return;

  // find group in vislist
  let vii = this.visItems;

  if (this.treebox) this.treebox.beginUpdateBatch();

  for (let [idx, vi] in Iterator(vii)) {
    if (vi.container) {
      if (vi.obj.id === gid) {
        // found it! now find next group start
        let nidx = idx+1;
        while (nidx < vii.length && !vii[nidx].container) ++nidx;
        if (nidx >= vii.length) break; // nothing to do
        // find next group end
        while (nidx < vii.length && !vii[nidx].container) ++nidx;
        // now move it right before nidx
        let wasOpen = vi.open;
        this.closeAt(idx);
        // remove...
        vi = vii.splice(idx, 1)[0];
        this.treebox.rowCountChanged(idx, -1);
        // ...and insert
        vii.splice(nidx, 0, vi);
        this.treebox.rowCountChanged(nidx, 1);
        // fix other things in underlying items
        this.fixVisDataFrom(idx);
        if (wasOpen) this.openAt(nidx);
        break;
      }
    }
  }

  if (this.treebox) this.treebox.endUpdateBatch();
};


FeedTreeModel.prototype.openAt = function (idx) {
  //this.visItems[idx].open = true; return;
  let vii = this.visItems;
  if (idx < 0 || idx >= vii.length) return;
  if (!vii[idx].container || !vii[idx].hasFeeds) return;
  if (vii[idx].open) return;
  vii[idx].open = true;
  // now we should insert feeds in the group
  let vi = vii[idx];
  let gid = vi.obj.id;
  let insIdx = idx+1;
  for (let fid of vi.obj.feeds) {
    let feed = this.cacheFeedsById[fid];
    if (typeof(feed) !== "object") continue;
    let fvi = {
      obj: feed,
      container: false,
      open: false,
      parentGid: gid,
      parentIndex: idx,
      icon: null,
      text: buildObjText(feed),
    };
    vii.splice(insIdx++, 0, fvi);
  }
  if (insIdx > idx+1) {
    // something was changed
    this.fixVisDataFrom(idx);
    if (this.treebox) {
      this.treebox.rowCountChanged(idx+1, insIdx-idx-1);
      this.treebox.invalidateRow(idx); // this will redraw state icon
    }
  }
};


FeedTreeModel.prototype.closeAt = function (idx) {
  //this.visItems[idx].open = false; return;
  let vii = this.visItems;
  if (idx < 0 || idx >= vii.length) return;
  if (!vii[idx].container || !vii[idx].hasFeeds) return;
  if (!vii[idx].open) return;
  vii[idx].open = false;
  // ok, we have something to remove here
  let eidx = ++idx;
  while (eidx < vii.length && !vii[eidx].container) ++eidx;
  vii.splice(idx, eidx-idx);
  this.fixVisDataFrom(idx-1);
  if (this.treebox) {
    this.treebox.rowCountChanged(idx, -(eidx-idx));
    this.treebox.invalidateRow(idx-1); // this will redraw state icon
  }
};


////////////////////////////////////////////////////////////////////////////////
FeedTreeModel.prototype.openAllGroups = function () {
  if (this.treebox) this.treebox.beginUpdateBatch();
  let vii = this.visItems;
  for (let idx = 0; idx < vii.length; ++idx) {
    let vi = vii[idx];
    if (!vi.container || vi.open) continue;
    this.openAt(idx);
  }
  if (this.treebox) this.treebox.endUpdateBatch();
};


FeedTreeModel.prototype.closeAllGroups = function () {
  if (this.treebox) this.treebox.beginUpdateBatch();
  let vii = this.visItems;
  for (let idx = 0; idx < vii.length; ++idx) {
    let vi = vii[idx];
    if (!vi.container || !vi.open) continue;
    this.closeAt(idx);
  }
  if (this.treebox) this.treebox.endUpdateBatch();
};


FeedTreeModel.prototype.openGroupsWithUnreads = function () {
  if (this.treebox) this.treebox.beginUpdateBatch();
  let vii = this.visItems;
  for (let idx = 0; idx < vii.length; ++idx) {
    let vi = vii[idx];
    if (!vi.container || vi.open) continue;
    if (vi.obj.countUnread < 1) continue;
    this.openAt(idx);
  }
  if (this.treebox) this.treebox.endUpdateBatch();
};


FeedTreeModel.prototype.closeGroupsWithoutUnreads = function () {
  if (this.treebox) this.treebox.beginUpdateBatch();
  let vii = this.visItems;
  for (let idx = 0; idx < vii.length; ++idx) {
    let vi = vii[idx];
    if (!vi.container || !vi.open) continue;
    if (vi.obj.countUnread > 0) continue;
    this.closeAt(idx);
  }
  if (this.treebox) this.treebox.endUpdateBatch();
};


////////////////////////////////////////////////////////////////////////////////
// action: "copy", "move"
// return null or object:
//   int groupUpDown: <0: move svi group up -n positions; >0: move svi group down n positions
//   int groupId    : group id for up/down or -1
//   GUObj[]  gup   : group updates
// GUObj:
//   int gid
//   int[] feeds: new list of feeds
//TODO: fix tree model
FeedTreeModel.prototype.draggedOnto = function (svi, dvi, action) {
  if (!svi || !dvi) return null;
  let vii = this.visItems;
  // find svi and dvi indexes
  let sidx = -1, didx = -1, len = this.rowCount;
  for (let [idx, vi] in Iterator(vii)) {
    if (vi.obj === svi.obj) sidx = idx;
    if (vi.obj === dvi.obj) didx = idx;
    if (sidx >= 0 && didx >= 0) break;
  }
  if (sidx < 0 || didx < 0) return null; // alas
  if (sidx === didx) return null; // nothing to do
  let res = {groupUpDown:0, groupId:-1, gup:[]};
  if (svi.container) {
    // move group
    if (!dvi.container) {
      // onto feed: put before next group
      while (didx < len && !vii[didx].container) ++didx;
      if (didx >= len) return null; // alas
      dvi = vii[didx];
    }
    // build group list
    let glist = [], gls = -1, gld = -1;
    for (let vi of vii) {
      if (vi.container) {
        if (vi.obj === svi.obj) gls = glist.length;
        if (vi.obj === dvi.obj) gld = glist.length;
        glist.push(vi.obj);
      }
    }
    if (gls < 0 || gld < 0 || glist[gls].id === 0 || gls === gld) return null;
    res.groupId = svi.obj.id;
    res.groupUpDown = gld-gls;
  } else {
    // move feed
    let fid = svi.obj.id;
    if (dvi.container) {
      // onto group: make it last in group
      let gid = dvi.obj.id;
      let di = dvi.obj.feeds.indexOf(fid);
      if (di < 0) {
        // wasn't here
        let nf = dvi.obj.feeds.slice();
        nf.push(fid);
        res.gup.push({gid:gid, feeds:nf});
      } else if (di != dvi.obj.feeds.length-1) {
        // was here
        let nf = dvi.obj.feeds.slice();
        nf.splice(di, 1);
        nf.push(fid);
        res.gup.push({gid:gid, feeds:nf});
      }
      if (action === "move") {
        let sgp = vii[svi.parentIndex];
        if (sgp.obj.id != gid) {
          let i = sgp.obj.feeds.indexOf(fid);
          if (i >= 0) {
            let nf = sgp.obj.feeds.slice();
            nf.splice(i, 1);
            res.gup.push({gid:sgp.obj.id, feeds:nf});
          }
        }
      }
    } else {
      // onto feed: insert it before
      let sgp = vii[svi.parentIndex];
      let dgp = vii[dvi.parentIndex];
      if (sgp.obj.id === dgp.obj.id) {
        // same group
        if (fid === dvi.obj.id) return null; // nothing to do
        let nf = sgp.obj.feeds.slice();
        let si = nf.indexOf(fid);
        let ids = nf.indexOf(fid);
        if (ids+1 === nf.indexOf(dvi.obj.id)) return null; // nothing to do
        nf.splice(ids, 1);
        nf.splice(nf.indexOf(dvi.obj.id), 0, fid);
        res.gup.push({gid:sgp.obj.id, feeds:nf});
      } else {
        // different groups
        // add to new group
        {
          let nf = dgp.obj.feeds.slice();
          nf.splice(nf.indexOf(dvi.obj.id), 0, fid);
          res.gup.push({gid:dgp.obj.id, feeds:nf});
        }
        if (action === "move") {
          let nf = sgp.obj.feeds.slice();
          nf.splice(nf.indexOf(fid), 1);
          res.gup.push({gid:sgp.obj.id, feeds:nf});
        }
      }
    }
  }
  return res;
};


////////////////////////////////////////////////////////////////////////////////
FeedTreeModel.prototype.getDataAt = function (index) (index < 0 || index >= this.visItems.length ? null : this.visItems[index]);


// yields underlying object
FeedTreeModel.prototype.feedIterator = function () {
  for (let [id, fobj] in Iterator(this.cacheFeedsById)) yield fobj;
};


// yields underlying object
FeedTreeModel.prototype.feedInGroupIterator = function (gid) {
  if (typeof(gid) !== "number") return;
  let grp = this.cacheGroupsById[gid];
  if (typeof(grp) === "undefined") return;
  for (let [fid, feed] in Iterator(this.cacheFeedsById)) {
    if (grp.feeds.indexOf(fid) >= 0) yield feed;
  }
};


FeedTreeModel.prototype.getGroupUObjById = function (gid) {
  if (typeof(gid) !== "number") return null;
  let grp = this.cacheGroupsById[gid];
  if (typeof(grp) === "undefined") return null;
  return grp;
};


FeedTreeModel.prototype.getFeedUObjById = function (fid) {
  if (typeof(fid) !== "number") return null;
  let feed = this.cacheFeedsById[fid];
  if (typeof(feed) === "undefined") return null;
  return feed;
};


FeedTreeModel.prototype.getGroupUObjByRow = function (row) {
  if (typeof(row) !== "number") return null;
  if (row < 0) return null;
  let vii = this.visItems, len = this.rowCount;
  if (row >= len) return null;
  if (vii[row].container) return vii[row].obj;
  while (row >= 0 && !vii[row].container) --row;
  if (row < 0) return null;
  return vii[row].obj;
};


FeedTreeModel.prototype.invalidateFeedById = function (fid) {
  if (!this.treebox) return;
  let feed = this.cacheFeedsById[fid];
  if (typeof(feed) !== "object") return; // unknown feed
  for (let [idx, vi] in Iterator(this.visItems)) {
    if (!vi.container && vi.obj.id === fid) {
      this.treebox.invalidateRow(idx);
    }
  }
};


FeedTreeModel.prototype.setFeedTotalsById = function (fid, unread, total, waserr) {
  if (typeof(waserr) === "undefined") waserr = false;
  let feed = this.cacheFeedsById[fid];
  if (typeof(feed) !== "object") return; // unknown feed
  // calculate deltas -- we'll need 'em for updating groups
  let deltaUnread = unread-feed.countUnread;
  let deltaTotal = total-feed.countTotal;
  // fix counts for fid
  feed.countUnread = unread;
  feed.countTotal = total;
  // fix counts for groups
  let changedGroups = {};
  let groupCount = 0;
  for (let [gid, grp] in Iterator(this.cacheGroupsById)) {
    if (grp.feeds.indexOf(fid) >= 0) {
      changedGroups[gid] = true;
      ++groupCount;
      grp.countUnread += deltaUnread;
      grp.countTotal += deltaTotal;
    }
  }
  if (groupCount == 0) return; // nothing more to do
  // now fix visible items
  if (this.treebox) this.treebox.beginUpdateBatch();
  for (let [idx, vi] in Iterator(this.visItems)) {
    if (vi.container) {
      // group
      if (vi.obj.id in changedGroups) {
        vi.text = buildObjText(vi.obj, "uncategorized");
        if (this.treebox) this.treebox.invalidateRow(idx);
      }
    } else if (vi.obj.id === fid) {
      // feed
      vi.text = buildObjText(vi.obj);
      if (waserr) {
        vi.icon = "chrome://korenews/skin/images/brokenFeed.png";
      } else {
        vi.icon = null;
      }
      if (this.treebox) this.treebox.invalidateRow(idx);
    }
  }
  if (this.treebox) this.treebox.endUpdateBatch();
};


FeedTreeModel.prototype.setUpdateStatus = function (fid, status) {
  if (typeof(status) === "undefined") status = false;
  let feed = this.cacheFeedsById[fid];
  if (typeof(feed) !== "object") return; // unknown feed
  if (this.treebox) this.treebox.beginUpdateBatch();
  for (let [idx, vi] in Iterator(this.visItems)) {
    if (!vi.container && vi.obj.id === fid) {
      if (status === "error") {
        vi.icon = "chrome://korenews/skin/images/brokenFeed.png";
      } else if (status) {
        vi.icon = "chrome://korenews/skin/images/download.png";
      } else {
        vi.icon = null;
      }
      if (this.treebox) this.treebox.invalidateRow(idx);
    }
  }
  if (this.treebox) this.treebox.endUpdateBatch();
};


FeedTreeModel.prototype.setFeedIconById = function (fid, icon) {
  let feed = this.cacheFeedsById[fid];
  if (typeof(feed) !== "object") return; // unknown feed
  feed.icon = icon;
  if (this.treebox) this.treebox.beginUpdateBatch();
  for (let [idx, vi] in Iterator(this.visItems)) {
    if (!vi.container && vi.obj.id === fid) {
      if (this.treebox) this.treebox.invalidateRow(idx);
    }
  }
  if (this.treebox) this.treebox.endUpdateBatch();
};


FeedTreeModel.prototype.setFeedTitleById = function (fid, title) {
  let feed = this.cacheFeedsById[fid];
  if (typeof(feed) !== "object") return; // unknown feed
  feed.title = title;
  // fix visible items
  if (this.treebox) this.treebox.beginUpdateBatch();
  for (let [idx, vi] in Iterator(this.visItems)) {
    if (!vi.container && vi.obj.id === fid) {
      vi.text = buildObjText(vi.obj);
      if (this.treebox) this.treebox.invalidateRow(idx);
    }
  }
  if (this.treebox) this.treebox.endUpdateBatch();
};


////////////////////////////////////////////////////////////////////////////////
// treeview model
// the total number of rows in the tree (including the offscreen rows)
//Object.defineProperty(this, "rowCount", {get: function () this.visItems.length});

//boolean canDrop(in long index, in long orientation, in nsIDOMDataTransfer dataTransfer);
FeedTreeModel.prototype.canDrop = function (index, orientation, dataTransfer) false;

//void cycleCell(in long row, in nsITreeColumn col);
FeedTreeModel.prototype.cycleCell = function (row, col) {};

//void cycleHeader(in nsITreeColumn col);
FeedTreeModel.prototype.cycleHeader = function (col) {};

//void drop(in long row, in long orientation, in nsIDOMDataTransfer dataTransfer);
FeedTreeModel.prototype.drop = function (row, orientation, dataTransfer) {};

//AString getCellProperties(in long row, in nsITreeColumn col);
// return spalce-delimited string of properties
FeedTreeModel.prototype.getCellProperties = function (row, col) {
  let props = this.getRowProperties(row);
  let uobj = this.visItems[row].obj;
  if (uobj.countUnread > 0) props += " unread";
  let vi = this.visItems[row];
  if (!vi.container && (vi.obj.flags&flags.FFLAG.NO_UPDATES) != 0) props += " noupdates";
  props += " favicon";
  return props;
};

//AString getCellText(in long row, in nsITreeColumn col);
FeedTreeModel.prototype.getCellText = function (row, col) this.visItems[row].text;

//AString getCellValue(in long row, in nsITreeColumn col);
// this method is only called for columns of type other than text

//AString getColumnProperties(in nsITreeColumn col);
FeedTreeModel.prototype.getColumnProperties = function (col) "";

//AString getImageSrc(in long row, in nsITreeColumn col);
FeedTreeModel.prototype.getImageSrc = function (row, col) {
  let vi = this.visItems[row];
  if (vi.container) return (vi.open ? "chrome://korenews/skin/images/folderOpened.png" : "chrome://korenews/skin/images/folderClosed.png");
  if (vi.icon) return vi.icon;
  return (vi.obj.icon ? vi.obj.icon : "chrome://korenews/skin/images/feed.png");
  //return null;
};

//long getLevel(in long index);
FeedTreeModel.prototype.getLevel = function (index) (this.visItems[index].container ? 0 : 1);

//long getParentIndex(in long rowIndex);
FeedTreeModel.prototype.getParentIndex = function (rowIndex) (rowIndex >= 0 && rowIndex < this.visItems.length ? this.visItems[rowIndex].parentIndex : -1);

//long getProgressMode(in long row, in nsITreeColumn col);
// this method is only called for columns of type progressmeter

//AString getRowProperties(in long index);
FeedTreeModel.prototype.getRowProperties = function (index) "";

//boolean hasNextSibling(in long rowIndex, in long afterIndex);
// used to determine if the row at rowIndex has a nextSibling that occurs *after* the index specified by afterIndex
FeedTreeModel.prototype.hasNextSibling = function (rowIndex, afterIndex) {
  let vii = this.visItems;
  if (afterIndex+1 >= vii.length) return false;
  let vi = vii[rowIndex];
  if (vi.container) {
    return (vi.nextSibIndex == afterIndex+1);
  } else {
    // for feeds: nex item should be feed with same parentIndex
    return (vii[afterIndex+1].parentIndex == vi.parentIndex);
  }
};

//boolean isContainer(in long index);
FeedTreeModel.prototype.isContainer = function (index) !!this.visItems[index].container;

//boolean isContainerEmpty(in long index);
FeedTreeModel.prototype.isContainerEmpty = function (index) (!this.visItems[index].hasFeeds);

//boolean isContainerOpen(in long index);
FeedTreeModel.prototype.isContainerOpen = function (index) !!this.visItems[index].open;

//boolean isEditable(in long row, in nsITreeColumn col);
FeedTreeModel.prototype.isEditable = function (row, col) false; //TODO: true to allow inline editing of feed titles?

//boolean isSelectable(in long row, in nsITreeColumn col);
// this method is only called if the selection type is cell or text

//boolean isSeparator(in long index);
FeedTreeModel.prototype.isSeparator = function (index) false;

//boolean isSorted();
FeedTreeModel.prototype.isSorted = function () false;

//void performAction(in wstring action);
FeedTreeModel.prototype.performAction = function (action) {
  debuglog("feedtree action: '", action, "'");
};

//void performActionOnCell(in wstring action, in long row, in nsITreeColumn col);

//void performActionOnRow(in wstring action, in long row);

//void selectionChanged();
FeedTreeModel.prototype.selectionChanged = function () {
  debuglog("feedtree: selection changed");
};

//void setCellText(in long row, in nsITreeColumn col, in AString value);
// this method is called when the contents of the cell have been edited by the user

//void setCellValue(in long row, in nsITreeColumn col, in AString value);
// this method is only called for columns of type other than text

//void setTree(in nsITreeBoxObject tree);
FeedTreeModel.prototype.setTree = function (treebox) {
  this.treebox = treebox;
  /*
  if (treebox) {
    if (this.visItems) this.treebox.rowCountChanged(0, this.visItems.length);
    this.treebox.invalidate();
  }
  */
};

//void toggleOpenState(in long index);
FeedTreeModel.prototype.toggleOpenState = function (index) {
  if (this.isContainerEmpty(index)) return;
  if (this.visItems[index].open) this.closeAt(index); else this.openAt(index);
};


////////////////////////////////////////////////////////////////////////////////
exports.FeedTreeModel = FeedTreeModel;
