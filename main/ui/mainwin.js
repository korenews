/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
require("utils/utils");
let signals = require("signals");
//const opml = require("opml");

let feedUpdater = require("feedupdater");
let {queueFavIconUpdate} = require("faviconloader");

let FeedsDB = require("feedsdb");

let ifutils = require("utils/iframe");
let flags = require("flags");

let {openEditor} = require("mainjs:ui/editorutils");


////////////////////////////////////////////////////////////////////////////////
// we are supporting only one window for now, so...
let window = null;
let document = null;
let winitems = null;

let feedtreeView = null;
let arttreeView = null;
let artFeedId = -1; // current feed id for article view

let artQuery = null;
let artQueryId = -1;
let artObj = null; // current article object

let dragObj = null;
let ftState = null;


////////////////////////////////////////////////////////////////////////////////
// if you specify URL, this will put URL to clipboard (with title from `text`)
function putToClipboard (text, url) {
  if (typeof(text) !== "string") return; // alas

  let trans = Cc["@mozilla.org/widget/transferable;1"].createInstance(Ci.nsITransferable);
  trans.init(document.defaultView.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIWebNavigation).QueryInterface(Ci.nsILoadContext));

  function addData (type, str) {
    let ss = Cc["@mozilla.org/supports-string;1"].createInstance(Ci.nsISupportsString);
    ss.data = str;
    let len = str.length*(type === "text/unicode" ? 2 : 1);
    trans.addDataFlavor(type);
    trans.setTransferData(type, ss, len);
  }

  if (typeof(url) === "string") {
    addData("text/x-moz-url", url+"\n"+text);
    addData("text/plain", url);
    addData("text/unicode", url);
  } else {
    addData("text/plain", text);
    addData("text/unicode", text);
  }

  let clip = Cc["@mozilla.org/widget/clipboard;1"].getService(Ci.nsIClipboard);
  if (clip.supportsSelectionClipboard()) clip.setData(trans, null, 0);
  clip.setData(trans, null, 1);
}


////////////////////////////////////////////////////////////////////////////////
function cancelArtQuery () {
  if (artQuery) artQuery.cancel();
  artQuery = null;
  artQueryId = -1;
  artObj = null;
}


function setupArtTreeViewFor (fid) {
  if (typeof(fid) === "undefined") fid = -1;
  if (fid !== artFeedId && artFeedId > 0) FeedsDB.db.deleteDelMarkedArticlesForAsync(artFeedId);
  //FeedsDB.db.deleteDelMarkedArticlesForAsync(fid);
  artFeedId = fid;
  cancelArtQuery();
  arttreeView.cleanup();
}


////////////////////////////////////////////////////////////////////////////////
let signalList = {
  "total-unread-update": signalUpdateTotalUnread,
  "model-changed": signalModelChanged,
  "model-recreate": signalModelRecreate,
  "feed-update-start": signalFeedUpdateStart,
  "feed-update-error": signalFeedUpdateError,
  "feed-update-cancelled": signalFeedUpdateCancelled,
  "feed-update-complete": signalFeedUpdateComplete,
  "feed-icon-updated": signalFeedIconUpdated,
  "feed-totals-updated": signalFeedTotalsUpdated,
  "feed-title-updated": signalFeedTitleUpdated,
  "article-mark-set": signalArticleMarkChanged,
  "article-state-set": signalArticleStateChanged,
};


////////////////////////////////////////////////////////////////////////////////
let {FeedTreeModel} = require("mainjs:ui/models/feedtree");
let {ArtTreeModel} = require("mainjs:ui/models/arttree");


////////////////////////////////////////////////////////////////////////////////
// go thru all window elements, collect those with ids and setup object
// note that this will anchor elements, so make sure that you'll nullify it
function buildWinItems (win) {
  let wi = {};
  let errors = "";
  let conService = Cc["@mozilla.org/consoleservice;1"].getService(Ci.nsIConsoleService);

  function doElements (el, indent) {
    for (; el; el = el.nextSibling) {
      //conService.logStringMessage("indent="+indent+"; el="+el+" <"+el.tagName+">");
      /*
      if (el.tagName === "window") {
        if ("window" in wi) {
        } else {
          wi["__window__"] = el;
        }
      }
      */
      if (typeof(el.getAttribute) === "function") {
        let id = el.getAttribute("id");
        if (id) {
          // try to normalize it
          let name = id.replace(/[-.]/g, "_");
          if (name in wi) {
            // first wins
            errors += "\n  duplicate id: '"+id+"' ("+name+") for '"+el.tagName+"'";
          } else {
            wi[name] = el;
          }
        }
      }
      doElements(el.firstChild, indent+1);
    }
  }

  doElements(win.document, 0);
  if (errors) logError("ERRORS in ", win.document.documentURI, errors);
  return wi;
}


////////////////////////////////////////////////////////////////////////////////
function setFeedTreeTitle (unread) {
  if (!window || !feedtreeView) return;
  if (typeof(unread) === "undefined") {
    FeedsDB.db.queryTotalUnread(); // send async query
  } else {
    winitems.feed_tree_titlebar.setAttribute("label", "Feeds"+(unread > 0 ? " ("+unread+")" : ""));
  }
}


////////////////////////////////////////////////////////////////////////////////
function populateModel (win) {
  let doneC = 0;

  function doneAsync () {
    if (!feedtreeView) return;
    if (++doneC == 2) {
      //feedtreeView.dumpVisItems();
      if (ftState) {
        feedtreeView.restoreState(ftState);
        ftState = null;
      } else {
        feedtreeView.openGroupsWithUnreads();
      }
    }
  }

  // get groups
  FeedsDB.db.getGroupsAsync(function (item) {
    if (window !== win || !feedtreeView) return; // this window is dead
    if (!item) { doneAsync(); return; }
    item.open = false;
    feedtreeView.addGroup(item);
  });

  // get feeds
  FeedsDB.db.getFeedsAsync(function (item) {
    if (window !== win || !feedtreeView) return; // this window is dead
    if (!item) { doneAsync(); return; }
    feedtreeView.addFeed(item);
  });
}


exports.onNewWindow = function (win) {
  debuglog("initializing KoreNews UI");
  if (window) return false;
  // it doesn't matter if we register listeners more than once
  for (let [name, cb] in Iterator(signalList)) signals.addListener(name, cb);
  ftState = null;
  window = win;
  document = win.document;
  feedtreeView = new FeedTreeModel();
  arttreeView = new ArtTreeModel();
  arttreeView.onMarkToggled = onArticleMarkToggled;
  populateModel(win);
  return true;
};


exports.onWindowUnloaded = function (win) {
  debuglog("KoreNews UI unloaded");
  if (win === window) {
    if (artFeedId > 0) FeedsDB.db.deleteDelMarkedArticlesForAsync(artFeedId); // remove delmarked
    window = null;
    document = null;
    feedtreeView = null;
    arttreeView = null;
    winitems = null;
    dragObj = null;
    ftState = null;
    cancelArtQuery();
    artFeedId = -1;
    for (let [name, cb] in Iterator(signals)) signals.removeListener(name, cb);
  }
};


exports.onWindowLoaded = function (win) {
  debuglog("KoreNews UI loaded");
  winitems = buildWinItems(win);

  {
    let sss = Components.classes["@mozilla.org/content/style-sheet-service;1"].getService(Components.interfaces.nsIStyleSheetService);
    let newFileURI = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService).newFileURI;
    let ioSrv = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
    let conService = Cc["@mozilla.org/consoleservice;1"].getService(Ci.nsIConsoleService);

    let flcss = userdirs.HtmlDir;
    flcss.append("korenews.css");

    if (flcss.exists() && flcss.isFile() && flcss.isReadable()) {
      try {
        let uri = ioSrv.newURI(newFileURI(flcss).spec, null, null);
        //conService.logStringMessage("*** got URI: <"+uri.spec+"> ("+sss.USER_SHEET+")");
        if (!sss.sheetRegistered(uri, sss.USER_SHEET)) {
          //conService.logStringMessage("*** adding sheet from: <"+uri.spec+">");
          sss.loadAndRegisterSheet(uri, sss.USER_SHEET);
        }
        if (!sss.sheetRegistered(uri, sss.USER_SHEET)) conService.logStringMessage("SHIIIII...");
      } catch (e) {
        //conService.logStringMessage("FUUUUUUCK!"+e.msg);
      }
    } else {
      try {
        let uri = ioSrv.newURI(newFileURI(flcss).spec, null, null);
        //conService.logStringMessage("*** got URI: <"+uri.spec+"> ("+sss.USER_SHEET+")");
        if (sss.sheetRegistered(uri, sss.USER_SHEET)) {
          //conService.logStringMessage("*** removing sheet from: <"+uri.spec+">");
          sss.unregisterSheet(uri, sss.USER_SHEET);
        }
        if (sss.sheetRegistered(uri, sss.USER_SHEET)) conService.logStringMessage("SHIIIII...");
      } catch (e) {
        //conService.logStringMessage("FUUUUUUCK!"+e.msg);
      }
    }
  }

  showAboutPage();

  /*
  win.addEventListener("feedModelReady", function (evt) {
    //win.removeEventListener("feedModelReady", arguments.callee, false);
    if (feedtreeView) setupFeedTreeModel(evt.setsignals);
  }, false);
  if (feedtreeView) setupFeedTreeModel(true);
  */

  if (winitems.art_tree) {
    let atree = winitems.art_tree;
    arttreeView.cleanup();
    atree.view = arttreeView;
    atree.addEventListener("select", function (evt) {
      cancelEvent(evt);
      artRenderCurrentArticle();
    });
    atree.addEventListener("click", function (evt) {
      if (evt.button === 0 && evt.target.tagName === "treechildren") {
        cancelEvent(evt);
        artRenderCurrentArticle();
      }
    });
    atree.addEventListener("keydown", function (evt) {
      if (!window || !arttreeView || artFeedId < 1) return;
      if (evt.keyCode == 13 && !evt.ctrlKey && !evt.shiftKey && !evt.altKey) {
        cancelEvent(evt);
        artRenderCurrentArticle();
        return;
      }
      if (evt.keyCode == 13 && !evt.ctrlKey && (evt.shiftKey || evt.altKey)) {
        cancelEvent(evt);
        if (artObj) ifutils.openInTab(window, artObj.url, {background:true});
        return;
      }
      //debuglog("keyCode=", evt.keyCode);
      if (commonArtKeys(evt) || commonKeys(evt)) cancelEvent(evt);
    });
  }

  if (winitems.feed_tree) {
    let ftree = winitems.feed_tree;
    ftree.addEventListener("select", function (evt) {
      //debuglog("ftree: selected");
    });
    ftree.addEventListener("click", function (evt) {
      // open new article only by left click
      if (evt.button === 0 && evt.target.tagName === "treechildren") {
        cancelEvent(evt);
        showArticlesWithVI(feedtreeView.getDataAt(ftree.currentIndex));
      }
    });
    ftree.addEventListener("keydown", function (evt) {
      if (evt.keyCode === 13 && !evt.ctrlKey && !evt.shiftKey && !evt.altKey) {
        cancelEvent(evt);
        showArticlesWithVI(feedtreeView.getDataAt(ftree.currentIndex));
      }
      if (commonKeys(evt)) cancelEvent(evt);
    });
    ftree.view = feedtreeView;
    setFeedTreeTitle();
    ftree.focus();
  }
};


////////////////////////////////////////////////////////////////////////////////
function goToUnreadFeed (dir, skipcurrent) {
  if (!window) return;
  if (typeof(dir) === "undefined") dir = "down";
  if (typeof(skipcurrent) === "undefined") skipcurrent = true;
  let ftree = winitems.feed_tree;
  let cidx = ftree.currentIndex;
  let nidx;
  if (dir === "up") {
    // previous
    for (nidx = cidx-1; nidx >= 0; --nidx) {
      let vi = feedtreeView.getDataAt(nidx);
      if (vi.container) continue;
      if (vi.obj.countUnread) break;
    }
    if (nidx < 0) {
      for (nidx = feedtreeView.rowCount-1; nidx > cidx; --nidx) {
        let vi = feedtreeView.getDataAt(nidx);
        if (vi.container) continue;
        if (vi.obj.countUnread) break;
      }
    }
  } else {
    // next
    for (nidx = cidx+1; nidx < feedtreeView.rowCount; ++nidx) {
      let vi = feedtreeView.getDataAt(nidx);
      if (vi.container) continue;
      if (vi.obj.countUnread) break;
    }
    if (nidx >= feedtreeView.rowCount) {
      for (nidx = 0; nidx < cidx; ++nidx) {
        let vi = feedtreeView.getDataAt(nidx);
        if (vi.container) continue;
        if (vi.obj.countUnread) break;
      }
    }
  }
  if (nidx != cidx && nidx >= 0 && nidx < feedtreeView.rowCount) {
    // found new group
    feedtreeView.selection.select(nidx);
    ftree.treeBoxObject.ensureRowIsVisible(nidx);
    showArticlesWithVI(feedtreeView.getDataAt(ftree.currentIndex));
  }
}


function commonKeys (evt) {
  if (!window) return false;
  if (evt.ctrlKey && !evt.altKey /*&& !evt.shiftKey*/) {
    switch (evt.keyCode) {
      case 71: // '^G' -- next feed with unread messages
        goToUnreadFeed((evt.shiftKey ? "up" : "down"), true);
        return true;
    }
  }
  if (evt.ctrlKey || evt.altKey || evt.shiftKey) return false;
  let eatIt = true;
  switch (evt.keyCode) {
    case 117: // F6 -- feed tree
      winitems.feed_tree.focus();
      return true;
    case 118: // F7 -- article tree
      if (artFeedId > 0) winitems.art_tree.focus();
      return true;
    case 120: // F9 -- article text
      if (artObj) {
        let iframe = winitems.article_content_iframe;
        iframe.focus();
        let doc = iframe.contentDocument;
        if (doc.activeElement) doc.activeElement.blur();
        doc.body.focus();
      }
      return true;
  }
  return false;
}


function commonArtKeys (evt) {
  function pgUp (iw) {
    let cy = iw.scrollY;
    if (cy == 0) return;
    let wh = iw.innerHeight;
    let hdrh = addonOptions.artviewHeaderSize;
    if (hdrh < 0) hdrh = 0;
    iw.scrollBy(0, -(wh-hdrh));
  }

  function pgDown (iw) {
    let cy = iw.scrollY;
    let wh = iw.innerHeight;
    let hdrh = addonOptions.artviewHeaderSize;
    if (hdrh < 0) hdrh = 0;
    iw.scrollBy(0, wh-hdrh);
  }

  if (!window) return false;
  if (evt.keyCode == 32) {
    if (evt.ctrlKey || evt.altKey) return false;
    if (artObj) {
      let iw = winitems.article_content_iframe.contentWindow;
      if (iw) {
        if (evt.shiftKey) pgUp(iw); else pgDown(iw);
      }
    }
    return true;
  }
  if (artFeedId < 0) return false;
  if (evt.altKey /*|| evt.shiftKey*/) return false;
  switch (evt.keyCode) {
    case 46: // delete
      if (evt.ctrlKey || evt.altKey || evt.shiftKey) return false;
      if (artObj) {
        //debuglog("artObj.state=", artObj.state);
        if (artObj.state === flags.ASTATE.READ) {
          FeedsDB.db.articleChangeDelMarkByIdSignalled(artObj.id, true);
        } else if (artObj.state === flags.ASTATE.DELMARKED) {
          FeedsDB.db.articleChangeDelMarkByIdSignalled(artObj.id, false);
        }
      }
      return true;
    case 82: // 'R' -- mark current read
      winitems.art_tree.focus();
      if (artObj) FeedsDB.db.articleChangeReadByIdSignalled(artObj.id, true);
      return true;
    case 77: case 78: // 'M', 'N' -- next unread article
      if (evt.ctrlKey || evt.altKey || evt.metaKey) return false; // don't eat ctrl+n, as i'm using it for "new tab"
      winitems.art_tree.focus();
      // can't find next unread article, and shift pressed? go "ctrl+g"!
      if (!artGoTo("unread")) {
        if (evt.shiftKey) goToUnreadFeed("down");
      }
      return true;
    case 85: // 'U' -- mark current unread
      winitems.art_tree.focus();
      if (artObj) FeedsDB.db.articleChangeReadByIdSignalled(artObj.id, false);
      return true;
  }
  return false;
}


////////////////////////////////////////////////////////////////////////////////
function showAboutPage () {
  let iframe = winitems.article_content_iframe;
  ifutils.loadUrl(iframe, "chrome://korenews-help/content/about.html");
}


////////////////////////////////////////////////////////////////////////////////
// make `idx` current row
function artEnsureGoodRowVisibility () {
  if (!arttreeView) return;
  if (arttreeView.selection.count != 1) return;
  let idx = winitems.art_tree.currentIndex;
  if (idx < 0 || idx >= arttreeView.rowCount) return;
  let tbox = winitems.art_tree.treeBoxObject;
  tbox.ensureRowIsVisible(idx);
  // now make sure that we see at least two more rows
  let lv = tbox.getLastVisibleRow()-idx; // how much visible rows we have at the bottom?
  if (lv >= 2) return;
  if (lv < 0) return; // just in case
  // we have to shift view
  let fv = tbox.getFirstVisibleRow();
  let sn = idx-fv; // how much rows we have at the top?
  if (sn > lv) sn = lv;
  if (sn < 1) return; // just in case
  tbox.scrollByLines(sn);
}


function artMakeCurrent (idx) {
  if (!arttreeView) return;
  if (typeof(idx) !== "number") return;
  if (idx < 0 || idx >= arttreeView.rowCount) return;
  if (arttreeView.rowCount < 1) return;
  arttreeView.selection.select(idx);
  artEnsureGoodRowVisibility();
}


const SelectArticleMode = {
  LastRead: 0,
  FirstUnread: 1,
};


// SelectArticleMode which
function selectArticle (which) {
  if (!arttreeView) return;
  let nidx = -1, lastReadIdx = -1;
  for (let [idx, art] in arttreeView.artIteratorIndexed()) {
    if (art.state === flags.ASTATE.UNREAD) {
      if (lastReadIdx >= 0 && which === SelectArticleMode.LastRead) {
        nidx = lastReadIdx;
        break;
      }
      if (which === SelectArticleMode.FirstUnread) {
        nidx = idx;
        break;
      }
    } else if (art.state === flags.ASTATE.READ) {
      lastReadIdx = idx;
    }
  }
  if (nidx < 0) nidx = (lastReadIdx >= 0 ? lastReadIdx : 0);
  artMakeCurrent(nidx);
  artRenderCurrentArticle();
}


function showArticlesWithVI (vi) {
  if (!vi || !arttreeView) return;
  if (vi.container) {
    //FeedsDB.db.getArticlesForGroupAsync(vi.obj.id, function (art) {});
  } else {
    if (artFeedId == vi.obj.id) return; // nothing to do
    let atree = winitems.art_tree;
    // fix title
    setupArtTreeViewFor(vi.obj.id);
    let tit = (vi.obj.title || "feed #"+vi.obj.id);
    winitems.feed_header_title.textContent = tit;
    document.title = "KoreNews -- "+tit;
    atree.focus();
    FeedsDB.db.getArticlesForFeedAsync(vi.obj.id, function (art) {
      if (!arttreeView) return;
      if (!art) {
        // done
        selectArticle(addonOptions.artlistOpenFirstUnread ? SelectArticleMode.FirstUnread : SelectArticleMode.LastRead);
      } else {
        arttreeView.addArticle(art);
      }
    });
  }
}


////////////////////////////////////////////////////////////////////////////////
// return `false` if no article found, or `true` if selection was moved
function artGoTo (dir) {
  if (!window) return false;
  if (!arttreeView) return false;
  if (artFeedId < 1) return false;
  let atree = winitems.art_tree;
  let cidx = atree.currentIndex;
  let nidx = cidx;
  switch (dir) {
    case "up":
      if (cidx < 0) return false;
      if (--nidx < 0) return false;
      break;
    case "down":
      if (cidx < 0) return false;
      if (++nidx >= arttreeView.rowCount) return false;
      break;
    case "unread":
      if (cidx < 0) cidx = 0;
      for (nidx = cidx; nidx < arttreeView.rowCount; ++nidx) {
        let vi = arttreeView.getDataAt(nidx);
        if (vi.state == flags.ASTATE.UNREAD) break;
      }
      if (nidx >= arttreeView.rowCount) {
        for (nidx = 0; nidx < cidx; ++nidx) {
          let vi = arttreeView.getDataAt(nidx);
          if (vi.state == flags.ASTATE.UNREAD) break;
        }
        if (nidx >= cidx) {
          // no unread, select last
          //if (arttreeView.rowCount < 1) return false;
          //nidx = arttreeView.rowCount-1;
          return false;
        }
      }
      break;
  }
  artMakeCurrent(nidx);
  artRenderCurrentArticle();
  return true;
}


////////////////////////////////////////////////////////////////////////////////
function runArtFilter (jsname, art, iframe) {
  const newFileURI = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService).newFileURI;
  let text, url;
  try {
    let fljs = userdirs.JSArt;
    fljs.append(jsname);
    if (!fljs.exists() || !fljs.isFile() || !fljs.isReadable()) return;
    text = fileReadText(fljs);
    url = newFileURI(fljs).spec;
  } catch (e) {
    return;
  }
  let sandbox = Cu.Sandbox(iframe.contentWindow, {
    sandboxName: "article filtering sandbox",
    sameZoneAs: iframe.contentWindow, // help GC a little
    sandboxPrototype: iframe.contentWindow/*.wrappedJSObject*/,
    wantXrays: false,
  });
  sandbox.window = iframe.contentWindow;
  sandbox.document = iframe.contentDocument;
  sandbox.article = art;
  sandbox.log = function () {
    let s = "";
    for (let f = 0; f < arguments.length; ++f) s += ""+arguments[f];
    conlog("KNFILTER: ", s);
  };
  try {
    Cu.evalInSandbox(text, sandbox, "ECMAv5", url, 1);
    Cu.evalInSandbox("main()", sandbox, "ECMAv5", url, 1);
  } catch (e) {
    logException("KNFILTER", e);
  }
  Cu.nukeSandbox(sandbox);
}


function runArticleFilter (art, iframe) runArtFilter("artfilter.js", art, iframe);
function runArticleDOMFilter (art, iframe) runArtFilter("domfilter.js", art, iframe);


////////////////////////////////////////////////////////////////////////////////
function renderArticleWithVI (vi) {
  const newFileURI = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService).newFileURI;
  if (!vi) return;
  let aid = vi.id;
  if (artQueryId === aid) return; // nothing to do
  cancelArtQuery();
  artQueryId = aid;
  artQuery = FeedsDB.db.queryArticleTextAsync(aid, function (art) {
    //debuglog("got article #", aid);
    artQuery = null;
    if (artQueryId != aid) return;
    if (!winitems) return;
    let iframe = winitems.article_content_iframe;
    runArticleFilter(art, iframe);
    artObj = art;
    // load custom html
    let avhtmlurl = "chrome://korenews/content/html/artview.html";
    {
      let flhtml = userdirs.HtmlDir;
      flhtml.append("artview.html");
      if (flhtml.exists() && flhtml.isFile() && flhtml.isReadable()) {
        avhtmlurl = newFileURI(flhtml).spec;
      }
    }
    //debuglog(" clearing iframe");
    ifutils.load(iframe, avhtmlurl, function () {
      if (!winitems) return;
      if (artQueryId !== aid) return;
      // load custom css
      {
        let flcss = userdirs.HtmlDir;
        flcss.append("artview.css");
        if (flcss.exists() && flcss.isFile() && flcss.isReadable()) {
          let lnk = iframe.contentDocument.createElement("link");
          lnk.setAttribute("rel", "stylesheet");
          lnk.setAttribute("href", newFileURI(flcss).spec);
          iframe.contentDocument.head.appendChild(lnk);
        }
      }
      //debuglog(" setting iframe contents (", art.body.length, ")");
      //iframe.contentDocument.body.innerHTML = art.body;
      function $ (id) iframe.contentDocument.getElementById(id);
      let tit = normSpaces(xmlUnescape(art.title||""));
      with ($("korenews-title")) {
        href = art.url;
        textContent = tit||"<untitled>";
      }
      $("korenews-feed-title").textContent = " ("+winitems.feed_header_title.textContent+")";
      $("korenews-artlink").href = art.url;
      $("korenews-date").textContent = art.pubdate.toString();
      $("korenews-text").innerHTML = art.body;
      // run filtering script in sandbox
      runArticleDOMFilter(art, iframe);
      // mark as read
      FeedsDB.db.articleChangeReadByIdSignalled(aid, true);
      for (let a of iframe.contentDocument.querySelectorAll("a[href]")) {
        a.addEventListener("click", function (evt) {
          cancelEvent(evt);
          ifutils.openInTab(window, evt.currentTarget.href, {background:true});
          if (artFeedId > 0) window.setTimeout(function () winitems.art_tree.focus(), 1);
        }, true);
      }
      iframe.contentWindow.addEventListener("keydown", function (evt) {
        if (!window || !arttreeView) return;
        if (commonArtKeys(evt) || commonKeys(evt)) cancelEvent(evt);
      }, true);
    });
  });
}


function artRenderCurrentArticle () {
  if (!arttreeView) return;
  if (arttreeView.rowCount < 1) return;
  if (arttreeView.selection.count != 1) return;
  let idx = winitems.art_tree.currentIndex;
  if (idx < 0 || idx >= arttreeView.rowCount) return;
  renderArticleWithVI(arttreeView.getDataAt(idx));
}


////////////////////////////////////////////////////////////////////////////////
function onArticleMarkToggled (art) {
  FeedsDB.db.articleToggleMarkByIdSignalled(art.id);
}


////////////////////////////////////////////////////////////////////////////////
function signalModelRecreate (signame) {
  if (window) {
    //TODO: keep open/closed states and so on
    cancelArtQuery();
    artQuery = null;
    artQueryId = -1;
    artObj = null; // current article object
    artFeedId = -1;
    //clean arttree model
    cancelArtQuery();
    arttreeView.cleanup();
    winitems.feed_header_title.textContent = "feed name";
    document.title = "KoreNews";
    // recreate feed tree model
    feedtreeView = new FeedTreeModel();
    if (winitems.feed_tree) winitems.feed_tree.view = feedtreeView;
    populateModel(window);
  }
}

function signalModelChanged (signame, data) {
  signalModelRecreate(signame);
}

function signalUpdateTotalUnread (signame, totalUnread) {
  if (!totalUnread) totalUnread = 0;
  /*TODO: this doesn't work
  if (window) {
    let icon = document.getElementById("window-icon");
    debuglog("icon: ", icon, "; href=", icon.getAttribute("href"));
    icon.setAttribute("href", "chrome://korenews/skin/kore/baph16"+(totalUnread > 0 ? "_red" : "")+".png");
    debuglog("icon: ", icon, "; new href=", icon.getAttribute("href"));
  }
  */
  setFeedTreeTitle(totalUnread);
}

function signalFeedUpdateStart (signame, data) {
  if (feedtreeView) feedtreeView.setUpdateStatus(data.fid, true);
}

function signalFeedUpdateCancelled (signame, data) {
  if (feedtreeView) feedtreeView.setUpdateStatus(data.fid, false);
}

function signalFeedUpdateError (signame, data) {
  if (feedtreeView) feedtreeView.setUpdateStatus(data.fid, "error");
}

function signalFeedUpdateComplete (signame, data) {
  if (feedtreeView) {
    if (data.updateTotals) {
      feedtreeView.setFeedTotalsById(data.fid, data.unreadCount, data.totalCount);
    } else {
      feedtreeView.setUpdateStatus(data.fid, false);
    }
    appendNewArticles(data.fid, data.alistNew);
  }
}

function signalFeedTotalsUpdated (signame, data) {
  if (feedtreeView) feedtreeView.setFeedTotalsById(data.fid, data.unreadCount, data.totalCount);
}

function signalArticleMarkChanged (signame, data) {
  if (arttreeView) {
    if (artObj && artObj.id == data.aid) artObj.mark = data.mark;
    arttreeView.setArticleMark(data.aid, data.mark);
  }
}

function signalArticleStateChanged (signame, data) {
  if (arttreeView) {
    if (artObj && artObj.id == data.aid) artObj.state = data.state;
    arttreeView.setArticleState(data.aid, data.state);
    // move to next alive article if current one is deleted
    let atree = winitems.art_tree;
    let idx = atree.currentIndex;
    if (idx < 0) return;
    let aa = arttreeView.getDataAt(idx);
    if (!aa || aa.id != data.aid) return; // nothing to do
    if (aa.state >= flags.ASTATE.DELMARKED) {
      //debuglog("idx=", idx, "; state=", arttreeView.getDataAt(idx).state);
      let nidx;
      for (nidx = idx+1; nidx < arttreeView.rowCount; ++nidx) {
        if (arttreeView.getDataAt(nidx).state != flags.ASTATE.DELMARKED) break;
      }
      if (nidx >= arttreeView.rowCount) {
        for (nidx = idx-1; nidx >= 0; --nidx) {
          if (arttreeView.getDataAt(nidx).state != flags.ASTATE.DELMARKED) break;
        }
        if (nidx < 0) return; // no live articles here
      }
      artMakeCurrent(nidx);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
function signalFeedIconUpdated (signame, data) {
  FeedsDB.db.updateFeedIconById(data.fid, data.icon);
  feedtreeView.setFeedIconById(data.fid, data.icon);
}


function signalFeedTitleUpdated (signame, data) {
  feedtreeView.setFeedTitleById(data.fid, data.title);
}


////////////////////////////////////////////////////////////////////////////////
function appendNewArticles (fid, alist) {
  if (!arttreeView) return;
  if (alist.length == 0 || artFeedId != fid) return;
  FeedsDB.db.getArticlesForFeedAsync(fid, function (art) {
    if (!arttreeView) return;
    if (!art) return; // done
    arttreeView.addArticle(art); // it's ok to add duplicates, they will be ignored
  });
}


////////////////////////////////////////////////////////////////////////////////
// feed popup menu
exports.onDeactivatedFeedMenu = function (popup) {
  popup.vi = null; // don't anchor it
};


function appendPlural (val) {
  if (val != 1) return "s";
  return "";
}


function upIntervalText (upinterval) {
  upinterval = Math.floor(upinterval/60); // minutes
  if (upinterval < 60) return ""+upinterval+" min"+appendPlural(upinterval);
  upinterval = Math.floor(upinterval/60); // hours
  if (upinterval < 24) return ""+upinterval+" hour"+appendPlural(upinterval);
  upinterval = Math.floor(upinterval/24); // days
  return ""+upinterval+" day"+appendPlural(upinterval);
}


exports.onActivatingFeedMenu = function (evt) {
  if (!feedtreeView) return false;

  let popup = evt.target;
  let trigNode = evt.target.triggerNode;
  let vi;
  let aname; // attribute with text

  if (trigNode.tagName === "treecol") {
    // header
    vi = true; // special
    aname = "whole";
  } else if (trigNode.tagName === "treechildren") {
    // children
    vi = feedtreeView.getDataAt(winitems.feed_tree.currentIndex);
    if (!vi) return false;
    aname = (vi.container ? "plural" : "singular");
  } else {
    // wtf?
    return false;
  }

  function setVisible (el, vis) {
    if (el) {
      if (vis) el.removeAttribute("hidden"); else el.setAttribute("hidden", "true");
    }
  }

  function isVisible (el) (el ? !el.getAttribute("hidden") : false);

  for (let el = popup.firstChild; el; el = el.nextSibling) {
    if (el.tagName === "menuseparator") {
      setVisible(el, true);
      continue;
    }
    if (el.tagName !== "menuitem") continue;
    let text = el.getAttribute(aname);
    if (!text) {
      setVisible(el, false);
      text = el.getAttribute("singular");
      if (!text) text = el.getAttribute("plural");
      if (!text) text = el.getAttribute("whole");
      el.setAttribute("label", text);
    } else {
      setVisible(el, true);
      if (el.getAttribute("updateinterval") === "tan") {
        //conlog("vi="+vi+"; vi.obj="+vi.obj+"; upinterval="+vi.obj.upinterval);
        el.setAttribute("originalText", text);
        //el.setAttribute("feedId", ""+vi.obj.id);
        text += ": "+upIntervalText(vi.obj.upinterval);
      }
      el.setAttribute("label", text);
    }
    let flagName = el.getAttribute("flagName");
    if (flagName && vi !== true && !vi.container) {
      let flg = ((vi.obj.flags&flags.FFLAG[flagName]) != 0);
      if (el.getAttribute("flagNegated") === "tan") flg = !flg;
      el.setAttribute("checked", (flg ? "true" : ""));
    }
  }

  if (vi.container) {
    let el = document.getElementById("pmfRemoveGroup");
    if (vi.obj.id === 0 || vi.obj.feeds.length > 0) {
      el.setAttribute("disabled", true);
    } else {
      el.removeAttribute("disabled");
    }
  }

  // now go thru menu again and hide all separators that
  // are followed by another separator or nothing
  // ok, this is lame, but it works
  {
    let el = popup.firstChild;
    // process separators
    while (el) {
      if (el.tagName === "menuseparator") {
        // find previous visible item
        let pel = el.previousSibling;
        while (pel && !isVisible(pel)) pel = pel.previousSibling;
        if (!pel || pel.tagName === "menuseparator") setVisible(el, false);
      }
      el = el.nextSibling;
    }
    // remove trailing separators
    el = popup.lastChild;
    while (el) {
      if (isVisible(el)) {
        if (el.tagName !== "menuseparator") break;
        setVisible(el, false);
      }
      el = el.previousSibling;
    }
  }

  popup.vi = vi; // for commands
  return true;
};


function pmfDoAction (vi, action, ignoredisabled) {
  if (!vi || !feedtreeView) return;
  if (typeof(ignoredisabled) === "undefined") ignoredisabled = true;
  if (vi === true) {
    for (let fobj in feedtreeView.feedIterator()) {
      if (ignoredisabled && (fobj.flags&flags.FFLAG.NO_UPDATES)) continue;
      action(fobj.id);
    }
  } else if (!vi.container) {
    if (ignoredisabled && (vi.obj.flags&flags.FFLAG.NO_UPDATES)) return;
    action(vi.obj.id);
  } else {
    for (let fobj in feedtreeView.feedInGroupIterator(vi.obj.id)) {
      if (ignoredisabled && (fobj.flags&flags.FFLAG.NO_UPDATES)) continue;
      action(fobj.id);
    }
  }
}


// delta in minutes
// changes `vi.obj.upinterval` too
function doActionIntervalChange (vi, direction, faster) {
  if (!vi || vi === true || vi.container || direction == 0) return; // oops

  let upinterval = vi.obj.upinterval;
  let delta = 1, prevDelta = 1, nextDelta = 1; // minutes
  upinterval = Math.floor(upinterval/60); // minutes
  if (upinterval < 60) {
    prevDelta = 1;
    delta = 1;
    nextDelta = 60;
  } else {
    upinterval = Math.floor(upinterval/60); // hours
    if (upinterval < 24) {
      prevDelta = 1;
      delta = 60;
      nextDelta = 24*60;
      divisor = 60;
    } else {
      upinterval = Math.floor(upinterval/24); // days
      if (direction < 0 && upinterval < 7) faster = false;
      prevDelta = 60;
      delta = 24*60;
      nextDelta = 7*24*60;
    }
  }
  if (direction < 0) { delta = -delta; prevDelta = -prevDelta; nextDelta = -nextDelta; }

  let upit = Math.floor(vi.obj.upinterval/60);
  let newup;

  if (faster) {
    if (direction > 0) {
           if (upit < 30) { newup = 30; faster = false; delta = 0; }
      else if (upit < 60) { newup = 60; faster = false; delta = 0; }
    } else {
           if (upit < 60 && upit > 30) { newup = 30; faster = false; delta = 0; }
      else if (upit < 30 && upit > 0) { newup = 1; faster = false; delta = 0; }
    }
  }

  if (faster) { prevDelta = delta; delta = nextDelta; }

  if (delta) {
    newup = upit+delta;
    //conlog("upit="+upit+"; prevDelta="+prevDelta+"; delta="+delta+"; nextDelta="+nextDelta+"; newup="+newup+"; mft="+Math.floor(newup/delta));
    if (newup <= 0 || newup < Math.abs(delta)) {
      newup = Math.abs(delta)-Math.abs(prevDelta);
      //conlog(" newup clamped="+newup);
    } else {
      newup = Math.floor(newup/Math.abs(delta))*Math.abs(delta);
      //conlog(" newup corrected="+newup);
    }
    if (newup < 0) newup = 0;
    if (newup > 5*7*24*60) newup = 5*7*24*60;
  }

  if (newup == upit) return;

  newup *= 60; // to seconds
  FeedsDB.db.updateFeedUpInterval(vi.obj.id, newup);
  vi.obj.upinterval = newup;
}


function doActionIntervalChangeWithPopupItem (item, direction, faster) {
  //if (faster) conlog("FASTER!!!");
  let vi = item.parentNode.vi;
  if (!vi || vi === true || vi.container) return; // oops
  doActionIntervalChange(vi, direction, faster);
  //item.setAttribute("label", item.getAttribute("originalText")+": "+upIntervalText(vi.obj.upinterval));
  let popup = item.parentNode;
  for (let el = popup.firstChild; el; el = el.nextSibling) {
    if (el.tagName === "menuseparator") continue;
    if (el.tagName !== "menuitem") continue;
    if (el.getAttribute("updateinterval") !== "tan") continue;
    let text = el.getAttribute("originalText");
    text += ": "+upIntervalText(vi.obj.upinterval);
    el.setAttribute("label", text);
  }
}


exports.onpmfIntervalInc = function (evt) doActionIntervalChangeWithPopupItem(evt.target, 1, evt.ctrlKey);
exports.onpmfIntervalDec = function (evt) doActionIntervalChangeWithPopupItem(evt.target, -1, evt.ctrlKey);


exports.onpmfRefreshFeed = function (evt) pmfDoAction(evt.target.parentNode.vi, function (fid) feedUpdater.queueRefresh(fid, false), true/*ignoredisabled*/);
exports.onpmfUpdateIcon = function (evt) pmfDoAction(evt.target.parentNode.vi, queueFavIconUpdate, false);
exports.onpmfMarkAsRead = function (evt) pmfDoAction(evt.target.parentNode.vi, function (fid) FeedsDB.db.markFeedAsRead(fid), false);


exports.onpmfClearFeed = function (evt) {
  let vi = evt.target.parentNode.vi;
  if (!vi.container) {
    // clear article list if necessary
    let fid = vi.obj.id;
    if (artFeedId === fid) {
      setupArtTreeViewFor();
      winitems.feed_header_title.textContent = "feed name";
      document.title = "KoreNews";
    }
    // clear database
    FeedsDB.db.feedDropAllArticlesById(fid);
  }
};


exports.onpmfRemoveFeed = function (evt) {
  let vi = evt.target.parentNode.vi;
  if (!vi.container) {
    let fid = vi.obj.id;
    // clear article list if necessary
    if (artFeedId === fid) {
      setupArtTreeViewFor();
      winitems.feed_header_title.textContent = "feed name";
      document.title = "KoreNews";
    }
    // remove from model
    feedtreeView.removeFeedById(fid);
    // clear database
    FeedsDB.db.feedDropById(fid);
  }
};


exports.onpmfDoUpdates = function (evt) {
  let it = evt.target;
  let vi = it.parentNode.vi;
  if (vi.container) return;
  //debuglog("updates: ", it.getAttribute("checked"));
  let ck = (it.getAttribute("checked") == "true");
  //debuglog("ck: ", ck);
  ck = !ck;
  it.setAttribute("checked", ck);
  //TODO: send signal
  vi.obj.flags ^= flags.FFLAG.NO_UPDATES;
  FeedsDB.db.feedChangeFlagById(vi.obj.id, flags.FFLAG.NO_UPDATES, "toggle");
  feedtreeView.invalidateFeedById(vi.obj.id);
};


exports.onpmfSendCookies = function (evt) {
  let it = evt.target;
  let vi = it.parentNode.vi;
  if (vi.container) return;
  //debuglog("updates: ", it.getAttribute("checked"));
  let ck = (it.getAttribute("checked") == "true");
  //debuglog("ck: ", ck);
  ck = !ck;
  it.setAttribute("checked", ck);
  //TODO: send signal
  vi.obj.flags ^= flags.FFLAG.SEND_COOKIES;
  FeedsDB.db.feedChangeFlagById(vi.obj.id, flags.FFLAG.SEND_COOKIES, "toggle");
};


exports.onpmfMoveGroup = function (evt, dir) {
  let it = evt.target;
  let vi = it.parentNode.vi;
  if (!vi.container || vi.obj.id === 0) return; // don't move "uncategorized"
  let ng = (dir === "up" ? FeedsDB.db.groupMoveUp(vi.obj.id) : FeedsDB.db.groupMoveDown(vi.obj.id));
  if (ng) {
    if (dir === "up") feedtreeView.moveGroupUp(vi.obj.id); else feedtreeView.moveGroupDown(vi.obj.id);
  }
};


exports.onpmfCreateGroup = function (evt) {
  let ps = Cc["@mozilla.org/embedcomp/prompt-service;1"].getService(Ci.nsIPromptService);
  let val = {value:""};
  let gn = ps.prompt(null, "Create new group", "Group name", val, null, {});
  //debuglog("gn=", gn, "; val=", JSON.stringify(val.value));
  if (!gn || !val.value) return;
  let grp = FeedsDB.db.getOrAddGroupByTitle(val.value);
  if (grp.newGroup) {
    ftState = feedtreeView.saveState();
    signals.emit("model-recreate");
  }
};


exports.onpmfOpenAllGroups = function (evt) (feedtreeView ? feedtreeView.openAllGroups() : undefined);
exports.onpmfCloseAllGroups = function (evt) (feedtreeView ? feedtreeView.closeAllGroups() : undefined);
exports.onpmfCloseAllReadGroups = function (evt) (feedtreeView ? feedtreeView.closeGroupsWithoutUnreads() : undefined);
exports.onpmfOpenAllUnreadGroups = function (evt) (feedtreeView ? feedtreeView.openGroupsWithUnreads() : undefined);


exports.onpmfRemoveGroup = function (evt) {
  let vi = evt.target.parentNode.vi;
  if (!vi.container || vi.obj.feeds.length > 0) return;
  let gid = vi.obj.id;
  if (gid === 0) return;
  feedtreeView.removeGroupById(gid);
  FeedsDB.db.groupDropById(gid);
};


exports.onpmfEditArtFilter = function (evt) {
  let fljs = userdirs.JSArt;
  fljs.append("artfilter.js");
  openEditor(fljs, window, "function main () {\n}\n");
};


exports.onpmfEditArtDOMFilter = function (evt) {
  let fljs = userdirs.JSArt;
  fljs.append("domfilter.js");
  openEditor(fljs, window, "function main () {\n}\n");
};


exports.onpmfEditFeedFilter = function (evt) {
  let vi = evt.target.parentNode.vi;
  if (vi.container) return;
  let fljs = userdirs.JSFeed;
  fljs.append("feed_"+vi.obj.id+".js");
  openEditor(fljs, window,
    "// "+vi.obj.title+"\n"+
    "// item object:\n"+
    "//   string title\n"+
    "//   string link\n"+
    "//   string author (optional)\n"+
    "//   string gid\n"+
    "//   unixtime pubdate\n"+
    "//   string hash\n"+
    "//   string body\n"+
    "// return 'false' to skip article\n"+
    "\n"+
    "const titles = [\n"+
    "];\n"+
    "\n"+
    "const idiots = [\n"+
    "];\n"+
    "\n"+
    "function main () {\n"+
    "  // author\n"+
    "  for (var pat of idiots) {\n"+
    "    if (typeof(pat) === \"string\") {\n"+
    "      if (item.author === pat) return false;\n"+
    "    } else if (pat instanceof RegExp) {\n"+
    "      if (pat.test(item.author)) return false;\n"+
    "    }\n"+
    "  }\n"+
    "  // title\n"+
    "  for (var re of titles) {\n"+
    "    if (typeof(re) === \"string\") {\n"+
    "      if (item.title === re) return false;\n"+
    "    } else if (re instanceof RegExp) {\n"+
    "      if (re.test(item.title)) return false;\n"+
    "    }\n"+
    "  }\n"+
    "}\n"
  );
};


exports.onpmfUrlToClipboard = function (evt) {
  let vi = evt.target.parentNode.vi;
  if (vi.container) return;

  let feed = FeedsDB.db.getFeedById(vi.obj.id);
  if (!feed) return;

  putToClipboard(""+feed.title, ""+feed.url);
};


exports.onpmfEditFeedTitle = function (evt) {
  let vi = evt.target.parentNode.vi;
  if (vi.container) return;

  let feed = FeedsDB.db.getFeedById(vi.obj.id);
  if (!feed) return;

  let ps = Cc["@mozilla.org/embedcomp/prompt-service;1"].getService(Ci.nsIPromptService);
  let val = {value:""+feed.title};
  let gn = ps.prompt(null, "Rename feed", "Feed name", val, null, {});
  if (!gn || !val.value) return;
  if (val.value == feed.title) return;

  let db = FeedsDB.db;
  feed.title = val.value;
  db.updateFeedTitleById(vi.obj.id, feed.title);
  signals.emit("feed-title-updated", {fid:feed.id, title:feed.title});
};


////////////////////////////////////////////////////////////////////////////////
// article popup menu
exports.onDeactivatedArtMenu = function (popup) {
  popup.vi = null; // don't anchor it
};


exports.onActivatingArtMenu = function (evt) {
  if (!arttreeView) return false;

  let popup = evt.target;
  let trigNode = evt.target.triggerNode;
  if (trigNode.tagName !== "treechildren") return false;

  let vi;
  let aname; // attribute with text

  vi = arttreeView.getDataAt(winitems.art_tree.currentIndex);
  if (!vi) return false;
  aname = (vi.container ? "plural" : "singular");

  for (let el = popup.firstChild; el; el = el.nextSibling) {
    let text = el.getAttribute(aname);
    if (!text) {
      el.setAttribute("disabled", "true");
    } else {
      el.removeAttribute("disabled");
      el.setAttribute("label", text);
    }
  }

  popup.vi = vi; // for commands
  return true;
};


function pmaDoReadUnread (vi, setread) {
  if (!arttreeView) return;
  FeedsDB.db.articleChangeReadByIdSignalled(vi.id, setread);
}


exports.onpmaMarkAsRead = function (evt) pmaDoReadUnread(evt.target.parentNode.vi, true);
exports.onpmaMarkAsUnread = function (evt) pmaDoReadUnread(evt.target.parentNode.vi, false);


exports.onpmaCopyAuthor = function (evt) {
  let art = evt.target.parentNode.vi;
  if (!art) return;
  putToClipboard(art.author);
};


exports.onpmaCopyTitle = function (evt) {
  let art = evt.target.parentNode.vi;
  if (!art) return;
  putToClipboard(art.title);
};


////////////////////////////////////////////////////////////////////////////////
exports.feedtreeDragStart = function (evt) {
  let ftree = evt.target.parentNode;
  if (ftree.getAttribute("id") !== "feed-tree") return; // just in case
  let row = {}, col = {}, type = {};
  ftree.treeBoxObject.getCellAt(evt.clientX, evt.clientY, row, col, type);
  row = row.value;
  let vi = feedtreeView.getDataAt(row);
  //debuglog("row=", row, "; container=", vi.container, "; id=", vi.obj.id);
  if (vi.container && vi.obj.id === 0) {
    // can't drag "uncategorized"
    //cancelEvent(evt);
    evt.dataTransfer.effectAllowed = "none";
  } else {
    dragObj = vi;
    evt.dataTransfer.setData("korenews/ftobject", "yay!");
    evt.dataTransfer.effectAllowed = "copyMove";
  }
};


exports.feedtreeDragEnd = function (evt) {
  dragObj = null;
};


exports.feedtreeDragOver = function (evt) {
  let ftree = evt.target.parentNode;
  if (ftree.getAttribute("id") !== "feed-tree") return true; // just in case
  if (evt.dataTransfer.getData("korenews/ftobject") && dragObj) {
    // allow drop
    evt.preventDefault();
    return false;
  }
  return true;
};


exports.feedtreeDrop = function (evt) {
  let ftree = evt.target.parentNode;
  if (ftree.getAttribute("id") !== "feed-tree") return; // just in case
  if (!evt.dataTransfer.getData("korenews/ftobject") || !dragObj) return;
  // get destination vi
  let row = {}, col = {}, type = {};
  ftree.treeBoxObject.getCellAt(evt.clientX, evt.clientY, row, col, type);
  row = row.value;
  let vi = feedtreeView.getDataAt(row);
  // allow drop
  evt.preventDefault();
  //debuglog("act: ", evt.dataTransfer.dropEffect);
  let act = feedtreeView.draggedOnto(dragObj, vi, evt.dataTransfer.dropEffect);
  if (act) {
    //debuglog("action: ", JSON.stringify(act));
    // move group
    if (act.groupId > 0) {
      while (act.groupUpDown < 0) {
        ++act.groupUpDown;
        FeedsDB.db.groupMoveUp(act.groupId);
      }
      while (act.groupUpDown > 0) {
        --act.groupUpDown;
        FeedsDB.db.groupMoveDown(act.groupId);
      }
    }
    // update group feeds
    for (let gup of act.gup) {
      FeedsDB.db.updateGroupFeedsById(gup.gid, gup.feeds);
    }
    // now simply rebuild model, 'cause i'm lazy old fart
    ftState = feedtreeView.saveState();
    signals.emit("model-recreate");
  }
};
