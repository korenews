PRAGMA auto_vacuum = NONE;
PRAGMA encoding = "UTF-8";
PRAGMA journal_mode = WAL;
PRAGMA synchronous = NORMAL;

/* group list */
CREATE TABLE IF NOT EXISTS groups (
    id INTEGER PRIMARY KEY AUTOINCREMENT  /* unique group id */
  , title TEXT NOT NULL                   /* group title */
  , feeds TEXT NOT NULL DEFAULT ''        /* comma-separated feed ids */
  , treeindex INTEGER NOT NULL            /* index in tree */
);
CREATE INDEX IF NOT EXISTS groups_treeindex ON groups(treeindex);


/* feed list */
CREATE TABLE IF NOT EXISTS feeds (
    id INTEGER PRIMARY KEY AUTOINCREMENT      /* unique feed id */
  , url TEXT NOT NULL                         /* feed refresh url */
  , homeurl TEXT NOT NULL                     /* feed home url */
  , icon TEXT NOT NULL DEFAULT ''             /* feed icon in "data:" format */
  , title TEXT NOT NULL                       /* feed title */
  , descr TEXT NOT NULL DEFAULT ''            /* feed description */
  , flags INTEGER NOT NULL                    /* bitmask, FFLAG_XXX */
  , upinterval INTEGER NOT NULL DEFAULT 1200  /* update interval, in seconds, 60 mins */
  , uplast INTEGER NOT NULL                   /* last update, unixtime, in seconds */
  , upfailed INTEGER NOT NULL                 /* !0: last update was failed */
  , total INTEGER NOT NULL                    /* total number of articles (not including deleted) */
  , unread INTEGER NOT NULL                   /* number of unread articles */
);
CREATE INDEX IF NOT EXISTS feeds_url ON feeds(url);
CREATE INDEX IF NOT EXISTS feeds_uplast ON feeds(uplast);


/* article list */
CREATE TABLE IF NOT EXISTS articles (
    id INTEGER PRIMARY KEY AUTOINCREMENT  /* unique article id */
  , fid INTEGER NOT NULL                  /* feed id to which this article belongs to */
  , guid TEXT NOT NULL                    /* unique article id within feed */
  , title TEXT NOT NULL                   /* article title */
  , url TEXT NOT NULL                     /* article url */
  , author TEXT NOT NULL                  /* article author */
  , hash TEXT NOT NULL                    /* article hash (to sort out dupes) */
  , body TEXT NOT NULL                    /* article body */
  , pubdate INTEGER NOT NULL              /* article creation date, unixtime, in seconds */
  , hitdate INTEGER NOT NULL              /* last time this article was seen in rss -- so we can delete those which aren't in feed anymore */
  , state INTEGER NOT NULL                /* see ASTATE */
  , mark INTEGER NOT NULL                 /* !0: marked */
);
CREATE INDEX IF NOT EXISTS articles_fid ON articles(fid);
CREATE INDEX IF NOT EXISTS articles_id_fid ON articles(id, fid);
CREATE INDEX IF NOT EXISTS articles_id_fid_guid ON articles(id, fid, guid);
CREATE INDEX IF NOT EXISTS articles_fid_guid ON articles(fid, guid);
CREATE INDEX IF NOT EXISTS articles_id_fid_guid_date ON articles(id, fid, guid, pubdate);
CREATE INDEX IF NOT EXISTS articles_date ON articles(pubdate);
CREATE INDEX IF NOT EXISTS articles_fid_date ON articles(fid, pubdate);
CREATE INDEX IF NOT EXISTS articles_fid_date_state ON articles(fid, pubdate, state);
CREATE INDEX IF NOT EXISTS articles_fid_state ON articles(fid, state);
