/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
// all our code will live in this nice closure
(function (global) {
  const {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = global.Components;

  let scp = {};
  Cu.import("chrome://korenews-jscode/content/addon.js", scp);

  let addonobj = scp.initAddon(global,
    "KoreNews",                          // addon name, used in `alert()`
    "chrome://korenews/content/",        // content URL, must end with "/"
    "chrome://korenews-jscode/content/", // addon js code URL, must end with "/"
    function (addonobj) {
      // add "isBootstrapped" flag
      addonobj.defProp("isBootstrapped", {get: function () false});
    }
  );

  global.getAddonObj = function () {
    if (addonobj) return addonobj;
    throw new Error("addon object wasn't initialized");
  };

  global.newWindow = scp.newWindow;
})(this);

var EXPORTED_SYMBOLS = ["getAddonObj", "newWindow"];
