/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
var PREFS_BRANCH = "extensions.korenews.";

var PREFS = {
  active: true,
  debugMode: false,
  logEnabled: true,
  upinterval_minutes: 30,
  artlistOpenFirstUnread: true, // false: open last read
  rssHandlerRegistered: false,
  artviewHeaderSize: 0, // size of fixed top header, for scrolling
  updateTimerLogsEnabled: false,
  editor: "/usr/bin/leafpad",
  // all pathes are relative to profile dir
  dbdir: "korenews/db",
  icondir: "korenews/icons",
  tempdir: "korenews/temp",
  htmldir: "korenews/html",
  jsrssfilterdir: "korenews/js_filters/rss",
  jsartfilterdir: "korenews/js_filters/art",
};

var PREFS_DIR_NAMES = {
  dbdir: "DBDir",
  icondir: "IconDir",
  tempdir: "TempDir",
  htmldir: "HtmlDir",
  jsrssfilterdir: "JSFeed",
  jsartfilterdir: "JSArt",
};
