/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
require("utils/utils");


////////////////////////////////////////////////////////////////////////////////
function setupDocShell (iframe) {
  let ds = iframe.docShell;
  ds.allowAuth = false;
  ds.allowDNSPrefetch = false;
  ds.allowImages = true;
  ds.allowJavascript = false;
  ds.allowMedia = false;
  ds.allowMetaRedirects = false;
  ds.allowPlugins = false;
  ds.allowSubframes = false;
  ds.allowWindowControl = false;
  ds.useErrorPages = false;
  if (typeof(iframe.k8ehset) === "undefined") {
    iframe.k8ehset = true;
    iframe.addEventListener("DOMContentLoaded", function (evt) {
      if (iframe.fillContentCB) {
        let cb = iframe.fillContentCB;
        iframe.fillContentCB = null;
        cb.apply(null);
      }
    }, false);
    iframe.addEventListener("load", function (evt) {
      if (iframe.fillContentCB) {
        let cb = iframe.fillContentCB;
        iframe.fillContentCB = null;
        cb.apply(null);
      }
    }, false);
  }
}
exports.setupDocShell = setupDocShell;


////////////////////////////////////////////////////////////////////////////////
// stop any previous page loading
function stopLoading (iframe) {
  let ds = iframe.docShell;
  let webnav = ds.QueryInterface(Ci.nsIWebNavigation);
  webnav.stop(webnav.STOP_ALL);
}
exports.stopLoading = stopLoading;


////////////////////////////////////////////////////////////////////////////////
function DSloadUrl (iframe, url) {
  let ds = iframe.docShell;
  let webnav = ds.QueryInterface(Ci.nsIWebNavigation);
  webnav.stop(webnav.STOP_ALL);
  setupDocShell(iframe);
  webnav.loadURI(
    url,
    webnav.LOAD_FLAGS_BYPASS_HISTORY|webnav.LOAD_FLAGS_BYPASS_CACHE|webnav.LOAD_FLAGS_STOP_CONTENT,
    null, // referer
    null, // post data
    null  // headers
  );
}


////////////////////////////////////////////////////////////////////////////////
function loadUrl (iframe, url) {
  DSloadUrl(iframe, url);
}
exports.loadUrl = loadUrl;


////////////////////////////////////////////////////////////////////////////////
exports.load = function (iframe, htmlurl, fillContentCB) {
  iframe.fillContentCB = null;
  setupDocShell(iframe);
  stopLoading(iframe);
  iframe.fillContentCB = (typeof(fillContentCB) === "function" ? fillContentCB : null);
  DSloadUrl(iframe, htmlurl);
};


////////////////////////////////////////////////////////////////////////////////
exports.openInTab = function (win, url, inbg) {
  let afterCurrent = null;
  if (typeof(inbg) == "object") {
    afterCurrent = ("afterCurrent" in inbg ? !!inbg.afterCurrent : null);
    inbg = ("background" in inbg ? !!inbg.background : null);
  } else if (typeof(inbg) != "boolean") {
    inbg = null;
  }
  let dwn = win.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindow);
  let chromeWindow =
    dwn.QueryInterface(Ci.nsIInterfaceRequestor).
    getInterface(Ci.nsIWebNavigation).
    QueryInterface(Ci.nsIDocShellTreeItem).
    rootTreeItem.
    QueryInterface(Ci.nsIInterfaceRequestor).
    getInterface(Ci.nsIDOMWindow);
  if (!chromeWindow) return;
  let bro = chromeWindow.gBrowser;
  if (!bro) return;
  let docbro = bro.getBrowserForDocument(dwn.top.document);
  if (!docbro) return;
  //let tabIdx = bro.getBrowserIndexForDocument(dwn.top.document);
  //let myTab = bro.tabs[tabIdx];
  let myTab = bro._getTabForContentWindow(dwn.top);
  let tabIdx = myTab._tPos;
  let myIsCurrent = (myTab == bro.mCurrentTab);
  let nt = bro.addTab(url, {ownerTab: myTab, relatedToCurrent: myIsCurrent});
  let getBool = Services.prefs.getBoolPref;
  let bg = (inbg !== null ? inbg : getBool("browser.tabs.loadInBackground"));
  let rel = (afterCurrent !== null ? afterCurrent : getBool("browser.tabs.insertRelatedAfterCurrent"));
  if (!bg && myIsCurrent) bro.selectedTab = nt;
  if (myTab && rel) bro.moveTabTo(nt, tabIdx+1);
};
