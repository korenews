/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
if (typeof(exportsGlobal) === "undefined") exportsGlobal = {};


////////////////////////////////////////////////////////////////////////////////
exportsGlobal.xmlEscape = function (s) s.replace(/\&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/\x22/g, "&quot;");
exportsGlobal.xmlUnescape = function (s) s.replace(/\&lt;/g, "<").replace(/\&gt;/g, ">").replace(/\&quot;/g, '"').replace(/\&amp;/g, "&");
exportsGlobal.normSpaces = function (s) s.replace(/^\s+/, "").replace(/\s+$/, "").replace(/\s\s+/g, " ");


////////////////////////////////////////////////////////////////////////////////
exportsGlobal.unlinkFile = function (path) {
  try {
    let fl = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsILocalFile);
    fl.initWithPath(path);
    if (fl.exists()) fl.remove(false);
  } catch (e) {}
};


////////////////////////////////////////////////////////////////////////////////
// you may pass nsIFile, nsIURI or string
// string will be tries as URI first
function getFileOrUri (file) {
  if (typeof(file) === "undefined") throw new Error("path or file object expected");
  if ((file instanceof Ci.nsIFile) || (file instanceof Ci.nsIURI)) return file;
  if (typeof(file) !== "string") throw new Error("path or file object expected");
  // try to parse as URI
  {
    let mt = file.match(/^(main(?:js)?):(.+)$/);
    if (mt) {
      switch (mt[1]) {
        case "main": file = contentUrl+mt[2]; break;
        case "mainjs": file = jscodeUrl+mt[2]; break;
        default: throw new Error("invalid special scheme: '"+mt[1]+"'");
      }
    }
  }
  let ioSrv = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService)
  try { return ioSrv.newURI(file, null, null); } catch (e) {}
  // nope, try to open local file
  try {
    let fl = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsILocalFile);
    fl.initWithPath(file);
    return fl;
  } catch (e) {
    // xpcemu hook
    if (typeof(xpcshellBuildFile) === "function") try { return xpcshellBuildFile(file); } catch (ee) {}
    throw e;
  }
}
exportsGlobal.getFileOrUri = getFileOrUri;


////////////////////////////////////////////////////////////////////////////////
// you may pass nsIFile, nsIURI or string
function loadTextContents (path, encoding) {
  if (typeof(encoding) !== "string") encoding = "UTF-8"; else encoding = encoding||"UTF-8";

  function toUnicode (text) {
    if (typeof(text) === "string") {
      if (text.length == 0) return "";
    } else if (text instanceof ArrayBuffer) {
      if (text.byteLength == 0) return "";
      text = new Uint8Array(text);
      return new TextDecoder(encoding).decode(text);
    } else if ((text instanceof Uint8Array) || (text instanceof Int8Array)) {
      if (text.length == 0) return "";
      return new TextDecoder(encoding).decode(text);
    } else {
      return "";
    }
    let converter = Cc["@mozilla.org/intl/scriptableunicodeconverter"].createInstance(Ci.nsIScriptableUnicodeConverter);
    converter.charset = encoding;
    let res = converter.ConvertToUnicode(text);
    if (res.length >= 3 && res.substr(0, 3) == "\u00EF\u00BB\u00BF") res = res.substr(3); // fuck BOM
    return res;
  }

  let file = getFileOrUri(path);
  //debuglog("path=[", path, "]");
  if (file instanceof Ci.nsIFile) {
    //debuglog("file=[", file.path, "]");
    // read file
    let inputStream = Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);
    inputStream.init(file, -1, -1, null);
    let scInputStream = Cc["@mozilla.org/scriptableinputstream;1"].createInstance(Ci.nsIScriptableInputStream);
    scInputStream.init(inputStream);
    let output = scInputStream.read(-1);
    scInputStream.close();
    inputStream.close();
    return toUnicode(output, encoding);
  } else {
    //debuglog("uri=[", file.spec, "]");
    // download from URL
    let req = Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance();
    req.mozBackgroundRequest = true;
    req.open("GET", file.spec, false);
    req.timeout = 30*1000;
    req.setRequestHeader("User-Agent", "Mozilla/5.0 NewsFox/0.666");
    const cirq = Ci.nsIRequest;
    req.channel.loadFlags |= cirq.INHIBIT_CACHING|cirq.INHIBIT_PERSISTENT_CACHING|cirq.LOAD_BYPASS_CACHE|cirq.LOAD_ANONYMOUS;
    req.responseType = "arraybuffer";
    //if (sendCookies) addCookies(req, urlsend);
    let error = false;
    req.onerror = function () { error = true; }; // just in case
    req.ontimeout = function () { error = true; }; // just in case
    req.send(null);
    if (!error) {
      error = (req.status != 0 && Math.floor(req.status/100) != 2);
      //(/^file:/.test(file.spec) ? (req.status != 0) : (Math.floor(req.status/100) != 2));
    }
    if (error) throw new Error("can't load URI contents: status="+req.status+"; error="+error);
    return toUnicode(req.response, encoding);
  }
}
exportsGlobal.loadTextContents = loadTextContents;


////////////////////////////////////////////////////////////////////////////////
// you may pass nsIFile, nsIURI or string
// cback: function (contents); if `contents` is `null`, some error was occured
// addCookies (xmlhttp, url), can be null
function loadTextContentsAsync (path, cback, encoding, addCookiesCB) {
  if (typeof(encoding) !== "string") encoding = "utf-8"; else encoding = encoding||"utf-8";

  function removeQuotes (s) {
    s = s.trim();
    if (s.length < 2) return s;
    if (s[0] == '"' && s[s.length-1] == '"') return s.substr(1, s.length-2).trim();
    if (s[0] == "'" && s[s.length-1] == "'") return s.substr(1, s.length-2).trim();
    return s;
  }

  function toUnicode (text, encoding) {
    if (typeof(encoding) !== "string") encoding = "";
    encoding = removeQuotes(encoding);
    //logError("TRYING charset: '", encoding, "'");
    if (typeof(text) === "string") {
      if (text.length == 0) return "";
    } else if (text instanceof ArrayBuffer) {
      if (text.byteLength == 0) return "";
      text = new Uint8Array(text);
      return new TextDecoder(encoding).decode(text);
    } else if ((text instanceof Uint8Array) || (text instanceof Int8Array)) {
      if (text.length == 0) return "";
      return new TextDecoder(encoding).decode(text);
    } else {
      return "";
    }
    let converter = Cc["@mozilla.org/intl/scriptableunicodeconverter"].createInstance(Ci.nsIScriptableUnicodeConverter);
    converter.charset = encoding;
    let res = converter.ConvertToUnicode(text);
    if (res.length >= 3 && res.substr(0, 3) == "\u00EF\u00BB\u00BF") res = res.substr(3); // fuck BOM
    return res;
  }

  encoding = removeQuotes(encoding);
  try {
    let file = getFileOrUri(path);
    if (file instanceof Ci.nsIFile) {
      // read file
      let inputStream = Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);
      inputStream.init(file, -1, -1, null);
      let scInputStream = Cc["@mozilla.org/scriptableinputstream;1"].createInstance(Ci.nsIScriptableInputStream);
      scInputStream.init(inputStream);
      let output = scInputStream.read(-1);
      scInputStream.close();
      inputStream.close();
      let text = toUnicode(output, encoding);
      try { cback(text); } catch (e) {}
      return {abort: function () {}};
    } else {
      // download from URL
      let req = Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance();
      req.mozBackgroundRequest = true;
      req.open("GET", file.spec, true);
      req.timeout = 30*1000;
      req.setRequestHeader("User-Agent", "Mozilla/5.0 Goanna/666 PaleMoon/666 KoreNews/666");
      const cirq = Ci.nsIRequest;
      req.channel.loadFlags |= cirq.INHIBIT_CACHING|cirq.INHIBIT_PERSISTENT_CACHING|cirq.LOAD_BYPASS_CACHE;
      req.responseType = "arraybuffer";
      if (addCookiesCB) {
        addCookiesCB(req, file.spec);
      } else {
        req.channel.loadFlags |= cirq.LOAD_ANONYMOUS;
      }
      req.onabort = function () { let cb = cback; cback = null; if (cb) cb(null); };
      req.onerror = function () { let cb = cback; cback = null; if (cb) cb(null); };
      req.ontimeout = function () { let cb = cback; cback = null; if (cb) cb(null); };
      //req.onloadstart = function () { debuglog("onloadstart"); };
      /*
      req.onloadend = function () {
        debuglog("onloadend");
        // there can be some errors, but no onerror called, so fix that
        let cb = cback;
        cback = null;
        if (cb) cb(null);
      };
      */
      let enc = encoding;
      req.onload = function () {
        //logError("onload: status=", req.status);
        //logError("charset: ", req.getResponseHeader("Content-Type"));
        let cb = cback;
        cback = null;
        if (cb) {
          let error = (req.status != 0 && Math.floor(req.status/100) != 2);
          let text;
          if (!error) {
            let ct = req.getResponseHeader("Content-Type")||"";
            let mt = ct.match(/;.*\bcharset=([^\s,;]+)/);
            let encoding = enc;
            if (mt) encoding = removeQuotes(mt[1]).toLowerCase();
            //logError("charset: '", encoding, "'");
            //logError("response:", req.response);
            try {
              text = toUnicode(req.response, encoding||enc);
              if (encoding === "utf-8") {
                // try to autodetect
                let mt = text.match(/encoding=["']([^"'<>\/]+)["']/i);
                if (mt) {
                  encoding = removeQuotes(mt[1]);
                  //logError("charset1: '", encoding, "'");
                  text = toUnicode(req.response, encoding);
                }
              }
            } catch (e) {
              text = null;
              logException("loadTextContentsAsync: decode", e);
            }
          } else {
            text = null;
            //logError("path=", path, "; status=", req.status);
          }
          cb(text);
        }
      };
      req.send(null);
      return {abort: function () {
        try {
          if (req.readyState == 2 || req.readyState == 3) req.abort();
        } catch (e) {}
      }};
    }
  } catch (e) {
    logException("loadTextContentsAsync", e);
  }
  cback(null);
  return null;
}
exportsGlobal.loadTextContentsAsync = loadTextContentsAsync;


////////////////////////////////////////////////////////////////////////////////
// cback: function (contents); if `contents` is `null`, some error was occured
// addCookies (xmlhttp, url), can be null
function loadXMLContentsAsync (url, cback, addCookiesCB) {
  try {
    // download from URL
    let req = Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance();
    req.mozBackgroundRequest = true;
    req.open("GET", url, true);
    req.timeout = 30*1000;
    req.setRequestHeader("User-Agent", "Mozilla/5.0 Goanna/666 PaleMoon/666 KoreNews/666");
    const cirq = Ci.nsIRequest;
    req.channel.loadFlags |= cirq.INHIBIT_CACHING|cirq.INHIBIT_PERSISTENT_CACHING|cirq.LOAD_BYPASS_CACHE;
    //req.responseType = "arraybuffer";
    if (addCookiesCB) {
      addCookiesCB(req, url);
    } else {
      req.channel.loadFlags |= cirq.LOAD_ANONYMOUS;
    }
    req.onabort = function () { let cb = cback; cback = null; if (cb) cb(null); };
    req.onerror = function () { let cb = cback; cback = null; if (cb) cb(null); };
    req.ontimeout = function () { let cb = cback; cback = null; if (cb) cb(null); };
    req.onload = function () {
      let cb = cback;
      cback = null;
      if (cb) {
        let error = (req.responseXML === null || (req.status != 0 && Math.floor(req.status/100) != 2));
        if (!error) {
          cb(req.responseXML);
        } else {
          cb(null);
        }
      }
    };
    req.send(null);
    return {abort: function () {
      try {
        if (req.readyState == 2 || req.readyState == 3) req.abort();
      } catch (e) {}
    }};
  } catch (e) {
    logException("loadXMLContentsAsync", e);
  }
  cback(null);
  return null;
}
exportsGlobal.loadXMLContentsAsync = loadXMLContentsAsync;


////////////////////////////////////////////////////////////////////////////////
exportsGlobal.cancelEvent = function (evt) {
  if (evt) {
    if (typeof(evt.preventDefault) === "function") evt.preventDefault();
    if (typeof(evt.stopPropagation) === "function") evt.stopPropagation();
    if (typeof(evt.stopImmediatePropagation) === "function") evt.stopImmediatePropagation();
  }
};


////////////////////////////////////////////////////////////////////////////////
// download from URL
// cback (file, success)
// headerCheckCB (req)
exportsGlobal.downloadFileAsyncXXX = function (file, url, cback, headerCheckCB) {
  if (!(file instanceof Ci.nsIFile)) throw new Error("nsIFile expected");
  if (typeof(url) !== "string") throw new Error("string URL expected");
  if (typeof(cback) !== "function") throw new Error("function callback expected");
  if (typeof(headerCheckCB) === "undefined") headerCheckCB = null;
  else if (typeof(headerCheckCB) !== "function") throw new Error("function header callback expected");
  if (file.exists() && !file.isFile()) throw new Error("nsIFile of normal file expected");
  // zero file
  try { file.remove(false); } catch (e) {}
  file.create(file.NORMAL_FILE_TYPE, 384/*0600*/);
  // create request
  try {
    let req = Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance();
    req.mozBackgroundRequest = true;
    req.open("GET", url, true);
    req.timeout = 30*1000;
    req.setRequestHeader("User-Agent", "Mozilla/5.0 Goanna/666 PaleMoon/666 KoreNews/666");
    const cirq = Ci.nsIRequest;
    req.channel.loadFlags |= cirq.INHIBIT_CACHING|cirq.INHIBIT_PERSISTENT_CACHING|cirq.LOAD_BYPASS_CACHE|cirq.LOAD_ANONYMOUS;
    req.responseType = "moz-chunked-arraybuffer";
    let onError = function () {
      if (cback) {
        let cb = cback;
        cback = null;
        try { file.remove(false); } catch (e) {}
        cb(file, false);
      }
    };
    req.onabort = onError;
    req.onerror = onError;
    req.ontimeout = onError;
    req.onload = function () {
      if (cback) {
        let cb = cback;
        cback = null;
        let error = (req.status != 0 && Math.floor(req.status/100) != 2);
        if (error) {
          try { file.remove(false); } catch (e) {}
          cb(file, false);
        } else {
          cb(file, true);
        }
      }
    };
    req.onprogress = function (ev) {
      //conlog("ev: ", typeof(ev), " : ", ev.type, " : lct=", ev.lengthComputable, "; loaded=", ev.loaded, "; total=", ev.total);
      if (ev.lengthComputable) {
        conlog("progress: ", ev.loaded, " of ", ev.total);
      } else {
        conlog("progress: ", ev.loaded);
      }
      conlog(" got ", req.response.byteLength, " bytes");
      //dumphex(new Uint8Array(req.response));
    };
    req.send(null);
    return {abort: function () {
      try {
        if (req.readyState == 2 || req.readyState == 3) req.abort();
      } catch (e) {}
    }};
  } catch (e) {
    try { file.remove(false); } catch (e) {}
    throw e;
  }
};


////////////////////////////////////////////////////////////////////////////////
// download from URL
// cback (file, success)
// headerCheckCB (chan)
exportsGlobal.downloadFileAsync = function (file, url, cback, headerCheckCB) {
  if (!(file instanceof Ci.nsIFile)) throw new Error("nsIFile expected");
  if (typeof(url) !== "string") throw new Error("string URL expected");
  if (typeof(cback) !== "function") throw new Error("function callback expected");
  if (typeof(headerCheckCB) === "undefined") headerCheckCB = null;
  else if (typeof(headerCheckCB) !== "function") throw new Error("function header callback expected");
  if (file.exists() && !file.isFile()) throw new Error("nsIFile of normal file expected");
  // zero file
  try { file.remove(false); } catch (e) {}
  file.create(file.NORMAL_FILE_TYPE, 384/*0600*/);
  // create request
  let observer = {
    onDownloadComplete: function (dldr, request, ctxt, status, fl) {
      if (status != 0) {
        try { file.remove(false); } catch (e_) {}
        try { cback(file, false); } catch (e_) {}
      } else {
        try {
          let chan = request.QueryInterface(Ci.nsIHttpChannel);
          let okheader = true;
          if (headerCheckCB) {
            try {
              okheader = headerCheckCB(chan);
            } catch (e_) {
              okheader = false;
            }
          }
          if (!okheader) {
            try { file.remove(false); } catch (e_) {}
            cback(file, false);
          } else {
            cback(file, true);
          }
        } catch (e) {
          try { file.remove(false); } catch (e_) {}
          logException("downloadFileAsync callback", e);
        }
      }
    },
  };
  try {
    var IOService = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
    var IOchannel = IOService.newChannel(url, null, null);
    var listener = Cc["@mozilla.org/network/downloader;1"].createInstance(Ci.nsIDownloader);
    listener.init(observer, file);
    IOchannel.asyncOpen(listener, null);
  } catch (e) {
    if (file.exists()) try { file.remove(false); } catch (e) {}
    logException("downloadFileAsync", e);
  }
};


////////////////////////////////////////////////////////////////////////////////
exportsGlobal.fileReadBinary = function (fl) {
  let istream = Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);
  istream.init(fl, -1, -1, false);
  let bstream = Cc["@mozilla.org/binaryinputstream;1"].createInstance(Ci.nsIBinaryInputStream);
  bstream.setInputStream(istream);
  let bytes = bstream.readBytes(bstream.available());
  bstream.close();
  return bytes;
};


////////////////////////////////////////////////////////////////////////////////
exportsGlobal.fileReadText = function (file, charset) {
  var inputStream = Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);
  inputStream.init(file, -1, -1, null);
  var scInputStream = Cc["@mozilla.org/scriptableinputstream;1"].createInstance(Ci.nsIScriptableInputStream);
  scInputStream.init(inputStream);
  var output;
  try {
    output = scInputStream.read(-1);
  } catch (e) {
    output = "";
  }
  scInputStream.close();
  inputStream.close();
  var converter = Cc["@mozilla.org/intl/scriptableunicodeconverter"].createInstance(Ci.nsIScriptableUnicodeConverter);
  converter.charset = charset||"UTF-8";
  try {
    return converter.ConvertToUnicode(output);
  } catch (e) {}
  return "";
};


////////////////////////////////////////////////////////////////////////////////
/*
function isValidUtf8 (s, maxlen) {
  if (typeof(maxlen) === "undefined") maxlen = 32;
  let idx = 0;
  let len = s.length;
  while (idx < len && s.charCodeAt(idx) < 128) ++idx;
  if (idx >= len) return true;
  while (maxlen > 0) {
    let ch = s.charCodeAt(idx++);
    if (ch >= 80) {
      if ((ch&0xc0) != 0xc0) return false;
      let ul = 1;
      let mask = 0x20;
      while (mask && (ch&mask) != 0) {
        ++ul;
        mask >>= 1;
      }
      if (ul > 5) return false;
      while (ul--) {
        ch = s.charCodeAt(idx++);
        if ((ch&0xc0) != 0x80) return false;
      }
      --maxlen;
    }
  }
  return true;
}
*/
