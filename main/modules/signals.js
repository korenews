/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
let SignalNamePrefix = "korenews-signal-";
let obs = Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService);


////////////////////////////////////////////////////////////////////////////////
// this should not be used directly, so define it as property
Object.defineProperty(exports, "namePrefix", {get: function () SignalNamePrefix});


////////////////////////////////////////////////////////////////////////////////
// key: function
// value: dict of (key:string name, value:object observer)
let observers = {};

exports.addListener = function (name, cback) {
  if (typeof(name) !== "string" || !name) throw new Error("invalid signal name");
  if (typeof(cback) !== "function") throw new Error("callback function expected");
  // check if already here
  if (cback in observers) {
    let names = observers[cback];
    if (name in names) return; // nothing to do
  } else {
    observers[cback] = {};
  }
  const observer = {
    observe: function (subject, topic, data) {
      topic = topic.substr(SignalNamePrefix.length); // remove prefix
      if (data && data.length) {
        try {
          data = JSON.parse(data);
        } catch (e) {
          logException("DATA FOR SIGNAL '"+name+"' PARSE", e);
          return;
        }
      } else {
        data = null;
      }
      cback(topic, data);
    },
  };
  obs.addObserver(observer, SignalNamePrefix+name, false);
  observers[cback][name] = observer;
};


exports.removeListener = function (name, cback) {
  if (typeof(name) !== "string" || !name) throw new Error("invalid signal name");
  if (typeof(cback) !== "function") throw new Error("callback function expected");
  // find observer
  if (!(cback in observers)) return; // nothing to do
  let names = observers[cback];
  if (!(name in names)) return; // nothing to do
  try {
    obs.removeObserver(observers[cback][name], name);
  } catch (e) {}
};


exports.emit = function (name, data) {
  if (typeof(name) !== "string" || !name) throw new Error("invalid signal name");
  data = (typeof(data) === "undefined" ? null : (data !== null ? JSON.stringify(data) : null));
  obs.notifyObservers(null, SignalNamePrefix+name, data);
};
