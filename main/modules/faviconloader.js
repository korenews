/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
let signals = require("signals");
let FeedsDB = require("feedsdb");


////////////////////////////////////////////////////////////////////////////////
let favIconQueue = [];


function advanceQueue (fid) {
  // move to next queued update
  let idx = favIconQueue.indexOf(fid);
  if (idx >= 0) favIconQueue.splice(idx, 1);
  if (favIconQueue.length) updateFavIcon(favIconQueue[0]);
}


////////////////////////////////////////////////////////////////////////////////
function getIFile (file) {
  if (typeof(file) === "undefined") throw new Error("path or file object expected");
  if (typeof(file) === "string") {
    try {
      let fl = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsILocalFile);
      fl.initWithPath(file);
      return fl;
    } catch (e) {
      if (typeof(xpcshellBuildFile) === "function") try { return xpcshellBuildFile(file); } catch (ee) {}
    }
  } else if (file instanceof Ci.nsIFile) {
    return file;
  }
  throw new Error("path or file object expected");
}


////////////////////////////////////////////////////////////////////////////////
function getFileContentType (file) {
  let ioSrv = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService)

  function getUri (url) {
    try { return ioSrv.newURI(file, null, null); } catch (e) {}
    return null;
  }

  function getFile (path) {
    try {
      let fl = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsILocalFile);
      fl.initWithPath(path);
      return fl;
    } catch (e) {
      if (typeof(xpcshellBuildFile) === "function") try { return xpcshellBuildFile(path); } catch (ee) {}
    }
    return null;
  }

  try {
    let uri = null;
    if (typeof(file) === "string") {
      uri = getUri(file);
      if (!uri) {
        file = getFile(file);
        if (!file) throw new Error("can't open file or URL");
      }
    }
    if (!uri) {
      if (file instanceof Ci.nsIURI) {
        uri = file;
      } else if (file instanceof Ci.nsIFile) {
        if (!file.exists() || !file.isFile() || !file.isReadable()) return null; // alas
        if (file.fileSize == 0) return null; // alas
        uri = ioSrv.newFileURI(file, null, null);
      } else {
        throw new Error("invalid file object");
      }
    }
    let chan = ioSrv.newChannelFromURI(uri);
    let strm = chan.open();
    if (strm) {
      let ct = chan.contentType;
      strm.close();
      return (ct ? ct : null);
    }
  } catch (e) {}
  return null;
}


////////////////////////////////////////////////////////////////////////////////
function isImageFile (file) {
  let ct = getFileContentType(file);
  if (!ct) return false;
  if (!(/^image\/./.test(ct))) return false;
  file = getIFile(file);
  if (!file.exists() || !file.isFile() || !file.isReadable()) return false;
  if (file.fileSize < 8) return false;
  try {
    let istream = Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);
    istream.init(file, -1, -1, false);
    let bstream = Cc["@mozilla.org/binaryinputstream;1"].createInstance(Ci.nsIBinaryInputStream);
    bstream.setInputStream(istream);
    let size = bstream.available();
    if (size > 4096) size = 4096;
    let bytes = bstream.readBytes(size);
    bstream.close();
    if ((/<(?:x|ht)ml/i).test(bytes)) return false;
    if ((/<\?xml/i).test(bytes)) return false;
    if ((/<\!DOCTYPE/i).test(bytes)) return false;
    return true;
  } catch (e) {} // will throw when stream is closed
  return false;
}


////////////////////////////////////////////////////////////////////////////////
let faviconsInProgress = {};
let faviconCount = 0;
let faviconPrefix = null;


////////////////////////////////////////////////////////////////////////////////
function genFavIconFile (ext) {
  if (faviconPrefix === null) faviconPrefix = ""+(new Date().getTime())+"_"+Math.random()+"_";
  ext = ext||".ico";
  let file = userdirs.TempDir;
  //file.append((""+Math.random())+".ico");
  file.append(faviconPrefix+faviconCount+ext);
  if (++faviconCount == 0x7ffffff) {
    faviconCount = 0;
    faviconPrefix += "z";
  }
  file.createUnique(file.NORMAL_FILE_TYPE, 0600);
  return file;
}


////////////////////////////////////////////////////////////////////////////////
// here we cheat! to get png out of any image, we'll create windowless browser,
// and canvas elements on its dom window, will  load file into image, draw image
// onto canvas, and then get canvas data
function loadIcon (fid, path, cback) {
  const CANVAS_DATAURL_PREFIX = "data:image/png;base64,";
  const MAX_ICON_SIZE = 16384;

  let appShell = Cc["@mozilla.org/appshell/appShellService;1"].getService(Ci.nsIAppShellService);

  let webnav = appShell.createWindowlessBrowser(true);
  let docshell = webnav.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDocShell);
  let systemPrincipal = Cc["@mozilla.org/systemprincipal;1"].createInstance(Ci.nsIPrincipal);
  docshell.createAboutBlankContentViewer(systemPrincipal);
  let doc = docshell.contentViewer.DOMDocument;
  let contentWindow = doc.defaultView;

  let canvas = doc.createElementNS("http://www.w3.org/1999/xhtml", "canvas");
  canvas.setAttribute("width", "16");
  canvas.setAttribute("height", "16");
  doc.body.appendChild(canvas);

  let image = doc.createElementNS("http://www.w3.org/1999/xhtml", "img");
  doc.body.appendChild(image);

  let feedUid = fid;
  let filePath = path;

  function removeElements () {
    image.parentNode.removeChild(image);
    canvas.parentNode.removeChild(canvas);

    doc = null;
    contentWindow = null;

    // when you are done with it, destroy it
    if (webnav.close) { webnav.close() }; // only available in Firefox 46+, and is needed for good measure
    webnav = null; // in Firefox <= 45 setting to null will cause it to get GC'ed which will destroy it
  }

  function onError () {
    cback("chrome://korenews/skin/images/feed.png");
  }

  debuglog("loading file icon for ", feedUid, " from [", filePath, "]");
  image.onload = function () {
    unlinkFile(filePath);
    try {
      let ctx = canvas.getContext("2d");
      ctx.fillStyle = ctx.strokeStyle = "rgba(0,0,0,0)"; // transparent
      ctx.clearRect(0, 0, 16, 16);
      ctx.drawImage(image, 0, 0, 16, 16);
      let du = canvas.toDataURL();
      let size = atob(du.substr(CANVAS_DATAURL_PREFIX.length)).length;
      if (size > MAX_ICON_SIZE) {
        logError("icon too large for ", feedUid, " from [", filePath, "]");
        onError();
      } else {
        //canvas.src = canvas.toDataURL();
        //canvas.setAttribute("tooltiptext", filePath);
        debuglog("got file icon for ", feedUid, " from [", filePath, "]");
        cback(du);
      }
    } catch (e) {
      logException("loadIcon:onload", e);
    }
    removeElements();
  };

  image.onerror = function () {
    conlog("image.onerror!");
    unlinkFile(filePath);
    removeElements();
    onError();
  };

  try {
    let fl = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsILocalFile);
    fl.initWithPath(filePath);
    let uri = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService).newFileURI(fl);
    image.src = uri.spec;
  } catch (e) {
    logException("loadIcon", e);
    unlinkFile(filePath);
    onError();
  }
}


////////////////////////////////////////////////////////////////////////////////
function setFavIcon (fid, icon) {
  signals.emit("feed-icon-updated", {fid:fid, icon:icon});
  delete faviconsInProgress[fid];
  advanceQueue(fid);
}


function setFavIconFromFile (fid, path) {
  loadIcon(fid, path, function (idata) setFavIcon(fid, idata));
}


////////////////////////////////////////////////////////////////////////////////
/*
function getFileAsData (file, contentType) {
  let bytes = fileReadBinary(file);
  let str = "";
  for (let i = 0; i < bytes.length; ++i) str += String.fromCharCode(str.charCodeAt(i)&0xff);
  return "data:"+contentType+";base64,"+btoa(str);
}
*/


////////////////////////////////////////////////////////////////////////////////
function tryInvalidIcon (fid) {
  debuglog("can't get favicon for ", fid);
  setFavIcon(fid, "chrome://korenews/skin/images/feed.png");
}


////////////////////////////////////////////////////////////////////////////////
function updateFavIconInternal (fid, url, guessedUrl) {
  if (typeof(guessedUrl) === "undefined") guessedUrl = null;

  debuglog("trying real favicon for ", fid, " [", url, "]");
  let homeUrl = url;
  let feedUid = fid;
  let gurl = guessedUrl;

  try {
    let file = genFavIconFile(".ico");

    let contentType;

    downloadFileAsync(file, homeUrl, function (file, success) {
      try {
        if (!success) {
          try { file.remove(false); } catch (e_) {}
          if (gurl) {
            tryHomePage(feedUid, gurl, false);
          } else {
            tryInvalidIcon(feedUid);
          }
        } else if (!isImageFile(file)) {
          try { file.remove(false); } catch (e_) {}
          if (gurl) {
            tryHomePage(feedUid, gurl, false);
          } else {
            tryInvalidIcon(feedUid);
          }
        } else {
          // seems that file is good
          //let data = getFileAsData(file, contentType);
          //try { file.remove(false); } catch (e_) {}
          //setFavIcon(feedUid, data);
          setFavIconFromFile(feedUid, file.path);
        }
      } catch (e_) {
        delete faviconsInProgress[feedUid];
        tryInvalidIcon(feedUid);
      }
    }, function (chan) {
      let ct = (chan.getResponseHeader("Content-Type")||"");
      debuglog("updateFavIconInternal got this content type: '", ct, "'");
      if (!((/^image\/./i).test(ct))) {
        if (isImageFile(file)) {
          let nct = getFileContentType(file);
          debuglog("updateFavIconInternal got this content type: '", ct, "' for image file '", nct, "'");
          contentType = nct;
          return true;
        }
        return false;
      }
      contentType = ct;
      return true;
    });
  } catch (e) {
    logException("updateFavIconInternal", e);
    try { file.remove(false); } catch (e_) {}
    tryInvalidIcon(feedUid);
  }
};


function extractFavIconUrl (html, url, ctype) {
  try {
    let uri = Services.io.newURI(url, null, null);
    ctype = ctype||"text/html";
    let parser = Cc["@mozilla.org/xmlextras/domparser;1"].createInstance(Ci.nsIDOMParser);
    parser.init(null, uri, uri);
    let doc = parser.parseFromString(html, ctype);
    let fl = doc.head.querySelector("link[rel='icon']");
    if (!fl) fl = doc.head.querySelector("link[rel='shortcut icon']");
    if (fl && fl.href) return fl.href;
  } catch (e) {}
  return null;
}


function tryHomePage (fid, homeurl, tryguess) {
  if (typeof(tryguess) === "undefined") tryguess = false;

  let homeUrl = homeurl;
  let feedUid = fid;
  let gurl = null; // guessed url

  if (tryguess) {
    let mt = homeUrl.match(/^(https?:\/\/[^\/]+\/)./i);
    if (mt) {
      gurl = mt[1];
    }
  }

  debuglog("trying homepage extraction for ", fid, " [", homeUrl, "] gurl=[", gurl, "]");

  try {
    let file = genFavIconFile(".html");

    let contentType;
    let charset = "UTF-8";

    downloadFileAsync(file, homeUrl, function (file, success) {
      debuglog("tryHomePage done; content type: '", contentType, "'; charset: '", charset, "'; success:", success);
      try {
        if (!success) {
          debuglog(" tryHomePage: failure");
          try { file.remove(false); } catch (e_) {}
          tryInvalidIcon(feedUid);
        } else {
          // seems that file is good
          let text = fileReadText(file, charset);
          try { file.remove(false); } catch (e_) {}
          let url = (text ? extractFavIconUrl(text, homeUrl, contentType) : null);
          if (url) {
            debuglog(" tryHomePage: found [", url, "]");
            updateFavIconInternal(feedUid, url, gurl);
          } else {
            try { file.remove(false); } catch (e_) {}
            if (gurl) {
              debuglog(" tryHomePage: not found, trying guessed [", gurl, "]");
              //tryHomePage(feedUid, gurl, false);
              updateFavIconInternal(feedUid, gurl+"favicon.ico", gurl);
            } else {
              debuglog(" tryHomePage: not found");
              tryInvalidIcon(feedUid);
            }
          }
        }
      } catch (e_) {
        try { file.remove(false); } catch (e_) {}
        delete faviconsInProgress[feedUid];
      }
    }, function (chan) {
      let ct = (chan.getResponseHeader("Content-Type")||"");
      debuglog("tryHomePage got this content type: '", ct, "'");
      let mt = ct.match(/^text\/(html|xml)\b/);
      if (mt) {
        contentType = "text/"+mt[1];
        charset = chan.contentCharset||"UTF-8";
        return true;
      }
      return false;
    });
  } catch (e) {
    logException("tryHomePage", e);
    try { file.remove(false); } catch (e_) {}
    delete faviconsInProgress.fid;
    tryInvalidIcon(feedUid);
  }
}


////////////////////////////////////////////////////////////////////////////////
function updateFavIcon (fid) {
  if (fid in faviconsInProgress) return;
  let feed = FeedsDB.db.getFeedById(fid);
  if (!feed || !feed.homeurl) return;
  let obj = {fid:fid};
  faviconsInProgress[fid] = obj;

  let homeUrl = feed.homeurl;
  let feedUid = fid;
  let favUrl = homeUrl;
  if (favUrl[favUrl.length-1] != "/") favUrl += "/";
  favUrl += "favicon.ico";

  debuglog("updatig favicon for ", feedUid, " [", favUrl, "]");
  try {
    let file = genFavIconFile(".ico");

    let contentType;

    downloadFileAsync(file, favUrl, function (file, success) {
      debuglog("updateFavIcon [", favUrl, "] done; content type: '", contentType, "', success:", success);
      try {
        if (!success) {
          debuglog("  updateFavIcon: failure");
          try { file.remove(false); } catch (e_) {}
          if (homeUrl) tryHomePage(feedUid, homeUrl, true); else delete faviconsInProgress[feedUid];
        } else if (!isImageFile(file)) {
          debuglog("  updateFavIcon: not image");
          try { file.remove(false); } catch (e_) {}
          if (homeUrl) tryHomePage(feedUid, homeUrl, true); else delete faviconsInProgress[feedUid];
        } else {
          // seems that file is good
          //let data = getFileAsData(file, contentType);
          //try { file.remove(false); } catch (e_) {}
          //setFavIcon(feedUid, data);
          //debuglog(" setting icon");
          //let uri = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService).newFileURI(file);
          //setFavIcon(feedUid, uri.spec);
          setFavIconFromFile(feedUid, file.path);
        }
      } catch (e_) {
        delete faviconsInProgress[feedUid];
      }
    }, function (chan) {
      let ct = (chan.getResponseHeader("Content-Type")||"");
      debuglog("updateFavIcon got this content type: '", ct, "'");
      if (!((/^image\/./i).test(ct))) {
        if (isImageFile(file)) {
          let nct = getFileContentType(file);
          debuglog("updateFavIcon got this content type: '", ct, "' for image file '", nct, "'");
          contentType = nct;
          return true;
        }
        return false;
      }
      contentType = ct;
      return true;
    });
  } catch (e) {
    logException("updateFavIcon", e);
    try { file.remove(false); } catch (e_) {}
    delete faviconsInProgress.fid;
    tryInvalidIcon(feedUid);
  }
}


////////////////////////////////////////////////////////////////////////////////
exports.queueFavIconUpdate = function (fid) {
  if (typeof(fid) !== "number") return;
  let idx = favIconQueue.indexOf(fid);
  if (idx >= 0) return; // already queued
  favIconQueue.push(fid);
  if (favIconQueue.length == 1) updateFavIcon(fid);
};
