/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
exports.setRSSHandler = function (url) {
  if (addonOptions.rssHandlerRegistered) return;
  debuglog("setting RSS content handler...");
  // not fuckin' working
  //navigator.registerContentHandler("application/vnd.mozilla.maybe.feed", url, "KoreNews");
  const freeSlot = "chrome://browser-region/locale/region.properties";
  const prefs = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefService).getBranch("browser.contentHandlers.types.");
  let firstFreeHandler = -1;
  // 42 is arbitrary number
  for (let f = 0; f < 42; ++f) {
    try {
      let uri = prefs.getCharPref(""+f+".uri");
      if (uri === url) {
        // already set
        debuglog("found previous RSS handler");
        addonOptions.rssHandlerRegistered = true;
        return;
      }
    } catch (e) {
      firstFreeHandler = f;
      break;
    }
  }
  if (firstFreeHandler < 0) {
    logError("WTF, BOYS?!");
    return;
  }
  firstFreeHandler = ""+firstFreeHandler;
  prefs.setCharPref(firstFreeHandler+".title", "KoreNews");
  prefs.setCharPref(firstFreeHandler+".type", "application/vnd.mozilla.maybe.feed");
  prefs.setCharPref(firstFreeHandler+".uri", url);
  {
    let ps = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefService).getBranch("browser.feeds.handlers.");
    ps.setCharPref("webservice", url);
  }
  {
    let ps = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefService).getBranch("browser.contentHandlers.auto.");
    ps.setCharPref("application/vnd.mozilla.maybe.feed", url);
  }
  addonOptions.rssHandlerRegistered = true;
  debuglog("content handler #", firstFreeHandler, " set");
}
