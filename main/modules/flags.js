/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
const FFLAG = {
  NO_UPDATES:    0x0001, // never update this feed
  CUSTOM_UPDATE: 0x0002, // use "upinterval" from freed instead of global (disabled for now)
  SEND_COOKIES:  0x0004, // send cookies on request
  ADDCHECK_MASK: 0x0007, // mask for checking in addfeed
  NO_UPDATEALL:  0x0008, // don't update this feed when "update all" clicked
};
exports.FFLAG = FFLAG;


//WARNING! >= `DELMARKED` should be true for deleted articles
const ASTATE = {
  UNREAD:    0, // this article is not new, yet still unread
  READ:      1, // this article was read
  DELMARKED: 2, // this article is marked for deletion
  DELETED:   3, // this article is deleted
};
exports.ASTATE = ASTATE;
