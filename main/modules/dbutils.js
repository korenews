/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
// split string to SQL statements, so `executeSimpleSQL()` can be used with that
exports.sqlSplit = function (text) {
  let res = [];
  while (text.length > 0) {
    let pos = 0;
    while (pos < text.length && text.charCodeAt(pos) <= 32) ++pos;
    if (pos >= text.length) break;
    if (pos > 0) text = text.substr(pos);
    pos = 0;
    let inStr = false;
    let inComment = false;
    while (pos < text.length) {
      if (inComment) {
        // in comment
        ++pos;
        if (pos < text.length && text[pos-1] == "*" && text[pos] == "/") {
          ++pos;
          inComment = false;
        }
      } else if (inStr) {
        if (text[pos] == inStr) inStr = false;
        ++pos;
      } else {
        // normal
        if (text[pos] == '"' || text[pos] == "'") {
          inStr = text[pos++];
        } else if (pos+1 < text.length && text[pos] == "/" && text[pos+1] == "*") {
          inComment = true;
          pos += 2;
        } else {
          ++pos;
          if (text[pos-1] == ";") break;
        }
      }
    }
    res.push(text.substr(0, pos));
    text = text.substr(pos);
  }
  return res;
};
