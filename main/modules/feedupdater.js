/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
let flags = require("flags");
let signals = require("signals");
let FeedsDB = require("feedsdb");
let {queueFavIconUpdate} = require("faviconloader");
let timer = require("utils/timer");

let {appendParsedFeeds} = require("opml");


////////////////////////////////////////////////////////////////////////////////
/*
function getUrlHost (url) {
  var mt = url.match(/^[^:\/.]+:\/\/([^\/]+)/);
  return (mt ? mt[1] : null);
}
*/
function getUrlHost (url) {
  try {
    let uri = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService).newURI(url, null, null);
    //let u = uri.QueryInterface(Ci.nsIURL);
    //if (!u) return null;
    return uri.host;
  } catch (e) {
    return null;
  }
}


function guessHome (url) {
  try {
    let uri = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService).newURI(url, null, null);
    //let u = uri.QueryInterface(Ci.nsIURL);
    //if (!u) return null;
    let defPort = (uri.scheme === "http" ? 80 : 443);
    let home = uri.scheme+"://"+uri.host;
    if (uri.port > 0 && uri.port != defPort) home += ":"+uri.port;
    home += "/";
    return home;
  } catch (e) {
    return null;
  }
}


function addCookies (xmlhttp, url) {
  let host = getUrlHost(url);
  if (!host) return; // wtf?!
  //debuglog("*** checking cookies for '", host, "'");
  let cookieManager = Cc["@mozilla.org/cookiemanager;1"].getService(Ci.nsICookieManager2);
  let count = cookieManager.getCookiesFromHost(host);
  if (count == 0) return;
  let nsic = Ci.nsICookie;
  while (count.hasMoreElements()) {
    let cookie = count.getNext();
    if (cookie instanceof nsic) {
      debuglog("  ", cookie.name, " : ", cookie.value);
      //xmlhttp.setRequestHeader("Cookie", cookie.name+"="+cookie.value);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
function parseTZDate (isoDate) {
  try {
    let dateTime = isoDate.split("T");
    let ymd = dateTime[0].split("-");
    for (let i = ymd.length; i < 3; ++i) ymd[i] = 1;
    let utc;
    if (dateTime.length > 1) {
      let timeSplitter = dateTime[1].match(/[Z+-]/);
      let timeOffset = dateTime[1].split(timeSplitter);
      let hms = timeOffset[0].split(":");
      for (let i = hms.length; i < 3; ++i) hms[i] = 0; // hms.length<3 illegal
      utc = Date.UTC(ymd[0], ymd[1]-1, ymd[2], hms[0], hms[1], hms[2]);
      let mult = 0;
      if (timeSplitter == "+") mult = -1; else if (timeSplitter == "-") mult = 1;
      if (mult != 0) {
        let hm = timeOffset[1].split(":");
        // multiply since hm not integers
        utc = utc+mult*1000*(hm[0]*3600+hm[1]*60);
      }
    } else {
      utc = Date.UTC(ymd[0], ymd[1]-1, ymd[2], 0, 0, 0);
    }
    let ndate = new Date(utc);
    if (ndate.toString() == "Invalid Date" || ymd[0] < 1970 || ymd[1] > 12 || ymd[2] > 31) return null;
    return Math.floor(ndate.getTime()/1000);
  } catch (e) {
    return null;
  }
}


function rescueRFCDate (rfcDate) {
  try {
    let dateArray = rfcDate.split(" ");
    let yr = dateArray[3];
    if (yr.length == 2) yr = (yr < 70 ? "20"+yr: "19"+yr);
    dateArray[3] = yr;
    // From Bernhard Schelling bug#17681
    if (dateArray.length == 6 && isNaN(dateArray[5])) {
      const timezones = {
        "ACDT": "+1030",
        "ACST": "+0930",
        "ADT":  "-0300",
        "AEDT": "+1100",
        "AEST": "+1000",
        "AHST": "-1000",
        "AKDT": "-0800",
        "AKST": "-0900",
        "AST":  "-0400",
        "AT":   "-0200",
        "AWDT": "+0900",
        "AWST": "+0800",
        "BST":  "+0100",
        "BT":   "+0300",
        "CAT":  "-1000",
        "CCT":  "+0800",
        "CEDT": "+0200",
        "CEST": "+0200",
        "CET":  "+0100",
        "CXT":  "+0700",
        "EADT": "+1100",
        "EAST": "+1000",
        "EEDT": "+0300",
        "EEST": "+0300",
        "EET":  "+0200",
        "FST":  "+0200",
        "FWT":  "+0100",
        "GST":  "+1000",
        "HAA":  "-0300",
        "HAC":  "-0500",
        "HADT": "-0900",
        "HAE":  "-0400",
        "HAP":  "-0700",
        "HAR":  "-0600",
        "HAST": "-1000",
        "HAT":  "-0230",
        "HAY":  "-0800",
        "HDT":  "-0900",
        "HNA":  "-0400",
        "HNC":  "-0600",
        "HNE":  "-0500",
        "HNP":  "-0800",
        "HNR":  "-0700",
        "HNT":  "-0330",
        "HNY":  "-0900",
        "HST":  "-1000",
        "IDLE": "+1200",
        "IDLW": "-1200",
        "IST":  "+0100",
        "JST":  "+0900",
        "MEST": "+0200",
        "MESZ": "+0200",
        "MET":  "+0100",
        "MEWT": "+0100",
        "MEZ":  "+0100",
        "NDT":  "-0230",
        "NFT":  "+1130",
        "NST":  "-0330",
        "NT":   "-1100",
        "NZDT": "+1300",
        "NZST": "+1200",
        "NZT":  "+1200",
        "SST":  "+0200",
        "SWT":  "+0100",
        "UTC":  "-0000",
        "WADT": "+0800",
        "WAT":  "-0100",
        "WEDT": "+0100",
        "WEST": "+0100",
        "WET":  "-0000",
        "WST":  "+0800",
        "YDT":  "-0800",
        "YST":  "-0900",
        "ZP4":  "+0400",
        "ZP5":  "+0500",
        "ZP6":  "+0600"
      };
      let tz = String(dateArray[5]).toUpperCase();
      let tzs = timezones[tz];
      if (!tzs) {
        //Support for single letter military time zones
        if (dateArray[5].length == 1 && dateArray[5].match(/[A-I,K-Z]/)) {
          let i = dateArray[5].charCodeAt(0);
          i = (i == 90 ? 0 : i < 74 ? i-64 : i < 78 ? i-65 : 77-i);
          tzs = (i < -9 ? "-" : i < 0 ? "-0" : i < 10 ? "+0" : "+")+String(i < 0 ? 0-i : i)+"00";
        }
      }
      if (tzs) dateArray[5] = tzs;
    }
    let newString = dateArray.join(" ");
    let ndate = new Date(Date.parse(newString));
    if (ndate.toString() == "Invalid Date") return null;
    return Math.floor(ndate.getTime()/1000);
  } catch (e) {
    return parseTZDate(rfcDate);
  }
}


function parseRFCDate (rfcDate) {
  let ndate = new Date(Date.parse(rfcDate));
  if (ndate.toString() == "Invalid Date") {
    ndate = rescueRFCDate(rfcDate);
  } else {
    ndate = Math.floor(ndate.getTime()/1000);
  }
  return ndate;
}


////////////////////////////////////////////////////////////////////////////////
function parse (text) {
  let doc;

  if (typeof(text) === "string") {
    // so-called lj "developers" are complete shitheads; fix their idiocity
    let shitRE0 = /^\s*<\?xml version='1\.0' encoding='' \?>/;
    text = text.replace(shitRE0, "<?xml version='1.0' encoding='utf-8' ?>");
    // first load contents
    let parser = Cc["@mozilla.org/xmlextras/domparser;1"].createInstance(Ci.nsIDOMParser);
    //parser.init(null, uri, uri);
    doc = parser.parseFromString(text, "application/xml");
  } else {
    doc = text;
  }
  //conlog(doc);

  function dectectType () {
    const FeedTypes = [
      {name:"rss", ns:null},
      {name:"feed", ns:"http://purl.org/atom/ns#"},
      {name:"feed", ns:"http://www.w3.org/2005/Atom"}
    ];

    for (let fi of FeedTypes) {
      let list = doc.getElementsByTagNameNS(fi.ns, fi.name);
      if (list.length) {
        //conlog("got NS: [", fi.name, " : ", fi.ns, "]: ", list.length);
        if (fi.name == "rss") {
          let chan = list[0].getElementsByTagName("channel");
          if (chan.length) return {chan:chan[0], type:"rss", root:list[0]};
        } else {
          return {chan:list[0], type:"atom", root:list[0]};
        }
      }
    }
    // try rss 1.0 aka RDF
    let list = doc.getElementsByTagNameNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "RDF");
    if (list.length) {
      //let ns = doc.getElementsByTagNameNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "RDF")[0].getAttribute("xmlns");
      //conlog(ns);
      let chan = list[0].getElementsByTagName("channel");
      if (chan.length) return {chan:chan[0], type:"rdf", root:list[0]};
    }
    return null;
  }

  let info = dectectType();
  if (!info) throw new Error("can't detect feed type");
  //conlog("feed type: ", info.type, "; channel element: ", info.chan);


  function genGuid (item) {
    let cso = Cc["@mozilla.org/security/hash;1"].createInstance(Ci.nsICryptoHash);
    cso.init(cso.SHA1);
    let uco = Cc["@mozilla.org/intl/scriptableunicodeconverter"].createInstance(Ci.nsIScriptableUnicodeConverter);
    uco.charset = "UTF-8";
    if (item.title) cso.updateFromStream(uco.convertToInputStream(item.title), 0xffffffff);
    if (item.descr) cso.updateFromStream(uco.convertToInputStream(item.descr), 0xffffffff);
    //if (item.link) cso.updateFromStream(uco.convertToInputStream(item.link), 0xffffffff);
    //if (item.author) cso.updateFromStream(uco.convertToInputStream(item.author), 0xffffffff);
    let hash = cso.finish(false); // hash as raw octets
    //return [("0"+hash.charCodeAt(i).toString(16)).slice(-2) for (i in hash)].join("");
    var res = "";
    for (var i = 0; i < hash.length; ++i) res += ("0"+hash.charCodeAt(i).toString(16)).slice(-2);
    return res;
  }

  function normStr (s, dospaces) {
    if (dospaces) s = s.replace(/\s+/g, " ");
    return s.replace(/^\s+/, "").replace(/\s+$/, "");
  }

  function $ (el, namea, deftext) {
    if (typeof(deftext) !== "string") deftext = "";
    let name, attrn, attrv = null, attrg = null;
    if (typeof(namea) === "string") {
      name = namea;
      attrn = null;
    } else {
      name = namea[0];
      attrn = namea[1];
      attrv = namea[2];
      attrg = namea[3];
      if (!attrv) attrg = attrn;
    }
    for (let ell of el.getElementsByTagName(name)) {
      if (attrv) {
        if (attrn && ell.getAttribute(attrn) != attrv) continue;
        //if (attrn) conlog(" ", attrn, "=", ell.getAttribute(attrn), " : ", (ell.getAttribute(attrn) == attrv));
      } else {
        if (attrn && !ell.getAttribute(attrn)) continue;
      }
      if (attrg) {
        let val = ell.getAttribute(attrg);
        //conlog(" ", attrg, "=[", val, "]");
        if (typeof(val) === "string") return val||deftext;
      } else {
        return normStr(ell.textContent)||deftext;
      }
    }
    return deftext;
  }

  function parseRSSItems (feed, chan, itemlist) {
    // parse items
    let now = Math.floor((new Date()).getTime()/1000); // unixtime
    // get feed pubdate, if any
    let fpd = $(chan, "pubDate");
    if (!fpd) fpd = $(chan, "lastBuildDate");
    if (fpd) fpd = parseRFCDate(fpd);
    if (!fpd) fpd = now;
    //conlog("fpd: ", fpd.toUTCString());
    // get feed author, if any
    let fau = $(chan, "managingEditor");
    feed.items = [];
    for (let xit of itemlist) {
      //conlog("xit: ", xit.innerHTML);
      let item = {};
      item.title = $(xit, "title");
      item.body = $(xit, "description");
      let cde = $(xit, "content:encoded");
      if (cde.length) {
        //debuglog("***RSS: ENCODED: {"+cde+"}");
        if (item.body.length) item.body += "<br /><hr><br />";
        item.body += cde;
      }
      if (!item.title && !item.descr) continue; // empty item, skip it
      item.link = $(xit, "link");
      item.author = $(xit, "author", fau);
      item.guid = $(xit, "guid", item.link);
      if (!item.guid) try { item.guid = genGuid(item); } catch (e) { continue; } // no guid -- no item
      let pubdate = $(xit, "pubDate");
      if (pubdate) pubdate = parseRFCDate(pubdate);
      if (!pubdate) pubdate = fpd;
      item.pubdate = pubdate;
      try { item.hash = genGuid(item); } catch (e) { continue; } // i want hashes!
      feed.items.push(item);
    }
  }

  function parseRSS (info) {
    let chan = info.chan;
    let feed = {};
    feed.title = normStr($(chan, "title", "untitled feed"), true);
    feed.homeurl = $(chan, "link");
    feed.descr = normStr($(chan, "description"), true);
    parseRSSItems(feed, chan, chan.getElementsByTagName("item"));
    return feed;
  }

  function parseRDF (info) {
    let chan = info.chan;
    let feed = {};
    feed.title = normStr($(chan, "title", "untitled feed"), true);
    feed.homeurl = $(chan, "link");
    feed.descr = normStr($(chan, "description"), true);
    parseRSSItems(feed, chan, info.root.getElementsByTagName("item"));
    return feed;
  }

  function parseAtom (info) {
    function getAuthor (el, defnick) {
      let name;
      // lj idiocity
      let au = el.getElementsByTagName("lj:poster");
      if (au.length) {
        name = au[0].getAttribute("user");
        if (name) {
          name = normStr(""+name, true);
          if (name) {
            //conlog("LJ AUTHOR NAME: <"+name+">");
            return name;
          }
        }
      }
      if (defnick) {
        //conlog("defnick:<"+defnick+">");
        return defnick;
      }
      // normal atom
      au = el.getElementsByTagName("author");
      if (au.length == 0) return "";
      name = au[0].getElementsByTagName("name");
      if (name.length) {
        name = normStr(name[0].textContent, true);
        if (name) return name;
      }
      // no name, try email
      let email = au[0].getElementsByTagName("email");
      if (email.length) {
        email = normStr(email[0].textContent, true);
        if (email) return email;
      }
      return "";
    }

    function getContent (xit, tagname, isencoded) {
      for (let ct of xit.getElementsByTagName(tagname)) {
        let type = ct.getAttribute("type");
        if (type == "text" || type == "html") {
          // inline content
          let res = ct.textContent;
          if (type == "text") {
            res = normStr(res, true).
              replace(/&/g, '&amp;').
              replace(/</g, '&lt;').
              replace(/>/g, '&gt;').
              replace(/"/g, '&quot;'). //"
              replace(/'/g, '&apos;'); //'
          } else {
            res = normStr(res);
          }
          return res;
        } else if (type == "xhtml") {
          // subtree
          return normStr(ct.innerHTML);
        } else if (isencoded) {
          return normStr(ct.textContent);
        }
      }
      return "";
    }

    let chan = info.chan;
    let feed = {};
    feed.title = normStr($(chan, "title", "untitled feed"), true);
    feed.homeurl = $(chan, ["link", "rel", "alternate", "href"]);
    if (!feed.homeurl) feed.homeurl = $(chan, ["link", "href"]);
    feed.descr = normStr($(chan, "subtitle"), true);
    // get default author name
    let defnick = false;
    let ljj = chan.getElementsByTagName("lj:journal");
    if (ljj.length) ljj = ljj[0]; else ljj = null;
    if (ljj && ljj.getAttribute("type") == "personal") {
      defnick = ljj.getAttribute("username");
      if (typeof(defnick) !== "string" || !defnick) defnick = false;
    }
    // parse items
    let now = Math.floor((new Date()).getTime()/1000); // unix date, but in milliseconds
    // get feed pubdate, if any
    let fpd = $(chan, "updated");
    if (fpd) fpd = parseTZDate(fpd);
    if (!fpd) fpd = now;
    //conlog("fpd: ", fpd.toUTCString());
    // get feed author, if any
    let fau = getAuthor(chan, defnick);
    feed.items = [];
    for (let xit of chan.getElementsByTagName("entry")) {
      let item = {};
      item.title = $(xit, "title");
      //debuglog("***ATOM: trying 'content'");
      item.body = getContent(xit, "content");
      if (normStr(item.body).length == 0) {
        //debuglog("***ATOM: trying 'summary'");
        item.body = getContent(xit, "summary");
      }
      if (normStr(item.body).length == 0) {
        //debuglog("***ATOM: trying 'content:encoded'");
        item.body = getContent(xit, "content:encoded", true);
        //debuglog("***ENCODED:<"+item.body+">");
      }
      if (!item.title && !item.descr) continue; // empty item, skip it
      item.link = $(xit, ["link", "rel", "alternate", "href"]);
      if (!item.link) item.link = $(xit, ["link", "href"]);
      item.author = getAuthor(xit)||fau;
      item.guid = $(xit, "id", item.link);
      if (!item.guid) try { item.guid = genGuid(item); } catch (e) { continue; } // no guid -- no item
      let pubdate = $(xit, "published");
      if (!pubdate) pubdate = $(xit, "updated");
      if (pubdate) pubdate = parseRFCDate(pubdate);
      if (!pubdate) pubdate = fpd;
      item.pubdate = pubdate;
      try { item.hash = genGuid(item); } catch (e) { continue; } // i want hashes!
      feed.items.push(item);
    }
    return feed;
  }

  //debuglog("detected: '", info.type, "'");
  switch (info.type) {
    case "rss": return parseRSS(info);
    case "rdf": return parseRDF(info);
    case "atom": return parseAtom(info);
  }
  throw new Error("can't parse feed");
}
exports.parse = parse;


////////////////////////////////////////////////////////////////////////////////
exports.dumpFeed = function (feed) {
  conlog("feed home: ", feed.homeurl);
  conlog("feed title: ", feed.title);
  conlog("feed descr: ", feed.descr);
  conlog("feed items: ", feed.items.length);
  for (let item of feed.items) {
    conlog(" === item ===");
    conlog("  title: ", item.title);
    conlog("  link: ", item.link);
    conlog("  author: ", item.author);
    conlog("  guid: ", item.guid);
    conlog("  pubdate: ", item.pubdate);
    conlog("  hash: ", item.hash);
    conlog("  body: ", item.body);
  }
};


////////////////////////////////////////////////////////////////////////////////
function exposeProps (item, mode) {
  if (!("__exposedProps__" in item)) item.__exposedProps__ = {};
  mode = mode||"r";
  for (var k in item) {
    if (Object.prototype.hasOwnProperty.call(item, k)) {
      item.__exposedProps__[k] = mode;
    }
  }
  return item;
}


/*
function sbParseHtml (html, tagname) {
  tagname = tagname||"p";
  var p = document.createElement(tagname);
  if (html.match(/^<xhtml>/i)) {
    //var p = document.createElementNS(XHTML, tagname);
    html = html.replace(/^<xhtml>|<\/xhtml>$/gi, "");
    var xmlBody = new DOMParser(null, artURI, null).parseFromString(html, "text/xml");
    // Atom specification guarantees a single <div> element, now a <span>
    var xBody = document.importNode(xmlBody.childNodes[0], true);
    p.appendChild(xBody);
  } else {
    p.innerHTML = html;
  }
  return p;
}
*/


////////////////////////////////////////////////////////////////////////////////
let refreshingFeedsDictId = {}; // request objects
let refreshQueue = []; // object: fid, manual, inprogress


function doneRefreshing (fid, donext) {
  if (typeof(donext) === "undefined") donext = false;
  if (fid in refreshingFeedsDictId) delete refreshingFeedsDictId[fid];
  let idx = 0;
  let hasRefreshing = false;
  while (idx < refreshQueue.length) {
    let qobj = refreshQueue[idx];
    if (qobj.fid === fid) {
      // remove this item
      refreshQueue.splice(idx, 1);
    } else {
      if (!hasRefreshing && qobj.inprogress) hasRefreshing = true;
      ++idx;
    }
  }
  // if queue is not empty, do next feed
  if (donext && refreshQueue.length) {
    if (!hasRefreshing) {
      let qobj = refreshQueue[0];
      refreshFeed(qobj.fid, !qobj.manual);
    }
  } else {
    // if queue is empty, check for automatic updates
    if (donext && refreshQueue.length == 0) findAndStartNextUpdate();
  }
}


// feed: database feed object
// fo: parsed feed object
function updateFeed (fid, fo, manual) {
  let error = null;

  try {
    let feed = FeedsDB.db.getFeedById(fid);
    if (!feed) throw new Error("unknown feed with id "+fid);
    debuglog("got data for [", feed.url, "]");
    let db = FeedsDB.db;
    if (!feed.title && fo.title) {
      feed.title = fo.title;
      db.updateFeedTitleById(fid, fo.title);
      signals.emit("feed-title-updated", {fid:feed.id, title:fo.title});
    }

    // update feed meta
    if (!feed.homeurl && fo.homeurl) {
      //TODO: add guessing
      feed.homeurl = fo.homeurl;
      db.updateFeedHomeUrlById(fid, fo.homeurl);
    }
    if (!feed.descr && fo.descr) {
      feed.descr = fo.descr;
      db.updateFeedDescrById(fid, fo.descr);
    }

    // no icon, but got home url? update icon
    if (!feed.icon && feed.homeurl) queueFavIconUpdate(feed.id);

    // create signal object
    let sobj = {fid:feed.id, totalCount:0, newCount:0, unreadCount:0, alistNew:[], manual:manual, updateTotals:false};

    let aliveGuids = [];
    for (let fart of fo.items) aliveGuids.push(fart.guid);

    debuglog(":::[", feed.url, "]; got "+aliveGuids.length, " articles, filtering...");

    // get new articles; it is cached, so doing it is faster than filtering first
    let newGuids = db.getNewArticlesFromList(feed.id, aliveGuids);

    let newArts = [];
    for (let fart of fo.items) { if (fart.guid in newGuids) newArts.push(fart); }

    // if we have no new articles, simply send completion signal (to restore feed icon) and quit
    if (newArts.length == 0) {
      debuglog(":::[", feed.url, "]; no new articles found.");
      FeedsDB.db.updateFeedSetUpLast(fid);
      signals.emit("feed-update-complete", sobj);
      doneRefreshing(fid, true);
      return;
    }

    // filter with scripts, and build list of alive articles to put to db

    // prepare js sandbox
    const newFileURI = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService).newFileURI;
    let sandbox = null, url;
    let xfid = ""+fid;
    try {
      let fljs = userdirs.JSFeed;
      fljs.append("feed_"+fid+".js");
      if (fljs.exists() && fljs.isFile() && fljs.isReadable()) {
        let text = fileReadText(fljs);
        url = newFileURI(fljs).spec;
        debuglog("read JS filtering script: ", url);
        //let pcp = Cc["@mozilla.org/systemprincipal;1"].createInstance(Ci.nsIPrincipal);
        let pcp = Cc["@mozilla.org/nullprincipal;1"].createInstance(Ci.nsIPrincipal);
        sandbox = Cu.Sandbox(pcp, {
          sandboxName: "feed filtering sandbox",
          //sameZoneAs: iframe.contentWindow, // help GC a little
          //sandboxPrototype: {}/*.wrappedJSObject*/,
          wantXrays: false,
          //wantXHRConstructor: true,
        });
        //TODO: feed object
        sandbox.log = function () {
          let s = "";
          for (let f = 0; f < arguments.length; ++f) s += ""+arguments[f];
          conlog("KNRSSFILTER("+xfid+"): ", s);
        };
        //sandbox.parseHtml = sbParseHtml;
        // init script
        Cu.evalInSandbox(text, sandbox, "ECMAv5", url, 1);
      }
    } catch (e) {
      logException("KNRSSFILTER("+xfid+")", e);
      if (sandbox) {
        Cu.nukeSandbox(sandbox);
        sandbox = null;
      }
    }

    let unixNow = Math.floor((new Date()).getTime()/1000);
    sobj.unixNow = unixNow;

    let xxxcount = 0;
    for (let fidx = 0; fidx < newArts.length; ++fidx) {
      let fart = newArts[fidx];
      let skipit = false;
      fart.hitdate = unixNow;
      fart.state = flags.ASTATE.UNREAD;
      // run filter script
      if (sandbox) {
        sandbox.item = exposeProps(fart, "rw");
        try {
          skipit = Cu.evalInSandbox("main()", sandbox, "ECMAv5", url, 1);
          skipit = (skipit === false);
        } catch (e) {
          logException("KNFILTER", e);
        }
      }
      fart.fid = feed.id;
      if (!fart.hitdate) fart.hitdate = unixNow;
      if (skipit) {
        // this is deleted article; put it into database anyway, but without body
        fart.body = "";
        fart.state = flags.ASTATE.DELETED;
        ++xxxcount;
      }
    }
    // kill sandbox
    if (sandbox) { Cu.nukeSandbox(sandbox); sandbox = null; }

    debuglog(":::[", feed.url, "]; got "+newArts.length, " new articles ("+xxxcount+" filtered out)");

    db.insertNewArticlesFromListAsync(newArts, sobj, function (sobj) {
      // done
      debuglog(":::[", feed.url, "]; FINISHCB; totalCount="+sobj.totalCount+"; newCount="+sobj.newCount+"; unreadCount="+sobj.unreadCount+"; alnlen="+sobj.alistNew.length+"; uptotals="+sobj.updateTotals);
      signals.emit("feed-update-complete", sobj);
      if (sobj.updateTotals) db.queryTotalUnread();
    });

    /*
    //FIXME: move this to FeedsDB
    db.beginExclusiveTransaction();
    try {
      // get feed info
      let nfo = db.getFeedInfoById(feed.id);
      sobj.unreadCount = nfo.unread;
      sobj.totalCount = nfo.total;
      for (let fart of newArts) {
        / *let art = db.getArticleByFidAndGuid(feed.id, fart.guid, false); // without body
        if (art) {
          // found article, check for updates (not now, really)
          // update article hit info
          // k8: don't constantly update database
          //!db.articleWasHit(art.id, unixNow);
        } else* /
        {
          // this is new article, insert it
          let aid = db.articleInsertWithObj(fart);
          if (fart.state < flags.ASTATE.DELMARKED) {
            sobj.updateTotals = true;
            ++sobj.totalCount;
            ++sobj.newCount;
            if (fart.state == flags.ASTATE.UNREAD) ++sobj.unreadCount;
            sobj.alistNew.push(aid);
          }
        }
      }
      // update feed info (only if something's changed)
      if (sobj.updateTotals) db.updateFeedTotals(feed.id, sobj.unreadCount, sobj.totalCount, unixNow, false);
      db.commit();
    } catch (e) {
      db.dumpDBError();
      db.rollback();
      throw e;
    }
    // update total unread stats
    //debuglog("sending complete signal: ", JSON.stringify(sobj));
    signals.emit("feed-update-complete", sobj);
    if (sobj.updateTotals) db.queryTotalUnread();
    */
  } catch (e) {
    error = true;
    //logException("FEED PARSE ["+url+"] ("+title+")", e);
  }

  if (error) {
    FeedsDB.db.updateFeedFails(fid, true);
    doneRefreshing(fid, true);
    signals.emit("feed-update-error", {fid:fid, manual:manual});
  } else {
    FeedsDB.db.updateFeedSetUpLast(fid);
    doneRefreshing(fid, true);
  }
}


exports.cancelRefreshingFeed = function (fid) {
  let nfo = refreshingFeedsDictId[fid];
  if (nfo) {
    FeedsDB.db.updateFeedSetUpLast(fid);
    doneRefreshing(fid, true);
    if ("req" in nfo) nfo.req.abort();
    signals.emit("feed-update-cacelled", {fid:fid, manual:nfo.manual});
  }
};


exports.cancelAllRefreshing = function () {
  for (let [fid, nfo] in Iterator(refreshingFeedsDictId)) {
    if (nfo) {
      try {
        if ("req" in nfo) nfo.req.abort();
        signals.emit("feed-update-cacelled", {fid:fid, manual:nfo.manual});
      } catch (e) {}
    }
  }
  refreshingFeedsDictId = {};
  refreshQueue = [];
};


function refreshFeed (fid, automatic) {
  if (typeof(fid) !== "number") return;
  let manual = (typeof(automatic) === "undefined" ? true : !automatic);
  debuglog("requesting refresh for ", fid);
  if (fid in refreshingFeedsDictId) {
    debuglog("refresh for ", fid, " is already in progress");
    return;
  }
  doneRefreshing(fid, false);
  let feed = FeedsDB.db.getFeedById(fid);
  if (!feed) {
    // unknown feed
    debuglog("refused to refresh unknown feed ", fid);
    doneRefreshing(fid, true); // ping queue
    return;
  }
  let url = feed.url;
  let title = feed.title||"<unknown>";
  let sendCookies = (feed.flags&flags.FFLAG.SEND_COOKIES);
  //if (!feed.icon && feed.homeurl) queueFavIconUpdate(feed.id);
  feed = null; // free object

  // sort feed list, why not?
  if (refreshQueue.length) {
    refreshQueue.sort(function (a, b) {
      if (a.manual && !b.manual) return -1;
      if (b.manual && !a.manual) return 1;
      if (a.fid < b.fid) return -1;
      if (a.fid > b.fid) return 1;
      return 0;
    });
  }

  debuglog("refreshing [", url, "]");
  let nfo = {fid:fid, manual:manual};
  refreshingFeedsDictId[fid] = nfo;
  refreshQueue.push({fid:fid, manual:manual, inprogress:true});
  FeedsDB.db.updateFeedSetUpLast(fid);
  signals.emit("feed-update-start", nfo);
/*
  if (addonOptions.updateTimerLogsEnabled) {
    conlog("KORENEWS: refresing feed "+fid+" ["+url+"]\n"+
           "KORENEWS: "+refreshQueue.length+" feeds in refresh queue:"+buildRefresQueueString());
  }
*/
  nfo.req = /*loadXMLContentsAsync*/loadTextContentsAsync(url, function (text) {
    let error = (text === null);
    let fo;
    if (text && fid in refreshingFeedsDictId) {
      try {
        fo = parse(text);
        //updateFeed(fid, fo, manual);
        //doneRefreshing(fid, true);
      } catch (e) {
        error = true;
        logException("FEED PARSE ["+url+"] ("+title+")", e);
      }
    }
    if (error) {
      FeedsDB.db.updateFeedFails(fid, true);
      doneRefreshing(fid, true);
      signals.emit("feed-update-error", {fid:fid, manual:manual});
    } else {
      updateFeed(fid, fo, manual);
    }
  }, (sendCookies ? addCookies : null));
}
exports.refreshFeed = refreshFeed;


////////////////////////////////////////////////////////////////////////////////
function queueRefresh (fid, automatic) {
  if (typeof(fid) !== "number") return;
  automatic = (typeof(automatic) === "undefined" ? false : !!automatic);
  if (!automatic) debuglog("requested manual refresh for feed ", fid);
  // check if we already has this in queue
  for (let [idx, qobj] in Iterator(refreshQueue)) {
    if (qobj.fid === fid) {
      // found it; move to top if this check is manual
      if (idx > 0 && !automatic) {
        if (!automatic) debuglog("queue bump for manual refresh for feed ", fid);
        qobj.manual = true;
        refreshQueue.splice(idx, 1);
        refreshQueue.splice(0, 0, qobj);
      }
      return;
    }
  }
  // no, add this to queue/do refresh
  if (refreshQueue.length == 0) {
    // queue is empty, do refresh right now
    if (!automatic) debuglog("direct refresh for manual refresh for feed ", fid);
    refreshFeed(fid, automatic);
  } else {
    // queue is not empty, append
    if (automatic) {
      refreshQueue.push({fid:fid, manual:!automatic, inprogress:false});
    } else {
      // or insert to top
      if (!automatic) debuglog("top-insert for manual refresh for feed ", fid);
      refreshQueue.splice(0, 0, {fid:fid, manual:!automatic, inprogress:false});
    }
  }
}
exports.queueRefresh = queueRefresh;


////////////////////////////////////////////////////////////////////////////////
let uptimer = null;


function buildRefresQueueString () {
  let list = "";
  for (let f = 0; f < refreshQueue.length; ++f) list += " "+refreshQueue[f].fid;
  return list;
}


function findAndStartNextUpdate () {
  //debuglog("searching for next update: ", addonOptions.active, " : ", refreshQueue.length);
  if (uptimer) uptimer.cancel();
  uptimer = null;
  if (!addonOptions.active) {
    refreshQueue = [];
    return;
  }
  if (refreshQueue.length) {
    for (let [idx, qobj] in Iterator(refreshQueue)) {
      if (qobj.inprogress) {
        if (addonOptions.updateTimerLogsEnabled) {
          conlog("KORENEWS: "+refreshQueue.length+" feeds in refresh queue, "+qobj.fid+" in progress, nothing to do:"+buildRefresQueueString());
        }
        return;
      }
    }
    // no feeds in progress, ping first
    if (addonOptions.updateTimerLogsEnabled) {
      conlog("KORENEWS: "+refreshQueue.length+" feeds in refresh queue, pinging:"+buildRefresQueueString());
    }
    let qobj = refreshQueue[0];
    refreshFeed(qobj.fid, !qobj.manual);
    return;
  }
  debuglog("searching for next update...");
  FeedsDB.db.findNextFeedToUpdate(function (fid, nextup) {
    if (!addonOptions.active) return;
    if (fid === null) {
      if (refreshQueue.length > 0) {
        for (let [idx, qobj] in Iterator(refreshQueue)) {
          if (qobj.inprogress) {
            if (addonOptions.updateTimerLogsEnabled) {
              conlog("KORENEWS: "+refreshQueue.length+" feeds in refresh queue, "+qobj.fid+" in progress, nothing to do:"+buildRefresQueueString());
            }
            return;
          }
        }
        // no feeds in progress, ping first
        if (addonOptions.updateTimerLogsEnabled) {
          conlog("KORENEWS: "+refreshQueue.length+" feeds in refresh queue, pinging:"+buildRefresQueueString());
        }
        let qobj = refreshQueue[0];
        refreshFeed(qobj.fid, !qobj.manual);
        return;
      }
      // either nothing, or next update
      if (nextup !== null) {
        // got next update time
        let now = new Date();
        let ncmsecs = Math.floor(nextup-now+1000);
        if (typeof(ncmsecs) == "number") {
          if (nextup <= now) ncmsecs = 1000;
          debuglog("scheduling next check: ", ncmsecs, "; nextup=", nextup);
          if (addonOptions.updateTimerLogsEnabled) conlog("KORENEWS: scheduling next check: ", Math.floor(ncmsecs/1000)/60, "; nextup=", nextup);
          uptimer = timer.oneShotTimer(findAndStartNextUpdate, ncmsecs);
          return;
        } else {
          conlog("KORENEWS: fucked update interval: ", nextup);
        }
      }
    } else {
      debuglog("autoupdating feed ", fid);
      queueRefresh(fid, true);
      return;
    }
    // nothing was found
    let iv = addonOptions.upinterval_minutes;
    if (iv < 5) iv = 5;
    iv *= 60; // minutes -> seconds
    iv *= 1000; // seconds -> milliseconds
    uptimer = timer.oneShotTimer(findAndStartNextUpdate, iv);
    if (addonOptions.updateTimerLogsEnabled) conlog("KORENEWS: no feeds to check; next check in ", iv, " minutes");
  });
}
exports.findAndStartNextUpdate = findAndStartNextUpdate;


////////////////////////////////////////////////////////////////////////////////
exports.addFeedFromUrl = function (feedUrl) {
  let feed = FeedsDB.db.getFeedByUrl(feedUrl);
  if (feed) {
    debuglog("duplicate feed: ", feedUrl);
    return;
  }
  debuglog("new feed url: ", feedUrl);
  debuglog("downloading [", feedUrl, "]");
  let req = /*loadXMLContentsAsync*/loadTextContentsAsync(feedUrl, function (text) {
    let error = (text === null);
    if (text) {
      try {
        let fo = parse(text);
        let fd = {
          title: fo.title||"<unknown title>",
          url: feedUrl,
          homeurl: fo.homeurl||guessHome(fo.url),
          groups: "",
          noUpdates: false,
          sendCookies: false,
          customRefresh: true,
          refreshInterval: 60*60, // 1 hour
        };
        appendParsedFeeds([fd]);
      } catch (e) {
        error = true;
        logException("FEED PARSE ["+feedUrl+"]", e);
      }
      if (error) debuglog("error adding feed from: ", feedUrl);
    } else {
      logError("got no data from feed: ", feedUrl);
    }
  }, null);
};
