/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
let signals = require("signals");
let FeedsDB = require("feedsdb");


////////////////////////////////////////////////////////////////////////////////
exports.parse = function (text) {
  let parser = Cc["@mozilla.org/xmlextras/domparser;1"].createInstance(Ci.nsIDOMParser);
  //parser.init(null, uri, uri);
  let doc = parser.parseFromString(text, "application/xml");

  let curGroups = [];

  const interestingFields = {
    xmlUrl: "url",
    htmlUrl: "url",
    NFautoCheck: "boolean",
    NFsendCookies: "boolean",
    NFautoRefreshInterval: "integer",
  };

  let feeds = [];
  let feedIdx = {};

  function doItems (el) {
    let curg = curGroups.join(",");
    for (; el; el = el.nextSibling) {
      if (el.tagName == "outline") {
        let type = el.getAttribute("type");
        let text = el.getAttribute("text");
        if (type == "rss") {
          let fields = {};
          for (let [fname, ftype] in Iterator(interestingFields)) {
            let a = el.getAttribute(fname);
            if (typeof(a) !== "string") continue;
            switch (ftype) {
              case "url": case "string": fields[fname] = a; break;
              case "integer":
                let i = parseInt(a, 10);
                if (isNaN(i)) throw new Error("invalid integer value for field '"+fname+"': '"+a+"'");
                fields[fname] = i;
                break;
              case "boolean": fields[fname] = (a == "true"); break;
            }
          }
          if (!fields.xmlUrl) {
            conlog("url-less feed: '", text, "'");
            continue;
          }
          if (!text) text = fields.xmlUrl;
          if (fields.xmlUrl in feedIdx) {
            // been there, seen that
            let item = feedIdx[fields.xmlUrl];
            item.groups += ","+curg;
          } else {
            let item = {url:fields.xmlUrl, title:text};
            item.homeurl = (fields.htmlUrl ? fields.htmlUrl : "");
            item.noUpdates = !fields.NFautoCheck;
            item.sendCookies = !!fields.NFsendCookies;
            item.customRefresh = !!fields.NFautoRefreshInterval;
            item.refreshInterval = (fields.NFautoRefreshInterval ? fields.NFautoRefreshInterval*60 : 20*60);
            item.groups = curg;
            feeds.push(item);
            feedIdx[item.url] = item;
          }
        } else if (type == "NFgroup") {
          // new group
          //conlog("ENTERING GROUP '", text, "'");
          if (text) curGroups.push(text);
          doItems(el.firstChild);
          //conlog("LEAVING GROUP '", text, "'");
          curGroups.pop();
        }
      }
    }
  }

  doItems(doc.getElementsByTagName("body")[0].firstChild);

  // remove "FEEDS" group, cleanup groups
  for (let feed of feeds) {
    let groupCache = {};
    let groups = [];
    for (let gn of feed.groups.split(",")) {
      if (!gn || (gn in groupCache) || gn == "FEEDS") continue;
      groupCache[gn] = true;
      groups.push(gn);
    }
    if (groups.length == 0) conlog("orphan feed: ", feed.title);
    feed.groups = groups.join(",");
  }

  return feeds;
};


////////////////////////////////////////////////////////////////////////////////
exports.dumpFeeds = function (feeds) {
  for (let feed of feeds) {
    conlog(
      "==============================================\n",
      "title          : ", feed.title, "\n",
      "url            : ", feed.url, "\n",
      "home           : ", feed.homeurl, "\n",
      "groups         : ", feed.groups, "\n",
      "noUpdates      : ", feed.noUpdates, "\n",
      "sendCookies    : ", feed.sendCookies, "\n",
      "customRefresh  : ", feed.customRefresh, "\n",
      "refreshInterval: ", feed.refreshInterval
    );
  }
};


////////////////////////////////////////////////////////////////////////////////
// db: FeedsDB instance
exports.appendParsedFeeds = function (feeds) {
  let db = FeedsDB.db;
  let fl = [];
  db.beginExclusiveTransaction();
  try {
    // update database
    for (let feed of feeds) {
      //debuglog("adding feed '", feed.title, "'  [", feed.url, "]");
      fl.push(db.addFeedFromObject(feed));
    }
    db.commit();
  } catch (e) {
    db.dumpDBError();
    db.rollback();
    throw e;
  }
  // emit signals
  // update object:
  //   int[] newGroups
  //   int[] updatedGroups  (not new)
  //   int[] deletedGroups  (ids)
  //   int[] newFeeds
  //   int[] updatedFeeds   (not new)
  //   int[] deletedFeeds
  //debuglog("sending signal...");
  let upobj = {newGroups:[], updatedGroups:[], newFeeds:[], updatedFeeds:[], deletedGroups:[], deletedFeeds:[]};
  for (let fd of fl) {
    // push feed id
    if (fd.newFeed) upobj.newFeeds.push(fd.id);
    else if (fd.updatedFeed) upobj.updatedFeeds.push(fd.id);
    // push new group ids
    for (let gi of fd.newGroups) {
      if (upobj.newGroups.indexOf(gi) < 0) upobj.newGroups.push(gi);
    }
  }
  // now push refresed group ids
  // this is done as separate step, 'cause some feeds can refresh new groups
  for (let fd of fl) {
    for (let gi of fd.updatedGroups) {
      if (upobj.newGroups.indexOf(gi) >= 0) continue; // this feed is new
      if (upobj.updatedGroups.indexOf(gi) < 0) upobj.updatedGroups.push(gi);
    }
  }
  // send signal only if something was changed
  if (upobj.newFeeds.length || upobj.updatedFeeds.length ||
      upobj.newGroups.length || upobj.updatedGroups.length)
  {
    signals.emit("model-changed", upobj);
  }
};
