/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
let flags = require("flags");
let signals = require("signals");
let {sqlSplit} = require("dbutils");

let allowFeedObjCache = false;


////////////////////////////////////////////////////////////////////////////////
function FeedsDB () {
  this.dbObj = null;
}


////////////////////////////////////////////////////////////////////////////////
FeedsDB.prototype._getnsIFile = function () {
  let fl = userdirs.DBDir;
  fl.append("korenews.db");
  return fl;
};


////////////////////////////////////////////////////////////////////////////////
FeedsDB.prototype.open = function () {
  if (!this.dbObj) {
    if (this.dbObj === false) throw new Error("DATABASE FILE ERROR!");
    this.dbObj == false; // so we will not try to recreate DB each time
    let list = sqlSplit(loadTextContents("mainjs:dbschema/dbschema.sql"));
    let dbFile = this._getnsIFile();
    //debuglog("dbfile: [", dbFile.path, "]; size=", dbFile.fileSize);
    if (dbFile.exists() && (!dbFile.isFile() || !dbFile.isReadable() || !dbFile.isWritable())) {
      throw new Error("DB FILE ERROR!");
    }
    let createNew = (dbFile.exists() ? (dbFile.fileSize == 0) : true);
    //dbObj = Services.storage.openDatabase(dbFile);
    let dbsvc = Cc["@mozilla.org/storage/service;1"].getService(Ci.mozIStorageService);
    let dbObj = dbsvc.openDatabase(dbFile);
    if (createNew) {
      try {
        debuglog("KoreNews: opening database...");
        for (let op of list) dbObj.executeSimpleSQL(op);
        debuglog("KoreNews: database opened");
      } catch (e) {
        dbObj.close();
        throw e;
      }
      // default top-level group has id of '0'
      let st = dbObj.createStatement("SELECT id FROM groups WHERE id=0 LIMIT 1");
      if (!st.step()) {
        // no such group, add one
        dbObj.executeSimpleSQL("INSERT INTO groups (id,title,feeds,treeindex) VALUES(0,'','',0)");
      }
      st.reset();
    } else {
      let st = dbObj.createStatement("SELECT id FROM groups WHERE id=0 LIMIT 1");
      st.step();
      st.reset();
    }
    this.dbObj = dbObj;
    this._fudGuidCache = {}; // indexed by fid
    this._feedObjCache = {}; // indexed by fid
  }
  return this.dbObj;
};


FeedsDB.prototype.__defineGetter__("db", function () this.open());


////////////////////////////////////////////////////////////////////////////////
FeedsDB.prototype.simpleSQL = function (sql) {
  for (let ops of sqlSplit(sql)) {
    try {
      this.db.executeSimpleSQL(ops);
    } catch (e) {
      logError("FAILED SQL: ", ops);
      throw e;
    }
  }
};


////////////////////////////////////////////////////////////////////////////////
FeedsDB.prototype.dumpDBError = function () {
  if (this.db.lastError) logError("DB ERROR ", this.db.lastError, ": ", this.db.lastErrorString);
};


FeedsDB.prototype.beginExclusiveTransaction = function () {
  let db = this.db;
  if (db.transactionInProgress) throw new Error("nested transactions aren't supported");
  db.beginTransactionAs(db.TRANSACTION_EXCLUSIVE);
};


// return `true` if transaction was started
FeedsDB.prototype.beginExclusiveTransactionIfNone = function () {
  let db = this.db;
  if (db.transactionInProgress) return false;
  db.beginTransactionAs(db.TRANSACTION_EXCLUSIVE);
  return true;
};


FeedsDB.prototype.commit = function () {
  let db = this.db;
  if (!db.transactionInProgress) throw new Error("can't COMMIT without transaction");
  db.commitTransaction();
};


FeedsDB.prototype.rollback = function () {
  let db = this.db;
  if (!db.transactionInProgress) throw new Error("can't ROLLBACK without transaction");
  db.rollbackTransaction();
};


////////////////////////////////////////////////////////////////////////////////
FeedsDB.prototype._doTransacted = function (func) {
  if (typeof(func) !== "function") throw new Error("function expected");
  let doTrans = this.beginExclusiveTransactionIfNone();
  try {
    let res = func.apply(this);
    if (doTrans) this.commit();
    return res;
  } catch (e) {
    this.dumpDBError();
    if (doTrans) this.rollback();
    throw e;
  }
};


////////////////////////////////////////////////////////////////////////////////
function fillGroupObject (obj, st) {
  obj.id = st.row.id;
  obj.title = st.row.title;
  obj.feeds = [];
  for (let fid of st.row.feeds.split(",")) {
    if (!fid) continue;
    let n = parseInt(fid, 10);
    if (isNaN(n)) continue;
    obj.feeds.push(n);
  }
  return obj;
}


// null or object with all fields from "groups" table
FeedsDB.prototype.getGroupByTitle = function (title) {
  if (typeof(title) !== "string") throw new Error("string title expected");
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  let st = this.db.createStatement("SELECT id,title,feeds FROM groups WHERE title=:title LIMIT 1");
  st.params.title = title;
  let obj = null;
  if (st.step()) obj = fillGroupObject({}, st);
  st.reset();
  return obj;
};


// null or object with all fields from "groups" table
FeedsDB.prototype.getGroupById = function (id) {
  if (typeof(id) !== "number") throw new Error("integer id expected");
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  let st = this.db.createStatement("SELECT id,title,feeds FROM groups WHERE id=:id LIMIT 1");
  st.params.id = id;
  let obj = null;
  if (st.step()) obj = fillGroupObject({}, st);
  st.reset();
  return obj;
};


// sets 'bool .newGroup' field in resulting object
FeedsDB.prototype.getOrAddGroupByTitle = function (title) {
  if (typeof(title) !== "string") throw new Error("string title expected");
  return this._doTransacted(function () {
    //debuglog("getOrAddGroupByTitle: '", title, "'");
    let grp = this.getGroupByTitle(title);
    //debuglog("grp0: ", JSON.stringify(grp));
    if (!grp) {
      // no such group, create new
      let st = this.db.createStatement("INSERT INTO groups (title,treeindex) SELECT :title, MAX(treeindex)+1 FROM groups");
      st.params.title = title;
      st.step();
      st.reset();
      grp = {
        id: this.db.lastInsertRowID,
        title: title,
        feeds: [],
        newGroup: true,
      };
    } else {
      grp.newGroup = false;
    }
    //debuglog("grp1: ", JSON.stringify(grp));
    return grp;
  });
};


// feeds: array of integers
// duplicates will be removed
FeedsDB.prototype.updateGroupFeedsById = function (id, feeds) {
  if (typeof(id) !== "number") throw new Error("integer id expected");
  if (typeof(feeds) === "undefined" || (feeds !== null && !(feeds instanceof Array))) throw new Error("integer array of feeds expected");
  this._feedObjCache = {}; // reset feed cache
  // build feed string
  let fstr = "";
  if (feeds) {
    let fdict = {};
    for (let id of feeds) {
      if (typeof(id) !== "number") throw new Error("numeric feed id expected, got '"+id+"'");
      id = Math.floor(id);
      if (!(id in fdict)) {
        fdict[id] = true;
        fstr += ","+id;
      }
    }
    fstr = fstr.substr(1);
  }
  let st = this.db.createStatement("UPDATE groups SET feeds=:feedstr WHERE id=:id");
  st.params.id = id;
  st.params.feedstr = fstr;
  st.step();
  st.reset();
};


// returns array with two object:
//   int id
//   int treeindex
// or null
// dir: <0: up; >0: down
function moveGroupUpDown (db, id, dir) {
  if (typeof(id) !== "number") throw new Error("integer id expected");
  if (id < 1) return null; // can't move these groups
  if (typeof(dir) !== "number" || dir == 0) throw new Error("invalid direction");

  let prevIndex = -1, curIndex = -1, prevId = -1;

  function getIndexes () {
    // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
    let st = db.createStatement("SELECT id,treeindex FROM groups WHERE id>0 ORDER BY treeindex ASC");
    while (st.step()) {
      if (dir < 0) {
        // up
        if (st.row.id === id) {
          curIndex = st.row.treeindex;
          break;
        }
        prevId = st.row.id;
        prevIndex = st.row.treeindex;
      } else {
        // down
        if (st.row.id === id) {
          curIndex = st.row.treeindex;
          if (st.step()) {
            prevId = st.row.id;
            prevIndex = st.row.treeindex;
          }
          break;
        }
      }
    }
    st.reset();
  }

  let res = null;
  getIndexes();
  if (prevId > 0 && prevIndex >= 0 && curIndex > 0) {
    // group found, and it's not first: swap treeindexes
    db.executeSimpleSQL("UPDATE groups SET treeindex="+prevIndex+" WHERE id="+id);
    db.executeSimpleSQL("UPDATE groups SET treeindex="+curIndex+" WHERE id="+prevId);
    res = [
      {id:id, treeindex:prevIndex},
      {id:prevId, treeindex:curIndex},
    ];
  }
  return res;
}


// move given group up in tree, fixing treeindexes
// returns array with two object:
//   int id
//   int treeindex
// or null
FeedsDB.prototype.groupMoveUp = function (id) this._doTransacted(function () moveGroupUpDown(this.db, id, -1));
FeedsDB.prototype.groupMoveDown = function (id) this._doTransacted(function () moveGroupUpDown(this.db, id, 1));


////////////////////////////////////////////////////////////////////////////////
/* not used anymore
FeedsDB.prototype.getFeedInfoById = function (id) {
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  let st = this.db.createStatement("SELECT uplast,upfailed,total,unread FROM feeds WHERE id=:id LIMIT 1");
  st.params.id = id;
  let res = null;
  if (st.step()) {
    res = {
      id: id,
      uplast: st.row.uplast,
      upfailed: st.row.upfailed,
      total: st.row.total,
      unread: st.row.unread,
    };
  } else {
    res = {id:id, uplast:0, upfailed:0, total:0, unread:0};
  }
  st.reset();
  return res;
};
*/


FeedsDB.prototype.updateFeedSetUpLast = function (fid) {
  if (typeof(fid) !== "number") throw new Error("wtf?!");
  let st = this.db.createAsyncStatement("UPDATE feeds SET uplast=:uplast WHERE id=:fid");
  st.params.fid = fid;
  st.params.uplast = Math.floor((new Date()).getTime()/1000);
  this._doAsync(st, function (row, succ) {
  });
}


FeedsDB.prototype.updateFeedFails = function (fid) {
  if (typeof(fid) !== "number") throw new Error("wtf?!");

  let unixNow = Math.floor((new Date()).getTime()/1000);
  fid = Math.floor(fid);

  if (fid in this._feedObjCache) {
    if (!allowFeedObjCache) {
      delete this._feedObjCache[fid];
    } else {
      let obj = this._feedObjCache[fid];
      if (typeof(obj.upfailed) === "number" && typeof(obj.uplast) === "number") {
        obj.upfailed += 1;
        obj.uplast = unixNow;
      } else {
        delete this._feedObjCache[fid];
      }
    }
  }

  this._doTransacted(function () {
    this.simpleSQL("UPDATE feeds SET upfailed=upfailed+1,uplast="+unixNow+" WHERE id="+fid);
  });
};


/* not used anymore; was used in old update scheme
FeedsDB.prototype.updateFeedTotals = function (fid, countUnread, countTotal, uplast, waserr) {
  if (typeof(fid) !== "number") throw new Error("integer id expected");

  //this._fudGuidCache[fid] = {}; // reset feed cache

  // build query
  let str = "UPDATE feeds SET unread=:unread, total=:total";
  if (typeof(uplast) !== "undefined" && uplast !== null) {
    if (typeof(uplast) !== "number") throw new Error("invalid date value: '"+uplast+"'");
    str += ", uplast="+Math.floor(uplast);
  }
  if (typeof(waserr) !== "undefined") {
    if (waserr !== null) {
      str += ", upfailed=";
      str += (waserr ? "upfailed+1" : "0");
    }
  }
  str += " WHERE id=:fid";

  this._doTransacted(function () {
    let st = this.db.createStatement(str);
    st.params.fid = fid;
    st.params.unread = countUnread;
    st.params.total = countTotal;
    st.step();
    st.reset();
  });
};
*/


////////////////////////////////////////////////////////////////////////////////
function populateFeedObj (obj, st) {
  obj.id = st.row.id;
  obj.url = st.row.url;
  obj.homeurl = st.row.homeurl;
  obj.icon = st.row.icon;
  obj.title = st.row.title;
  obj.descr = st.row.descr;
  obj.flags = st.row.flags;
  obj.upinterval = st.row.upinterval;
  obj.uplast = st.row.uplast;
  obj.upfailed = st.row.upfailed;
  obj.total = st.row.total;
  obj.unread = st.row.unread;
  return obj;
}


function copyFeedObj (src) {
  let obj = {};
  obj.id = src.id;
  obj.url = src.url;
  obj.homeurl = src.homeurl;
  obj.icon = src.icon;
  obj.title = src.title;
  obj.descr = src.descr;
  obj.flags = src.flags;
  obj.upinterval = src.upinterval;
  obj.uplast = src.uplast;
  obj.upfailed = src.upfailed;
  obj.total = src.total;
  obj.unread = src.unread;
  return obj;
}


// null or object with all fields from "feeds" table
// this is called only when we're trying to add feed from url, so speed doesn't matter
FeedsDB.prototype.getFeedByUrl = function (url) {
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  let st = this.db.createStatement("SELECT * FROM feeds WHERE url=:url LIMIT 1");
  st.params.url = ""+(url.spec ? url.spec : url);
  let obj = null;
  if (st.step()) {
    obj = populateFeedObj({}, st);
    if (allowFeedObjCache) this._feedObjCache[obj.id] = copyFeedObj(obj); // why not?
  }
  st.reset();
  return obj;
};


// null or object with all fields from "feeds" table
FeedsDB.prototype.getFeedById = function (fid) {
  if (fid in this._feedObjCache) {
    if (allowFeedObjCache) return copyFeedObj(this._feedObjCache[fid]);
    delete this._feedObjCache[fid];
  }
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  let st = this.db.createStatement("SELECT * FROM feeds WHERE id=:id LIMIT 1");
  st.params.id = fid;
  let obj = null;
  if (st.step()) {
    obj = populateFeedObj({}, st);
    if (allowFeedObjCache) this._feedObjCache[obj.id] = copyFeedObj(obj); // update cache
  }
  st.reset();
  return obj;
};


// object fields:
//   string title
//   string url
//   string homeurl
//   string groups (comma-delimited)
//   bool noUpdates
//   bool sendCookies
//   bool customRefresh
//   int refreshInterval (in seconds)
// returns feed object (not the one that was passed); alse, sets these fields in resulting object:
//   bool .newFeed -- is this feed just added?
//   bool .updatedFeed -- true if this is new feed or some of parameters was updated
//   int[] .newGroups -- ids of created groups
//   int[] .updatedGroups -- ids of updated groups
FeedsDB.prototype.addFeedFromObject = function (feed) {
  if (typeof(feed) !== "object") throw new Error("feed object expected");
  // first add all groups
  let newGroups = [];
  let groupCache = {};
  let gcount = 0;
  for (let gn of feed.groups.split(",")) {
    if (gn) ++gcount;
    if (!gn || (gn in groupCache)) continue;
    //debuglog("group '", gn, "'");
    let g = this.getOrAddGroupByTitle(gn);
    if (!g) throw new Error("WTF?! "+gn);
    groupCache[gn] = g;
    if (g.newGroup) newGroups.push(g.id);
  }
  if (!gcount) {
    // empty group
    let g = this.getGroupById(0);
    groupCache[""] = g;
    //if (g.newGroup) newGroups.push(g.id);
  }
  // now add feed
  let newflags =
    (feed.sendCookies ? flags.FFLAG.SEND_COOKIES : 0)|
    (feed.noUpdates ? flags.FFLAG.NO_UPDATES : 0)|
    flags.FFLAG.CUSTOM_UPDATE;
    //(feed.customRefresh ? flags.FFLAG.CUSTOM_UPDATE : 0);

  return this._doTransacted(function () {
    let fd = this.getFeedByUrl(feed.url);
    if (!fd) {
      // no such feed
      let st = this.db.createStatement(
        "INSERT INTO feeds (url,homeurl,title,flags,upinterval,uplast,upfailed,total,unread) VALUES(:url,:homeurl,:title,:flags,:upinterval,0,0,0,0)"
      );
      st.params.url = feed.url;
      st.params.homeurl = feed.homeurl;
      st.params.title = feed.title;
      st.params.flags = newflags;
      st.params.upinterval = feed.refreshInterval;
      st.step();
      st.reset();
      fd = this.getFeedByUrl(feed.url);
      if (!fd) throw new Error("database error!");
      fd.newFeed = true;
      fd.updatedFeed = true;
    } else {
      // existing feed, don't change anything
      fd.updatedFeed = false;
    }
    // now, as we have feed id, we can fix groups
    fd.newGroups = newGroups;
    fd.updatedGroups = [];
    for (let [gname, g] in Iterator(groupCache)) {
      if (g.feeds.indexOf(fd.id) < 0) {
        // if this is not a new group, add it to `updatedGroups`
        if (fd.newGroups.indexOf(g.id) < 0) fd.updatedGroups.push(g.id);
        // add to the group
        g.feeds.push(fd.id);
        this.updateGroupFeedsById(g.id, g.feeds);
      }
    }
    return fd;
  });
};


FeedsDB.prototype._updateFeedFieldById = function (fid, name, value) {
  //TODO: update cache here
  //      yet field updating is not that frequent, so we can get away with cache reseting
  if (fid in this._feedObjCache) {
    if (allowFeedObjCache) {
      //delete this._feedObjCache[fid];
      let obj = this._feedObjCache[fid];
      if (name in obj) obj[name] = value; else delete this._feedObjCache[fid]; // just in case
    } else {
      delete this._feedObjCache[fid];
    }
  }
  let st = this.db.createStatement("UPDATE feeds SET "+name+"=:value WHERE id=:id");
  st.params.id = fid;
  //st.params.name = name;
  st.params.value = value;
  while (st.step()) {}
  st.reset();
};


FeedsDB.prototype.updateFeedTitleById = function (fid, title) this._updateFeedFieldById(fid, "title", title);
FeedsDB.prototype.updateFeedHomeUrlById = function (fid, homeurl) this._updateFeedFieldById(fid, "homeurl", homeurl);
FeedsDB.prototype.updateFeedDescrById = function (fid, descr) this._updateFeedFieldById(fid, "descr", descr);
FeedsDB.prototype.updateFeedIconById = function (fid, icon) this._updateFeedFieldById(fid, "icon", icon);
FeedsDB.prototype.updateFeedFlagsById = function (fid, flags) this._updateFeedFieldById(fid, "flags", flags);
FeedsDB.prototype.updateFeedUpInterval = function (fid, secs) this._updateFeedFieldById(fid, "upinterval", secs);


FeedsDB.prototype._feedChangeFieldById = function (fid, name, delta) {
  let st = this.db.createStatement("UPDATE feeds SET :name=:name+:delta WHERE id=:id");
  st.params.id = fid;
  st.params.name = name;
  st.params.delta = delta;
  while (st.step()) {}
  st.reset();
};


FeedsDB.prototype.feedChangeTotalById = function (fid, delta) this._feedChangeFieldById(fid, "total", delta);
FeedsDB.prototype.feedChangeUnreadById = function (fid, delta) this._feedChangeFieldById(fid, "unread", delta);


////////////////////////////////////////////////////////////////////////////////
FeedsDB.prototype.feedChangeFlagById = function (fid, flag, action) {
  if (typeof(fid) !== "number") throw new Error("invalid fid!");
  fid = Math.floor(fid);
  if (typeof(flag) !== "number") throw new Error("invalid flag!");
  flag = Math.floor(flag);
  if (typeof(action) === "undefined") throw new Error("missing action!");
  //TODO: update cache here
  //      yet field updating is not that frequent, so we can get away with cache reseting
  if (fid in this._feedObjCache) delete this._feedObjCache[fid];
  this.beginExclusiveTransaction();
  try {
    // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
    let st = this.db.createStatement("SELECT flags FROM feeds WHERE id=:fid LIMIT 1");
    st.params.fid = fid;
    if (st.step()) {
      // has feed
      let f = st.row.flags;
      switch (action) {
        case "set": f |= flag; break;
        case "reset": f &= ~flag; break;
        case "toggle": f ^= flag; break;
      }
      let stu = this.db.createStatement("UPDATE feeds SET flags=:flags WHERE id=:fid");
      stu.params.fid = fid;
      stu.params.flags = f;
      stu.step();
      stu.reset();
    }
    this.commit();
  } catch (e) {
    this.dumpDBError();
    this.rollback();
    throw e;
  }
};


////////////////////////////////////////////////////////////////////////////////
FeedsDB.prototype.markFeedAsRead = function (fid) {
  if (typeof(fid) !== "number") throw new Error("invalid fid!");
  fid = Math.floor(fid);
  //TODO: update cache here
  if (fid in this._feedObjCache) delete this._feedObjCache[fid];
  this.beginExclusiveTransaction();
  let total = null;
  try {
    // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
    let st = this.db.createStatement("SELECT unread,total FROM feeds WHERE id=:fid LIMIT 1");
    st.params.fid = fid;
    if (st.step()) {
      if (st.row.unread > 0) {
        total = st.row.total;
        this.simpleSQL("UPDATE feeds SET unread=0 WHERE id="+fid);
        this.simpleSQL("UPDATE articles SET state="+flags.ASTATE.READ+" WHERE fid="+fid+" AND state="+flags.ASTATE.UNREAD);
      }
    }
    this.commit();
  } catch (e) {
    this.dumpDBError();
    this.rollback();
    throw e;
  }
  if (total !== null) {
    signals.emit("feed-totals-updated", {
      fid: fid,
      unreadCount: 0,
      totalCount: total,
    });
    this.queryTotalUnread(); // this updates the main total
  }
};


////////////////////////////////////////////////////////////////////////////////
FeedsDB.prototype.getNewArticlesFromList = function (fid, guidList) {
  if (guidList.length == 0) return {};
  // check cache
  let cc = this._fudGuidCache;
  let toCheck = [];
  if (fid in cc) {
    let cache = cc[fid];
    for (let f = 0; f < guidList.length; ++f) {
      let gg = guidList[f];
      if (gg in cache) {
        // do nothing
      } else {
        // check it
        toCheck.push(gg);
      }
    }
    if (toCheck.length == 0) return {}; // no new articles
  } else {
    toCheck = guidList;
    cc[fid] = {};
  }
  // update/rebuild cache
  let res = {};
  let cac = cc[fid];
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  let st = this.db.createStatement("SELECT guid FROM articles WHERE fid=:fid AND guid=:guid");
  debuglog(":::["+fid+"]; query "+toCheck.length+" guids...");
  for (let f = 0; f < toCheck.length; ++f) {
    let gg = toCheck[f];
    st.params.fid = fid;
    st.params.guid = gg;
    //debuglog("   <"+gg+">");
    try {
      if (st.step()) {
        if (st.row && st.row.guid) {
          //debuglog("   HASIT: <"+st.row.guid+">");
          cac[st.row.guid] = true;
        } else {
          //debuglog("   ********************: <"+toCheck[f]+">");
          res[gg] = true;
        }
      } else {
        //debuglog("   ::::::::::::::::::::: <"+toCheck[f]+">");
        res[gg] = true;
      }
    } finally {
      st.reset();
    }
  }
  cc[fid] = cac;
  return res;
};


////////////////////////////////////////////////////////////////////////////////
FeedsDB.prototype.getArticleByFidAndGuid = function (fid, guid, wantbody) {
  if (typeof(wantbody) === "undefined") wantbody = false;
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  let st = this.db.createStatement(
    "SELECT id,title,url,author,hash"+(wantbody ? ",body" : "")+",pubdate,state,mark FROM articles WHERE fid=:fid AND guid=:guid LIMIT 1"
  );
  st.params.fid = fid;
  st.params.guid = guid;
  let res = null;
  if (st.step()) {
    res = {
      id: st.row.id,
      title: st.row.title,
      url: st.row.url,
      author: st.row.author,
      hash: st.row.hash,
      pubdate: st.row.pubdate,
      //hitdate: st.row.hitdate,
      state: st.row.state,
      mark: st.row.mark,
    }
    if (wantbody) res.body = st.row.body;
  }
  st.reset();
  return res;
};


FeedsDB.prototype.getArticleInfoById = function (aid) {
  let res = null;
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  let st = this.db.createStatement("SELECT state,mark FROM articles WHERE id=:id LIMIT 1");
  st.params.id = aid;
  if (st.step()) {
    res = {
      id: aid,
      //hitdate: st.row.hitdate,
      state: st.row.state,
      mark: st.row.mark,
    }
  } else {
    res = {
      id: aid,
      //hitdate: 0,
      state: 0, // unread
      mark: 0,
    };
  }
  st.reset();
  return res;
};


FeedsDB.prototype.articleWasHit = function (aid, time) {
  if (typeof(aid) !== "number") throw new Error("invalid article id '"+aid+"' ("+typeof(aid)+")");
  if (aid >= 0) {
    if (typeof(time) === "undefined") time = Math.floor((new Date()).getTime()/1000);
    if (typeof(time) !== "number") throw new Error("invalid date value: '"+time+"'");
    this.db.executeSimpleSQL("UPDATE articles SET hitdate="+Math.floor(time)+" WHERE id="+Math.floor(aid));
  }
};


// insert new article; returns aid
// fart is:
//   int fid
//   string title
//   string link
//   string author
//   string guid
//   Date pubdate
//   string hash
//   string body
//   Date hitdate  (optional, default is now)
//   int state     (optional, default is 0)
//   int mark      (optional, default is 0)
/* not used anymore
FeedsDB.prototype.articleInsertWithObj = function (fart) {
  if (fart.fid in this._fudGuidCache) {
    let cc = this._fudGuidCache[fart.fid];
    cc[fart.guid] = true;
    this._fudGuidCache[fart.fid] = cc;
  } else {
    let cc = {};
    cc[fart.guid] = true;
    this._fudGuidCache[fart.fid] = cc;
  }
  return this._doTransacted(function () {
    let st = this.db.createStatement(
      "INSERT INTO articles (fid,guid,title,url,author,hash,body,pubdate,hitdate,state,mark) VALUES(:fid,:guid,:title,:url,:author,:hash,:body,:pubdate,:hitdate,:state,:mark)"
    );
    let pd = (typeof(fart.pubdate) !== "undefined" ? fart.pubdate : Math.floor((new Date()).getTime()/1000));
    let hd = (typeof(fart.hitdate) !== "undefined" ? fart.hitdate : Math.floor((new Date()).getTime()/1000));
    st.params.fid = fart.fid;
    st.params.guid = fart.guid;
    st.params.title = fart.title;
    st.params.url = fart.link;
    st.params.author = fart.author;
    st.params.hash = fart.hash;
    st.params.body = fart.body;
    st.params.pubdate = pd;
    st.params.hitdate = hd;
    st.params.state = fart.state||0;
    st.params.mark = fart.mark||0;
    st.step();
    st.reset();
    return this.db.lastInsertRowID;
  });
};
*/


////////////////////////////////////////////////////////////////////////////////
FeedsDB.prototype._insertNewArticlesFromListAsyncPhase2 = function (qi) {
  let queryInfo = qi;
  queryInfo.me = this; // just in case
  debuglog("_insertNewArticlesFromListAsyncPhase2(fid="+queryInfo.fid+"): unread="+queryInfo.sobj.unreadCount+"; total="+queryInfo.sobj.totalCount+"; newleft="+queryInfo.newArts.length);
  let st = this.db.createAsyncStatement("UPDATE feeds SET unread=:unread, total=:total, uplast=:uplast WHERE id=:fid");
  st.params.fid = qi.sobj.fid;
  st.params.unread = qi.sobj.unreadCount;
  st.params.total = qi.sobj.totalCount;
  st.params.uplast = (qi.sobj.unixNow ? qi.sobj.unixNow : Math.floor((new Date()).getTime()/1000));
  this._doAsync(st, function (row, succ) {
    if (!row) {
      debuglog("_insertNewArticlesFromListAsyncPhase2(fid="+queryInfo.fid+"): FINISHCB: unread="+queryInfo.sobj.unreadCount+"; total="+queryInfo.sobj.totalCount+"; newleft="+queryInfo.newArts.length);
      if (queryInfo.finishCB) queryInfo.finishCB(queryInfo.sobj);
    }
  });
};


FeedsDB.prototype._insertNewArticlesFromListAsyncPhase1 = function (qi) {
  let queryInfo = qi;
  debuglog("_insertNewArticlesFromListAsyncPhase1(fid="+queryInfo.fid+"): unread="+queryInfo.sobj.unreadCount+"; total="+queryInfo.sobj.totalCount+"; newleft="+queryInfo.newArts.length);
  if (queryInfo.newArts.length == 0) {
    if (queryInfo.sobj.updateTotals) {
      this._insertNewArticlesFromListAsyncPhase2(queryInfo);
    } else {
      debuglog("_insertNewArticlesFromListAsyncPhase1(fid="+queryInfo.fid+"): FINISHCB: unread="+queryInfo.sobj.unreadCount+"; total="+queryInfo.sobj.totalCount+"; newleft="+queryInfo.newArts.length);
      if (queryInfo.finishCB) queryInfo.finishCB(queryInfo.sobj);
    }
  } else {
    queryInfo.me = this; // just in case
    queryInfo.lastrowid = false;
    let fart = queryInfo.newArts[0];
    queryInfo.newArts.splice(0, 1);
    let st = this.db.createAsyncStatement(
      "INSERT INTO articles (fid,guid,title,url,author,hash,body,pubdate,hitdate,state,mark) VALUES(:fid,:guid,:title,:url,:author,:hash,:body,:pubdate,:hitdate,:state,:mark)"
    );
    if (fart.fid in this._fudGuidCache) {
      let cc = this._fudGuidCache[fart.fid];
      cc[fart.guid] = true;
      this._fudGuidCache[fart.fid] = cc;
    } else {
      let cc = {};
      cc[fart.guid] = true;
      this._fudGuidCache[fart.fid] = cc;
    }
    let pd = (typeof(fart.pubdate) !== "undefined" ? fart.pubdate : Math.floor((new Date()).getTime()/1000));
    let hd = (typeof(fart.hitdate) !== "undefined" ? fart.hitdate : Math.floor((new Date()).getTime()/1000));
    fart.state = fart.state||0;
    fart.mark = fart.mark||0;
    st.params.fid = fart.fid;
    st.params.guid = fart.guid;
    st.params.title = fart.title;
    st.params.url = fart.link;
    st.params.author = fart.author;
    st.params.hash = fart.hash;
    st.params.body = fart.body;
    st.params.pubdate = pd;
    st.params.hitdate = hd;
    st.params.state = fart.state;
    st.params.mark = fart.mark;
    this._doAsync(st, function (row, succ) {
      if (!row) {
        debuglog("_insertNewArticlesFromListAsyncPhase1(fid="+queryInfo.fid+"): done query 0: unread="+queryInfo.sobj.unreadCount+"; total="+queryInfo.sobj.totalCount+"; newleft="+queryInfo.newArts.length+"; lid="+queryInfo.lastrowid);
        if (queryInfo.lastrowid === false) {
          try {
            queryInfo.lastrowid = queryInfo.me.db.lastInsertRowID;
          } catch (e) {
            logException("FUUUU1!", e);
          }
        }
        debuglog("_insertNewArticlesFromListAsyncPhase1(fid="+queryInfo.fid+"): done query 1: unread="+queryInfo.sobj.unreadCount+"; total="+queryInfo.sobj.totalCount+"; newleft="+queryInfo.newArts.length+"; lid="+queryInfo.lastrowid);
        if (fart.state < flags.ASTATE.DELMARKED) {
          queryInfo.sobj.updateTotals = true;
          ++queryInfo.sobj.totalCount;
          ++queryInfo.sobj.newCount;
          if (fart.state == flags.ASTATE.UNREAD) ++queryInfo.sobj.unreadCount;
          queryInfo.sobj.alistNew.push(queryInfo.lastrowid);
        }
        queryInfo.me._insertNewArticlesFromListAsyncPhase1(queryInfo);
        return;
      }
      debuglog("_insertNewArticlesFromListAsyncPhase1(fid="+queryInfo.fid+"): query0: unread="+queryInfo.sobj.unreadCount+"; total="+queryInfo.sobj.totalCount+"; newleft="+queryInfo.newArts.length);
      if (queryInfo.lastrowid === false) {
        try {
          queryInfo.lastrowid = queryInfo.me.db.lastInsertRowID;
        } catch (e) {
          logException("FUUUU0!", e);
        }
      }
      debuglog("_insertNewArticlesFromListAsyncPhase1(fid="+queryInfo.fid+"): query1: unread="+queryInfo.sobj.unreadCount+"; total="+queryInfo.sobj.totalCount+"; newleft="+queryInfo.newArts.length+"; lid="+queryInfo.lastrowid);
      debuglog("   XXX(0): "+row.getResultByIndex(0));
      debuglog("   XXX(1): "+row.getResultByIndex(1));
    });
  }
};

// newArts: array of `fart` objects (see `articleInsertWithObj()`)
// sobj: signal object:
//   int fid
//   int totalCount (0)
//   int newCount (0)
//   int unreadCount (0)
//   fart[] alistNew ([])
//   bool manual
//   bool updateTotals (false)
//   int unixNow
FeedsDB.prototype.insertNewArticlesFromListAsync = function (newArts, sobj, finishCB) {
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  let st = this.db.createAsyncStatement("SELECT total,unread FROM feeds WHERE id=:id LIMIT 1");
  st.params.id = sobj.fid;
  let queryInfo = {
    fid: sobj.fid,
    sobj: sobj,
    newArts: newArts,
    finishCB: finishCB,
    me: this,
  };
  this._doAsync(st, function (row, succ) {
    if (!row) {
      // finished
      debuglog("insertNewArticlesFromListAsync(fid="+queryInfo.fid+"): done; unread="+queryInfo.sobj.unreadCount+"; total="+queryInfo.sobj.totalCount);
      queryInfo.me._insertNewArticlesFromListAsyncPhase1(queryInfo);
      return;
    }
    queryInfo.sobj.unreadCount += parseInt(row.getResultByName("unread"), 10);
    queryInfo.sobj.totalCount += parseInt(row.getResultByName("total"), 10);
  });
};


////////////////////////////////////////////////////////////////////////////////
// desctuctive command!
FeedsDB.prototype.dropEverything = function (reallyDrop) {
  if (reallyDrop !== true) return;
  this._fudGuidCache = {}; // reset all caches
  const tables = ["groups", "feeds", "articles"];
  this.open(); // just in case
  this.beginExclusiveTransaction();
  try {
    // update database
    for (let tn of tables) {
      debuglog("clearing table '", tn, "'");
      this.simpleSQL("DELETE FROM "+tn);
    }
    this.simpleSQL("INSERT INTO groups (id, title, feeds, treeindex) VALUES(0,'','',0)");
    this.simpleSQL("DELETE FROM sqlite_sequence");
    this.simpleSQL("INSERT INTO sqlite_sequence VALUES('groups',0)");
    this.simpleSQL("INSERT INTO sqlite_sequence VALUES('feeds',0)");
    this.simpleSQL("INSERT INTO sqlite_sequence VALUES('articles',0)");
    this.commit();
  } catch (e) {
    if (this.db.lastError) logError("DB ERROR ", this.db.lastError, ": ", this.db.lastErrorString);
    this.rollback();
    throw e;
  }
  debuglog("doing VACUUM");
  this.simpleSQL("VACUUM");
};


////////////////////////////////////////////////////////////////////////////////
// async queries
FeedsDB.prototype._doAsync = function (st, processor) {
  st.executeAsync({
    handleResult: function (rowset) {
      for (let row = rowset.getNextRow(); row; row = rowset.getNextRow()) processor(row, true);
    },
    handleError: function (err) { logError("_doAsync error: ", err.message); },
    handleCompletion: function (reason) {
      /*
      switch (reason) {
        case Ci.mozIStorageStatementCallback.REASON_FINISHED: break;
        case Ci.mozIStorageStatementCallback.REASON_CANCELED: break;
        case Ci.mozIStorageStatementCallback.REASON_ERROR: break;
      }
      */
      processor(null, (reason == Ci.mozIStorageStatementCallback.REASON_FINISHED)); // done
    },
  });

  return {
    cancel: function () { try { st.cancel(); } catch (e) {} }, // need `try`, cause double cancellation throws
  };
};


////////////////////////////////////////////////////////////////////////////////
// get group list, async
// calls callback with `null` (end) or object:
//   int id       (group id)
//   string title  (group title; "" means "default top-level group", always presented)
//   int treeindex
//   int[] feeds  (id of feeds in this group)
FeedsDB.prototype.getGroupsAsync = function (datacb) {
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  return this._doAsync(this.db.createAsyncStatement("SELECT id,title,feeds,treeindex FROM groups ORDER BY treeindex"), function (row, succ) {
    if (!row) { datacb(null, succ); return; }
    let fl = [];
    let feeds = row.getResultByName("feeds");
    for (let fid of feeds.split(",")) if (fid) fl.push(parseInt(fid, 10));
    datacb({
      id: row.getResultByName("id"),
      title: row.getResultByName("title"),
      feeds: fl,
      treeindex: row.getResultByName("treeindex"),
    }, true);
  });
};


////////////////////////////////////////////////////////////////////////////////
// get feed list, async
// calls callback with `null` (end) or object:
//   int id          (group id)
//   string url
//   string homeurl  (can be "")
//   string icon     ("data:" or "")
//   string title
//   string descr
//   int flags
//   int upinterval
//   int uplast
//   int upfailed
//   int total
//   int unread
FeedsDB.prototype.getFeedsAsync = function (datacb) {
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  return this._doAsync(this.db.createAsyncStatement("SELECT * FROM feeds"), function (row, succ) {
    if (!row) { datacb(null, succ); return; }
    /*
    let obj = {};
    obj.id = row.getResultByName("id");
    obj.url = row.getResultByName("url");
    obj.homeurl = row.getResultByName("homeurl");
    obj.icon = row.getResultByName("icon");
    obj.title = row.getResultByName("title");
    obj.descr = row.getResultByName("descr");
    obj.flags = row.getResultByName("flags");
    obj.upinterval = row.getResultByName("upinterval");
    obj.uplast = row.getResultByName("uplast");
    obj.upfailed = row.getResultByName("upfailed");
    obj.total = row.getResultByName("total");
    obj.unread = row.getResultByName("unread");
    this._feedObjCache[obj.id] = obj;
    */
    datacb({
      id: row.getResultByName("id"),
      url: row.getResultByName("url"),
      homeurl: row.getResultByName("homeurl"),
      icon: row.getResultByName("icon"),
      title: row.getResultByName("title"),
      descr: row.getResultByName("descr"),
      flags: row.getResultByName("flags"),
      upinterval: row.getResultByName("upinterval"),
      uplast: row.getResultByName("uplast"),
      upfailed: row.getResultByName("upfailed"),
      total: row.getResultByName("total"),
      unread: row.getResultByName("unread"),
    }, true);
  });
};


////////////////////////////////////////////////////////////////////////////////
// get new unread total (async)
FeedsDB.prototype.queryTotalUnread = function () {
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  this._doAsync(this.db.createAsyncStatement("SELECT SUM(unread) AS tur FROM feeds"), function (row, succ) {
    if (row) signals.emit("total-unread-update", row.getResultByName("tur"));
  });
};


////////////////////////////////////////////////////////////////////////////////
// cback (art)
//  int id
//  string title
//  string url
//  string author
//  string body
//  int state
//  int mark
//  Date pubdate
FeedsDB.prototype.queryArticleTextAsync = function (aid, cback) {
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  let st = this.db.createAsyncStatement("SELECT id,url,title,author,body,pubdate,state,mark FROM articles WHERE id=:aid LIMIT 1");
  st.params.aid = aid;
  return this._doAsync(st, function (row, succ) {
    if (!row) return;
    cback({
      id: row.getResultByName("id"),
      url: row.getResultByName("url"),
      title: row.getResultByName("title"),
      author: row.getResultByName("author"),
      body: row.getResultByName("body"),
      pubdate: new Date(row.getResultByName("pubdate")*1000),
      state: row.getResultByName("state"),
      mark: row.getResultByName("mark"),
    });
  });
};


// remove text of deleted articles, to compress database
/*
FeedsDB.prototype._updateTotalCountAsyncPhase3 = function (fid) {
  debuglog("_updateTotalCountAsyncPhase3(fid="+fid+"): entered");
  let st = this.db.createAsyncStatement("UPDATE articles SET body=:newbody WHERE fid=:fid AND state=:state");
  st.params.fid = fid;
  st.params.newbody = "";
  st.params.state = flags.ASTATE.DELETED;
  let delMarkedData = {
    fid: fid,
  };
  this._doAsync(st, function (row, succ) {
    if (!row) {
      // finished
      debuglog("_updateTotalCountAsyncPhase3(fid="+delMarkedData.fid+"): done");
      return;
    }
  });
};
*/


// get new feed totals, and sent a signal to UI
FeedsDB.prototype._updateTotalCountAsyncPhase2 = function (fid) {
  debuglog("_updateTotalCountAsyncPhase2(fid="+fid+"): entered");
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  let st = this.db.createAsyncStatement("SELECT unread,total FROM feeds WHERE id=:fid LIMIT 1");
  st.params.fid = fid;
  let delMarkedData = {
    fid: fid,
    unread: 0,
    total: 0,
    me: this,
  };
  this._doAsync(st, function (row, succ) {
    if (!row) {
      // finished
      debuglog("_updateTotalCountAsyncPhase2(fid="+delMarkedData.fid+"): done; unread="+delMarkedData.unread+"; total="+delMarkedData.total);
      // send signal with new totals
      signals.emit("feed-totals-updated", {
        fid: delMarkedData.fid,
        unreadCount: delMarkedData.unread,
        totalCount: delMarkedData.total,
      });
      // remove text of unread articles, to compress database
      //delMarkedData.me._updateTotalCountAsyncPhase3(delMarkedData.fid);
      return;
    }
    delMarkedData.unread += parseInt(row.getResultByName("unread"), 10);
    delMarkedData.total += parseInt(row.getResultByName("total"), 10);
  });
};


// update feed totals
FeedsDB.prototype._updateTotalCountAsyncPhase1 = function (fid, newtotal) {
  debuglog("_updateTotalCountAsyncPhase1(fid="+fid+"): entered; newtotal=", newtotal);
  let st = this.db.createAsyncStatement("UPDATE feeds SET total=:newtotal WHERE id=:fid");
  st.params.fid = fid;
  st.params.newtotal = newtotal;
  let delMarkedData = {
    fid: fid,
    me: this,
  };
  this._doAsync(st, function (row, succ) {
    if (!row) {
      // finished
      debuglog("_updateTotalCountAsyncPhase1(fid="+delMarkedData.fid+"): done");
      delMarkedData.me._updateTotalCountAsyncPhase2(delMarkedData.fid);
      return;
    }
  });
};


// count alive articles in feed
FeedsDB.prototype._updateTotalCountAsyncPhase0 = function (fid) {
  debuglog("_updateTotalCountAsyncPhase0(fid="+fid+"): entered");
  let st = this.db.createAsyncStatement("SELECT COUNT(id) AS cnt FROM articles WHERE fid=:fid AND state!=:state");
  st.params.fid = fid;
  st.params.state = flags.ASTATE.DELETED;
  let delMarkedData = {
    fid: fid,
    count: 0,
    me: this,
  };
  this._doAsync(st, function (row, succ) {
    if (!row) {
      // finished, update totals
      debuglog("_updateTotalCountAsyncPhase0(fid="+delMarkedData.fid+"): done; count="+delMarkedData.count);
      delMarkedData.me._updateTotalCountAsyncPhase1(delMarkedData.fid, delMarkedData.count);
      return;
    }
    delMarkedData.count += parseInt(row.getResultByName("cnt"), 10);
  });
};


FeedsDB.prototype._finishArticleDeletionAsync = function (fid) {
  debuglog("_finishArticleDeletionAsync(fid="+fid+"): entered");
  let st = this.db.createAsyncStatement("UPDATE articles SET state=:newstate,body=:newbody WHERE fid=:fid AND state=:state");
  st.params.fid = fid;
  st.params.state = flags.ASTATE.DELMARKED;
  st.params.newstate = flags.ASTATE.DELETED;
  st.params.newbody = "";
  let delMarkedData = {
    fid: fid,
    me: this,
  };
  this._doAsync(st, function (row, succ) {
    if (!row) {
      // finished, update totals
      debuglog("_finishArticleDeletionAsync(fid="+delMarkedData.fid+"): done");
      delMarkedData.me._updateTotalCountAsyncPhase0(delMarkedData.fid);
      return;
    }
  });
};


FeedsDB.prototype.deleteDelMarkedArticlesForAsync = function (fid) {
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  let st = this.db.createAsyncStatement("SELECT id FROM articles WHERE fid=:fid AND state=:state LIMIT 1");
  st.params.fid = fid;
  st.params.state = flags.ASTATE.DELMARKED;
  let delMarkedData = {
    fid: fid,
    hasDelmarked: false,
    me: this,
  };
  this._doAsync(st, function (row, succ) {
    if (!row) {
      // finished
      debuglog("deleteDelMarkedArticlesForAsync(fid="+delMarkedData.fid+"): done, hasDelmarked="+delMarkedData.hasDelmarked);
      if (delMarkedData.hasDelmarked) delMarkedData.me._finishArticleDeletionAsync(delMarkedData.fid);
      return;
    }
    debuglog("deleteDelMarkedArticlesForAsync(fid="+delMarkedData.fid+"): found delmarked article");
    delMarkedData.hasDelmarked = true;
  });
};


////////////////////////////////////////////////////////////////////////////////
// not used
FeedsDB.prototype.getArticlesForGroupAsync = function (gid, datacb) {};


////////////////////////////////////////////////////////////////////////////////
// get article list, async
// calls callback with `null` (end) or object:
//   int id         (article id)
//   string url
//   string title
//   string author
//   int pubdate    (unixtime)
//   int state
//   int mark
FeedsDB.prototype.getArticlesForFeedAsync = function (fid, datacb) {
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  let st = this.db.createAsyncStatement(
    "SELECT id,title,url,author,pubdate,state,mark FROM articles"+
    " WHERE fid=:fid AND state<=:state"+
    " ORDER BY pubdate,id"
  );
  st.params.fid = fid;
  st.params.state = flags.ASTATE.DELMARKED;
  return this._doAsync(st, function (row, succ) {
    if (!row) { datacb(null, succ); return; }
    datacb({
      id: row.getResultByName("id"),
      title: row.getResultByName("title"),
      url: row.getResultByName("url"),
      author: row.getResultByName("author"),
      pubdate: row.getResultByName("pubdate"),
      state: row.getResultByName("state"),
      mark: row.getResultByName("mark"),
    }, true);
  });
};


////////////////////////////////////////////////////////////////////////////////
FeedsDB.prototype.articleToggleMarkByIdSignalled = function (aid) {
  let me = this;
  let st = me.db.createAsyncStatement("UPDATE articles SET mark=1-mark WHERE id=:aid");
  st.params.aid = aid;
  me._doAsync(st, function (row, succ) {
    if (!row) {
      // done, now get new mark
      // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
      let sts = me.db.createAsyncStatement("SELECT mark FROM articles WHERE id=:aid LIMIT 1");
      sts.params.aid = aid;
      me._doAsync(sts, function (row, succ) {
        if (row) {
          // new data, send signal
          signals.emit("article-mark-set", {aid:aid, mark:!!row.getResultByName("mark")});
        }
      });
    }
  });
};


//FIXME: unify this with delete/undelete code
FeedsDB.prototype.articleChangeReadByIdSignalled = function (aid, setread) {
  let newstate = (setread ? flags.ASTATE.READ : flags.ASTATE.UNREAD);
  let oldstate = (setread ? flags.ASTATE.UNREAD : flags.ASTATE.READ);
  let me = this;
  // query article group and current state
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  let st = me.db.createAsyncStatement("SELECT fid FROM articles WHERE id=:aid AND state=:oldstate LIMIT 1");
  st.params.aid = aid;
  st.params.oldstate = oldstate;
  me._doAsync(st, function (row, succ) {
    if (!row) return; // alas
    // got article with such state, change it
    signals.emit("article-state-set", {aid:aid, state:newstate}); // signal article state change
    let fid = row.getResultByName("fid");
    // update article state
    let stu = me.db.createAsyncStatement("UPDATE articles SET state=:newstate WHERE id=:aid AND state=:oldstate");
    stu.params.aid = aid;
    stu.params.oldstate = oldstate;
    stu.params.newstate = newstate;
    me._doAsync(stu, function (row, succ) {
      if (!row) {
        // done updating article state, update feed totals
        let stu = me.db.createAsyncStatement("UPDATE feeds SET unread=unread+:delta WHERE id=:fid");
        stu.params.fid = fid;
        stu.params.delta = (setread ? -1 : 1);
        me._doAsync(stu, function (row, succ) {
          if (!row) {
            // done updating feed totals, do signalling
            // get new unread total (async)
            me.queryTotalUnread();
            // get new feeds total (async)
            // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
            let stt = me.db.createAsyncStatement("SELECT id,unread,total FROM feeds WHERE id=:fid");
            stt.params.fid = fid;
            me._doAsync(stt, function (row, succ) {
              if (row) {
                signals.emit("feed-totals-updated", {
                  fid: row.getResultByName("id"),
                  unreadCount: row.getResultByName("unread"),
                  totalCount: row.getResultByName("total"),
                });
              }
            });
          }
        });
      }
    });
  });
};


FeedsDB.prototype.articleChangeDelMarkByIdSignalled = function (aid, setdelmark) {
  let newstate = (setdelmark ? flags.ASTATE.DELMARKED : flags.ASTATE.READ);
  let oldstate = (setdelmark ? flags.ASTATE.READ : flags.ASTATE.DELMARKED);
  let me = this;
  // query article group and current state
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  let st = me.db.createAsyncStatement("SELECT fid FROM articles WHERE id=:aid AND state=:oldstate LIMIT 1");
  st.params.aid = aid;
  st.params.oldstate = oldstate;
  me._doAsync(st, function (row, succ) {
    if (!row) return; // alas
    // got article with such state, change it
    signals.emit("article-state-set", {aid:aid, state:newstate}); // signal article state change
    let fid = row.getResultByName("fid");
    // update article state
    let stu = me.db.createAsyncStatement("UPDATE articles SET state=:newstate WHERE id=:aid AND state=:oldstate");
    stu.params.aid = aid;
    stu.params.oldstate = oldstate;
    stu.params.newstate = newstate;
    me._doAsync(stu, function (row, succ) {});
  });
};


////////////////////////////////////////////////////////////////////////////////
// cback (fid, nextup), or      (do update now)
// cback (null, nextcheck), or  (wait for nextcheck before next check, if no update times were found)
// cback (null, null)           (done)
FeedsDB.prototype.findNextFeedToUpdate = function (cback) {
  let nextup = null;
  let now = new Date();
  // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
  return this._doAsync(this.db.createAsyncStatement("SELECT id,flags,upinterval,uplast FROM feeds ORDER BY uplast DESC"), function (row, succ) {
    if (!row) {
      debuglog("DONE!; nextup=", nextup);
      // done
      if (nextup !== null) cback(null, nextup); else cback(null, null);
      return;
    }
    //debuglog("id=",  row.getResultByName("id"), "; flg=", row.getResultByName("flags"), "; upinterval=", row.getResultByName("upinterval"), "; gup=", addonOptions.upinterval);
    let flg = row.getResultByName("flags");
    if (flg&flags.FFLAG.NO_UPDATES) return;
    let upinterval = row.getResultByName("upinterval");
    //!!!if ((flg&flags.FFLAG.CUSTOM_UPDATE) == 0) upinterval = 0;
    if (upinterval <= 0) upinterval = addonOptions.upinterval_minutes*60;
    if (upinterval <= 0) return;
    let uplast = row.getResultByName("uplast");
    let lup;
    //debuglog("uplast for feed ", row.getResultByName("id"), " is ", uplast);
    if (uplast < 1000) {
      let unixNow = Math.floor((new Date()).getTime()/1000)-24*60*60;
      lup = new Date(unixNow*1000);
    } else {
      lup = new Date((uplast+upinterval)*1000);
    }
    debuglog("uplast for feed ", row.getResultByName("id"), " is ", lup, " (uplast=", uplast, ", upinterval=", upinterval, ")");
    // is it time to update this one?
    if (lup <= now) {
      //fid = -1;
      nextup = null;
      cback(row.getResultByName("id"), now);
    } else if (nextup === null || lup < nextup) {
      nextup = lup;
    }
  });
};


////////////////////////////////////////////////////////////////////////////////
FeedsDB.prototype.feedDropAllArticlesById = function (fid) {
  if (typeof(fid) !== "number") throw new Error("invalid fid!");
  fid = Math.floor(fid);
  this._fudGuidCache[fid] = {}; // reset feed cache
  this.beginExclusiveTransaction();
  try {
    this.simpleSQL("DELETE FROM articles WHERE fid="+fid);
    this.simpleSQL("UPDATE feeds SET total=0,unread=0 WHERE id="+fid);
    this.commit();
  } catch (e) {
    this.dumpDBError();
    this.rollback();
    throw e;
  }
  signals.emit("feed-totals-updated", {
    fid: fid,
    unreadCount: 0,
    totalCount: 0,
  });
};


////////////////////////////////////////////////////////////////////////////////
FeedsDB.prototype.feedDropById = function (fid) {
  if (typeof(fid) !== "number") throw new Error("invalid fid!");
  fid = Math.floor(fid);
  this._fudGuidCache[fid] = {}; // reset feed cache
  this.beginExclusiveTransaction();
  try {
    this.simpleSQL("DELETE FROM articles WHERE fid="+fid);
    this.simpleSQL("DELETE FROM feeds WHERE id="+fid);
    // build changed groups list
    let groupUp = {}; // groups to update; key is id, value is new feeds
    // result field names are not guaranteed, but let's hope Mr.Hipp won't break it...
    let stg = this.db.createStatement("SELECT id,feeds FROM groups");
    while (stg.step()) {
      let wasHit = false;
      let fdlist = [];
      for (let xid of stg.row.feeds.split(",")) {
        if (!xid) continue;
        let fd = parseInt(xid, 10);
        if (isNaN(fd)) continue;
        if (fd == fid) {
          wasHit = true;
        } else if (fdlist.indexOf(fd) < 0) {
          fdlist.push(fd);
        }
      }
      if (wasHit) {
        //debuglog("groupup #", stg.row.id, ": <", fdlist.join(","), ">");
        groupUp[stg.row.id] = fdlist.join(",");
      }
    }
    stg.reset();
    // update groups
    for (let id in groupUp) {
      let feeds = groupUp[id];
      //debuglog("updating group <", id, "> with <", feeds, ">");
      let stu = this.db.createStatement("UPDATE groups SET feeds=:feeds WHERE id=:id");
      stu.params.id = id;
      stu.params.feeds = feeds;
      while (stu.step()) {}
      stu.reset();
    }
    this.commit();
  } catch (e) {
    this.dumpDBError();
    this.rollback();
    throw e;
  }
};


////////////////////////////////////////////////////////////////////////////////
FeedsDB.prototype.groupDropById = function (gid) {
  if (typeof(gid) !== "number") throw new Error("invalid gid!");
  this._fudGuidCache = {}; // reset all caches
  gid = Math.floor(gid);
  if (gid === 0) return;
  this.beginExclusiveTransaction();
  try {
    this.simpleSQL("DELETE FROM groups WHERE id="+gid);
    this.commit();
  } catch (e) {
    this.dumpDBError();
    this.rollback();
    throw e;
  }
};


////////////////////////////////////////////////////////////////////////////////
// lazily create FeedsDB object
let fdb = null;
Object.defineProperty(exports, "db", {get: function () {
  if (!fdb) fdb = new FeedsDB();
  return fdb;
}});
