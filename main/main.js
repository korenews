require("utils/utils");
require("gcli/commands");

let {cancelAllRefreshing} = require("feedupdater");


function openKoreNews (win) {
  debuglog("openKoreNews()");
  // search in current window
  const mainWinUrl = contentUrl+"korenews.xul";
  let tabbrowser = win.gBrowser;
  let tabs = tabbrowser.tabContainer.childNodes;
  let tab;
  for (let i = 0; i < tabs.length; ++i) {
    tab = tabs[i];
    let browser = tabbrowser.getBrowserForTab(tab);
    if (browser.contentDocument.location == mainWinUrl) {
      tabbrowser.selectedTab = tab;
      return;
    }
  }
  // search in other windows
  let wm = Cc["@mozilla.org/appshell/window-mediator;1"].getService(Ci.nsIWindowMediator);
  let enumerator = wm.getEnumerator("navigator:browser");
  while (enumerator.hasMoreElements()) {
    let win = enumerator.getNext();
    // |win| is [Object ChromeWindow] (just like |window|), do something with it
    tabbrowser = win.gBrowser;
    tabs = tabbrowser.tabContainer.childNodes;
    for (let i = 0; i < tabs.length; ++i) {
      tab = tabs[i];
      let browser = tabbrowser.getBrowserForTab(tab);
      if (browser.contentDocument.location == mainWinUrl) {
        // some problems with raising, so we close it to re-open after in current window
        //browser.loadURI("about:blank", null, null); // if there is only one tab it'll not be removed. So, we change the browser location to save feeds
        //tabbrowser.removeTab(tab);
        tabbrowser.selectedTab = tab;
        return;
      }
    }
  }
  // open new
  win.getBrowser().selectedTab = win.getBrowser().addTab(mainWinUrl);
  //win.getBrowser().loadURI("mainWinUrl", null, null);
}


function main_postinit () {
  debuglog("postinit...");
}


function main_preshutdown () {
  debuglog("aborting all updates...");
  //alert("000");
  cancelAllRefreshing();
}
