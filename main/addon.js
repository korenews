/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
let {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;

let addonobj = null;


////////////////////////////////////////////////////////////////////////////////
function initAddon (global, addonName, contentUrl, jsUrl, startupCB) {
  if (addonobj === null) {
    // check arguments
    if (typeof(addonName) !== "string" || addonName.length == 0) throw new Error("addon name expected (non-empty string)");
    if (typeof(contentUrl) !== "string" || contentUrl.length == 0) throw new Error("content url expected (non-empty string)");
    if (contentUrl[contentUrl.length-1] != "/") contentUrl += "/";
    if (typeof(jsUrl) === "undefined") jsUrl = contentUrl;
    if (typeof(jsUrl) !== "string" || jsUrl.length == 0) throw new Error("js url expected (non-empty string)");
    if (typeof(startupCB) === "undefined") startupCB = null;
    if (startupCB && typeof(startupCB) !== "function") throw new Error("startup callback expected (function)");

    //Components.utils.reportError("ADDON '"+addonName+"': initializing");
    // load "mainjs:init.js"
    let uri = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService).newURI(jsUrl+"init.js", null, null);
    let initfn = Cc["@mozilla.org/moz/jssubscript-loader;1"].getService(Ci.mozIJSSubScriptLoader).loadSubScript(uri.spec, this, "UTF-8");
    try {
      addonobj = initfn(global, addonName, contentUrl, jsUrl);
      if (!addonobj) throw new Error("can't init addon '"+addonName+"'"); // alas, something is wrong
      if (startupCB) startupCB(addonobj);
      addonobj.onStartup();
    } catch (e) {
      if (addonobj) try { addonobj.onShutdown(); } catch (xx) {}
      addonobj = false;
      Components.utils.reportError(e.stack);
      Components.utils.reportError(e);
      throw e;
    }
  }
  if (addonobj) return addonobj;
  throw new Error("addon object wasn't initialized");
}


////////////////////////////////////////////////////////////////////////////////
function getAddonObj () {
  if (addonobj) return addonobj;
  throw new Error("addon object wasn't initialized");
};


////////////////////////////////////////////////////////////////////////////////
let windowCount = 0;

function newWindow (global, win) {
  if (addonobj) {
    win.addEventListener("load", function () {
      ++windowCount;
      //Components.utils.reportError("NEW WINDOW: "+windowCount);
      // setup unload hook
      global.window.addEventListener("unload", function () {
        --windowCount;
        //Components.utils.reportError("DEAD WINDOW: "+windowCount);
        addonobj.onWindowUnload(win);
        if (windowCount == 0) {
          // no more registered windows --> shutdown
          //Components.utils.reportError("SHUTTING DOWN!");
          addonobj.onShutdown();
        }
      }, false);
      addonobj.onWindowLoad(win);
    }, false);
  }
}

////////////////////////////////////////////////////////////////////////////////
var EXPORTED_SYMBOLS = ["initAddon", "getAddonObj", "newWindow"];
