/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
// all our code will live in this nice closure
(function (global) {
  const {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = global.Components;
  let scp = {};
  Cu.import("chrome://korenews-jscode/content/addon_init.js", scp);
  let addonobj = scp.getAddonObj();

  const signals = addonobj.sbox.require("signals");
  const FeedsDB = addonobj.sbox.require("feedsdb");
  const {setRSSHandler} = addonobj.sbox.require("interceptor");

  scp.newWindow(global, global.window);

  let win = global.window;
  let inOpenMain = false;

  function openMainWindow () {
    //debuglog("opening new KoreNews window...");
    if (inOpenMain) return; // oops
    inOpenMain = true;
    try {
      addonobj.sbox.openKoreNews(win);
    } finally {
      inOpenMain = false;
    }
  }

  function signalUpdateTotalUnread (name, totalUnread) {
    if (!totalUnread) totalUnread = 0;
    let label = document.getElementById("korenews-status-button-label");
    let button = document.getElementById("korenews-status-button");
    label.textContent = ""+totalUnread;
    if (totalUnread === 0) {
      label.setAttribute("style", "color:#666");
      button.removeAttribute("hasnews");
    } else {
      label.removeAttribute("style");
      button.setAttribute("hasnews", "true");
    }
    let menu = document.getElementById("menu-korenews-open");
    if (totalUnread === 0) {
      menu.removeAttribute("hasnews");
    } else {
      menu.setAttribute("hasnews", "true");
    }
  }


  global.cmdOpenKoreNews = function () {
    openMainWindow();
  };

  //setRSSHandler(global.navigator, "http://ketmar.no-ip.org/rss");
  //setRSSHandler(global.navigator, "chrome://korenews/content/addfeed.xul");
  setRSSHandler("chrome://korenews/content/korenews-addfeed.xul?%s");

  global.window.addEventListener("load", function (ev) {
    signals.addListener("total-unread-update", signalUpdateTotalUnread);
    FeedsDB.db.queryTotalUnread(); // update button
  }, false);

  global.window.addEventListener("unload", function (ev) {
    signals.removeListener("total-unread-update", signalUpdateTotalUnread);
  }, false);

  const feedupdater = addonobj.sbox.require("feedupdater");
  feedupdater.findAndStartNextUpdate();
})(this);
