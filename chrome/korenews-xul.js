/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
let {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;

let scp = {};
Cu.import("chrome://korenews-jscode/content/addon_init.js", scp);
let addonobj = scp.getAddonObj();
addonobj.sbox.debuglog("new KoreNews UI window!");

let uiMain = addonobj.sbox.require("mainjs:ui/mainwin");


if (uiMain.onNewWindow(window)) {
  window.addEventListener("load", function (ev) {
    uiMain.onWindowLoaded(window);
  }, false);

  window.addEventListener("unload", function (ev) {
    uiMain.onWindowUnloaded(window);
  }, false);
}
